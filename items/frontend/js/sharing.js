

function facebookShare(itemId) 
{

	$.ajax(
	{
		url: rootUrl + 'Frontend/getItemForSharing/' + itemId,
		data: {},
		method: 'POST',
		success: function(data)
		{
			var ret = $.parseJSON(data);
			if(ret.success)
			{
				FB.ui(
				{
					method: 'share',
					href: ret.url,
				}, function(response){
				});			
			}
		}
	});

}


function twitterShare(itemId)
{
	
	$.ajax(
	{
		url: rootUrl + 'Frontend/getItemForSharing/' + itemId,
		data: {},
		method: 'POST',
		success: function(data)
		{
			var ret = $.parseJSON(data);
			if(ret.success)
			{
				window.open(
						  'https://twitter.com/share?url='+encodeURIComponent(ret.url) + '&hashtags=TBA21',
					      'facebook_share_dialog', 
					      'width=626,height=436'); 
					    return false;
			}
		}
	});
}