var item_activate_delay = false;

/***********************************
 * DEVICE SPECIFIC
 ************************************/
var searchOverlayHeight = 150;
var searchOverlayMargin = 10;
var contentContainerWidth = 320;
var snap_tolerance = 70;


function resize_mobile()
{
	$('#menu_dongle').css({'left': (wWidth - 75) /2});
	$('#menu_mobile').css({'width': wWidth, 'height': wHeight, 'top': wHeight});
}

function toggleMobileListeners(toggle)
{
	if(toggle)
	{
		$('.direction_pointer').click(function()
		{
			var continentId = $(this).attr('continent_id');
			nearest_continent = continentId;
			$('#canvas').animate({'left': snap_points[continentId][0], 'top': snap_points[continentId][1]}, 2000, 'easeInOutExpo', function()
			{
				viewport_offset_x = snap_points[continentId][0];
				viewport_offset_y = snap_points[continentId][1];
			});
			setTimeout(function()
			{
				$('.direction_pointer').fadeOut(500);
				//showMenuItems(continentId);
				checkItemVisibilityContinent(continentId);
				listView('continent', continentId);
			}, 1000);	
		});
		
		$('.item').on('click', function(event)
		{
			if(!item_activate_delay)
			{
				item_activate_delay = true;
				setTimeout(function()
				{
					item_activate_delay = false;
				}, 100);
				canvasItemClickMobile(event);
			}
			
		});
		
		$(window).on('orientationchange', function(event)
		{
			if(event.orientation == 'landscape')
			{
				$('#orientation_overlay').show();
			}
			else
			{
				$('#orientation_overlay').hide();
			}
		});
		
		
	}
	else
	{
		$('.direction_pointer').unbind('click');
		$('.item').off('touchend');
	}
}


function toggleMobileMenuListeners(toggle)
{
	if(toggle)
	{
		$('#menu_dongle').on('click', function()
		{
			toggleMobileMenu(true);
		});
		
		$('#menu_mobile_close').on('click', function()
		{
			toggleMobileMenu(false);
		});
	}
	else
	{
		$('#menu_dongle').off('click');
		$('#menu_mobile_close').off('click');
	}
}

function toggleMobileMenu(toggle)
{
	if(toggle)
	{
		$('#menu_dongle').fadeOut(150, function()
		{
			$('#menu_mobile').stop(false, false).animate({'top': 0}, 750, 'easeInOutQuart',function()
			{
				$('.continent_menu_items:not(.static)').find('.continent_menu_item span').click(function()
				{
					if(!$(this).parent().parent().hasClass('static'))
					{
						if(!$(this).parent().hasClass('artist_list'))
						{
							var changedItems = changeItemStatusByMetatag($(this).parent().attr('metatag_id'), $(this).parent().parent().parent().attr('continent_id'), 'inactive');
							if(changedItems.length == 1)
							{
								toggleMobileMenu(false);
								itemView(changedItems[0]);
							}
							else
							{
								toggleMobileMenu(false);
								listView('metatag', $(this).parent().attr('metatag_id'));
							}
								
						}
						else
						{
							toggleMobileMenu(false);
							listView('artist_list', null);
						}
					}
				});
				
				$('.continent_menu_header').click(function()
				{
					toggleMobileMenu(false);
					listView('continent', $(this).parent().attr('continent_id'));
				});
				
				$('.lang_select').click(function()
				{
					switchLanguage($(this).attr('lang'));
				});
				
				$('.continent_menu_items.static').find('.continent_menu_item span').click(function()
				{
					toggleMobileMenu(false);
					staticPages($(this).parent().attr('type'), $(this).parent().attr('itemId'));
				});
				
				$('#store_cart').on('click', function()
				{
					toggleMobileMenu(false);
					listView('cart', null);
				});
			});
		});
	}
	else
	{
		$('#menu_mobile').stop(false, false).animate({'top': wHeight}, 750, 'easeInOutQuart', function()
		{
			$('.continent_menu_items:not(.static)').find('.continent_menu_item span').unbind('click');
			$('.continent_menu_header').unbind('click');
			$('.continent_menu_items.static').find('.continent_menu_item span').unbind('click');
			$('.lang_select').unbind('click');
			$('#menu_dongle').fadeIn(150); 
			$('#store_cart').off('click');
		});
	}
}

function canvasItemClickMobile(event)
{
	if(!dragging)
	{
		
		item = findHoverItem(event.pageX, event.pageY);
		
		if(item != null && item.attr('item_id') != 'null' && !item.hasClass('item continent_center'))
		{
			changeItemStatus($('.item[status="active"][item_id!="' + item.attr('item_id') + '"]'), 'inactive');
			
			if(item.attr('status') == 'active')
			{
				itemView(item.attr('item_id'));
			}
			else
			{
				changeItemStatus(item, 'active');
				toggleTooltip(item.attr('tt_header_' + language), event.pageX+10, event.pageY+10);
			}
		}
	}
}

/***********************************
 * OVERWRITING FROM DESKTOP.JS
 ************************************/