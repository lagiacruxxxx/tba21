var viewport_offset_x = 0;
var viewport_offset_y = 0;
var active_menuitem = null;
var snap_points = new Array(5);
var dragging = false;
var view = 'hex';
var activeOverlay = [];
var menuHover = false;
var nearest_continent = null;


/***********************************
 * DEVICE SPECIFIC
 ************************************/
var searchOverlayHeight = 55;
var searchOverlayMargin = 15;
var contentContainerWidth = 1000;
var snap_tolerance = 180;

$(document).ready(function()
{	
	igniteDrag();

	if(!is_mobile)
	{
		if(!is_ipad)
		{
			toggleStaticMenuListeners(true);
			toggleHexMenuListeners(true);
			toggleCanvasListeners(true);
		}
		else
		{
			toggleStaticMenuListeners(true);
			toggleHexMenuListeners(true);
			toggleCanvasListeners(true);
			toggleHandheldListeners(true);
		}
	}
	else
	{
		toggleMobileMenuListeners(true);
		toggleMobileListeners(true);
		$(window).trigger('orientationchange');
	}
	
	if(!cookieinfo)
	{
		toggleCookieInfoListeners(true);
	}
	
	setTimeout(function()
	{
		checkItemVisibilityDrag();
	}, 500);
	
	
	if(location.hash != '')
		hashChange(location.hash);
	else
		location.hash = "";
	
	
	if(show_splash && location.hash == "")
	{
		switch(splash_itemId)
		{
			case "-1":
				setViewHex();
				listView('continent', 1);
				break;
			case "-2":
				setViewHex();
				listView('continent', 0);
				break;
			case "-3":
				setViewHex();
				listView('continent', 2);
				break;
			case "-4":
				setViewHex();
				listView('continent', 3);
				break;
			default:
				itemView(splash_itemId, 1);
				break;
		}
	}
});






/******************************************************************************************************************************************************************************************************
 * DRAGGING
 *****************************************************************************************************************************************************************************************************/
function igniteDrag()
{
	$('#canvas').draggable(
	{
		start: draggingStart,
		stop: draggingStop,
		drag: draggingDrag,
	});
}

function draggingStart(event, ui)
{
	dragging = true;
}

function draggingStop(event, ui)
{
	for(var i = 0 ; i < snap_points.length ; i++)
	{
		if(snap_points[i] !== undefined)
		{
			if(ui.position.left >= snap_points[i][0] - snap_tolerance && ui.position.left <= snap_points[i][0] + snap_tolerance && ui.position.top >= snap_points[i][1] - snap_tolerance && ui.position.top <= snap_points[i][1] + snap_tolerance)
			{
				$('#canvas').animate({'left': snap_points[i][0], 'top': snap_points[i][1]}, 150);
				ui.position.left = snap_points[i][0];
				ui.position.top = snap_points[i][1];
			}

			viewport_offset_x = ui.position.left;
			viewport_offset_y = ui.position.top;
		}
	}
	setTimeout(function()
	{
		dragging = false;	
	}, 50);	
}


function draggingDrag(event, ui)
{
	ui.position.left = Math.min(0, ui.position.left);
	ui.position.top = Math.min(0, ui.position.top);
	ui.position.left = Math.max(wWidth - canvasWidth, ui.position.left);
	ui.position.top = Math.max(wHeight - canvasHeight, ui.position.top);

	viewport_offset_x = ui.position.left;
	viewport_offset_y = ui.position.top;
	
	if(ui.position.left > snap_points[4][0] + 300 || ui.position.left < snap_points[4][0] - 300 || ui.position.top > snap_points[4][1] + 300 || ui.position.top < snap_points[4][1] - 300)
	{
		$('.direction_pointer').fadeOut(500);
		$('#bottom_menu').fadeOut(500);
		$('#menu_container').fadeIn(500);
		
		var screenpos_x = (viewport_offset_x*-1) + (wWidth/2);
		var screenpos_y = (viewport_offset_y*-1) + (wHeight/2);
		
		
		var distance = 99999;
		$('.continent_center').each(function()
		{
			var continent_x = parseInt($(this).css('left')) + itemWidth/2;
			var continent_y = parseInt($(this).css('top')) + itemHeight/2;
			var new_distance = Math.abs(screenpos_x - continent_x) + Math.abs(screenpos_y - continent_y);
			
			if(new_distance < distance)
			{
				distance = new_distance;
				nearest_continent = $(this).attr('continent_id');
			}
		});
		
		$('.teaser').fadeOut(150);
		
		showMenuItems(nearest_continent);
	}
	else
	{
		$('#bottom_menu').fadeIn(500);
		$('.direction_pointer').fadeIn(500);
		$('#menu_container').fadeOut(500);
		$('.teaser').fadeIn(150);
		nearest_continent = null;
	}
	
	
	checkItemVisibilityDrag();	
}

/******************************************************************************************************************************************************************************************************
 * CANVAS LISTENERS
 *****************************************************************************************************************************************************************************************************/
function toggleCanvasListeners(toggle)
{
	if(toggle)
	{
		$('#viewport').mousemove(function(event)
		{
			if(!is_mobile && !is_ipad)
				canvasMouseMove(event);
		});
		
		
		$('.direction_pointer').click(function()
		{
			triggerGATracker('continent', $(this).attr('text_0'));
			$('#bottom_menu').fadeOut(500);
			var continentId = $(this).attr('continent_id');
			nearest_continent = continentId;
			$('#canvas').animate({'left': snap_points[continentId][0], 'top': snap_points[continentId][1]}, 2000, 'easeInOutExpo', function()
			{
				viewport_offset_x = snap_points[continentId][0];
				viewport_offset_y = snap_points[continentId][1];
			});
			setTimeout(function()
			{
				if( continentId == 1)
				{
					setViewList();
					listView('continent', continentId);
				}
				
				$('.direction_pointer').fadeOut(500);
				showMenuItems(continentId);
				checkItemVisibilityContinent(continentId);
			}, 1000);	
		});
		
		if(!is_ipad)
		{
			$('.item').click(function(event)
			{
				canvasItemClick(event);
			});
		}
		
		$('#menu_logo, #menu_logo_small').click(function()
		{
			$('.view_select[view="hex"]').click();
			
			hideContentOverlay();
			center_canvas(true);
			setTimeout(function()
			{
				$('#bottom_menu').fadeIn(500);
				$('.direction_pointer').fadeIn(500);
				$('#menu_container').fadeOut(500);
				$('.teaser').fadeIn(500);
			}, 1500);
		});
	}
	else
	{
		$('.item').unbind('mousemove');
			
		if(!is_ipad)
		{
			$('.item').unbind('click');
		}
		$('.direction_pointer').unbind('click');
		$('#menu_logo, #menu_logo_small').unbind('click');
	}
}


function canvasItemClick(event)
{
	if(!dragging)
	{
		item = findHoverItem(event.pageX, event.pageY);
		
		if(item != null && item.attr('item_id') != 'null' && !item.hasClass('item continent_center'))
		{
			itemView(item.attr('item_id'));
		}
	}
}

function canvasMouseMove(event)
{
	$('.item[status="active"]').each(function()
	{
		if($(this).attr('metatags') !== undefined)
		{
			var metatags = $(this).attr('metatags').split(',');
			
			if(metatags.indexOf(active_menuitem) != -1)
				changeItemStatus($(this), 'selected');
			else
				changeItemStatus($(this), 'inactive');
		}
	});
	
	item = findHoverItem(event.pageX, event.pageY);
	
	if(item != null)
	{
		if(item.attr('status') != 'empty')
		{
			changeItemStatus(item, 'active');
			toggleTooltip(item.attr('tt_header_' + language), event.pageX +10 , event.pageY + 10);
		}
		else
		{
			toggleTooltip('');
		}
	}
	
	
}


function findHoverItem(pageX, pageY)
{
	var mouseX = pageX - viewport_offset_x;
	var mouseY = pageY - viewport_offset_y;
	var col = Math.floor(mouseX / itemWidth);
	var row = Math.floor(mouseY / (itemHeight/2));
	
	var item = $('.item[col="' + col + '"][row="' + row + '"]');
	
	if(!item.hasClass('rotated'))
	{
		if(!pointInTriange([mouseX, mouseY], [col * itemWidth, (row+1) * (itemHeight/2)], [(col+1) * itemWidth, (row+1) * (itemHeight/2)], [(col) * itemWidth, (row) * (itemHeight/2)]))
			item = $('.item[col="' + col + '"][row="' + (row-1) + '"]');
	}
	else
	{
		if(!pointInTriange([mouseX, mouseY], [col * itemWidth, (row+1) * (itemHeight/2)], [(col+1) * itemWidth, (row+1) * (itemHeight/2)], [(col+1) * itemWidth, (row) * (itemHeight/2)]))
			item = $('.item[col="' + col + '"][row="' + (row-1) + '"]');
	}
	
	if(item.hasClass('quad'))
	{
		mouseX = mouseX - parseInt(item.css('left'));
		mouseY = mouseY - parseInt(item.css('top'));
		
		if(!item.hasClass('rotated'))
		{
			if(pointInTriange([mouseX, mouseY], [0, 0], [0, (itemHeight/2)], [(itemWidth/2), (itemHeight/4)]))
				item = item.find('.item_quad[quad_id="0"]');
			
			else if(pointInTriange([mouseX, mouseY], [(itemWidth/2), (itemHeight/4)], [(itemWidth/2), (itemHeight*0.75)], [0, (itemHeight/2)]))
				item = item.find('.item_quad[quad_id="1"]');
			
			else if(pointInTriange([mouseX, mouseY], [0, (itemHeight/2)], [(itemWidth/2), (itemHeight*0.75)], [0, itemHeight]))
				item = item.find('.item_quad[quad_id="3"]');
			
			else if(pointInTriange([mouseX, mouseY], [(itemWidth/2), (itemHeight/4)], [(itemWidth/2), (itemHeight*0.75)], [itemWidth, (itemHeight/2)]))
				item = item.find('.item_quad[quad_id="2"]');
			else
				item = item.find('.item_quad[quad_id="1"]');
		}
		else
		{
			if(pointInTriange([mouseX, mouseY], [itemWidth, 0], [itemWidth, (itemHeight/2)], [(itemWidth/2), (itemHeight/4)]))
				item = item.find('.item_quad[quad_id="0"]');
			
			else if(pointInTriange([mouseX, mouseY], [(itemWidth/2), (itemHeight/4)], [(itemWidth/2), (itemHeight*0.75)], [itemWidth, (itemHeight/2)]))
				item = item.find('.item_quad[quad_id="1"]');
			
			else if(pointInTriange([mouseX, mouseY], [itemWidth, (itemHeight/2)], [(itemWidth/2), (itemHeight*0.75)], [itemWidth, itemHeight]))
				item = item.find('.item_quad[quad_id="3"]');
			
			else if(pointInTriange([mouseX, mouseY], [(itemWidth/2), (itemHeight/4)], [(itemWidth/2), (itemHeight*0.75)], [0, (itemHeight/2)]))
				item = item.find('.item_quad[quad_id="2"]');
			else
				item = item.find('.item_quad[quad_id="1"]');
		}
	}
	
	return item;
}

function changeItemStatus(items, status)
{
	items = items.filter('[status!="empty"]');
	
	switch(status)
	{
		case 'inactive':
			items.children('.original').show();
			items.children('.inactive').show();
			items.children('.selected').hide();
			items.children('.blur').hide();
			items.attr('status', 'inactive');
			break;
		case 'active':
			items.children('.original').show();
			items.children('.inactive').hide();
			items.children('.selected').hide();
			items.children('.blur').hide();
			items.attr('status', 'active');
			break;
		case 'selected':
			items.children('.original').show();
			items.children('.inactive').hide();
			items.children('.selected').show();
			items.children('.blur').hide();
			items.attr('status', 'selected');
			break;
		case 'blur':
			items.children('.inactive').hide();
			items.children('.selected').hide();
			items.children('.original').fadeOut(100);
			items.children('.blur').fadeIn(100);
			items.attr('status', 'blur');
			break;
			
	}
}


/******************************************************************************************************************************************************************************************************
 * MENU LISTENERS
 *****************************************************************************************************************************************************************************************************/
function toggleHexMenuListeners(toggle)
{
	if(toggle)
	{
		$('.continent_menu_items:not(.static)').find('.continent_menu_item span').click(function()
		{
			if(!$(this).parent().parent().hasClass('static'))
			{
				if(!$(this).parent().hasClass('artist_list'))
				{
					changeItemStatus($('.item, .item_rotated'), 'inactive');
					
					var continentId = $(this).parent().parent().parent().attr('continent_id');
					active_menuitem = $(this).parent().attr('metatag_id');
					
					var changedItems = changeItemStatusByMetatag(active_menuitem, continentId, 'selected');
					if(changedItems.length == 1)
					{
						itemView(changedItems[0]);
					}
				}
				else
					listView('artist_list', null);
			}
		});
		
		$('.continent_menu_items:not(.static)').find('.continent_menu_item span').hover(function()
		{
			toggleTooltip('');
			
			if(!$(this).parent().parent().hasClass('static'))
			{
				if(!$(this).parent().hasClass('artist_list'))
				{
					var metatag_id = $(this).parent().attr('metatag_id');
				
					var continentId = $(this).parent().parent().parent().attr('continent_id');
					
					changeItemStatusByMetatag(metatag_id, continentId, 'selected');
				}
			}
		},
		function()
		{
			if(!$(this).parent().hasClass('artist_list'))
			{
				var metatag_id = $(this).parent().attr('metatag_id');
				var continentId = $(this).parent().parent().parent().attr('continent_id');
				if(metatag_id != active_menuitem)
				{
					changeItemStatusByMetatag(metatag_id, continentId, 'inactive');
				}
			}
		});
		
		$('.continent_menu_header span').click(function()
		{
			var continentId = $(this).parent().parent().attr('continent_id');
			hideContentOverlay();
			if(nearest_continent == continentId)
			{
				if(parseInt($('.continent_menu[continent_id="' + continentId + '"]').find('ul').css('max-height')) == 0)
					showMenuItems(continentId);
				else
					showMenuItems(-1);
			}
			else
				$('.direction_pointer[continent_id="' + continentId + '"]').click();
			
			/*if( continentId == 1)
			{
				setViewList();
				listView('continent', continentId);
			}*/
		});
		
	}
	else
	{
		$('.continent_menu_items:not(.static)').find('.continent_menu_item span').unbind('mouseenter');
		$('.continent_menu_items:not(.static)').find('.continent_menu_item span').unbind('mouseleave');
		$('.continent_menu_items:not(.static)').find('.continent_menu_item span').unbind('click');
	}
}


function changeItemStatusByMetatag(metatagId, continentId, status)
{
	var itemcount = 0;
	var changedItems = [];
	$('.item[continent_id="' + continentId + '"]').each(function()
	{
		if(!$(this).hasClass('continent_center'))
		{
			if(!$(this).hasClass('quad'))
			{
				var metatags = $(this).attr('metatags').split(',');
				if(metatags.indexOf(metatagId) != -1)
				{
					changeItemStatus($(this), status);
					changedItems[itemcount++] = $(this).attr('item_id');
				}
			}
			else
			{
				$(this).find('.quad_item').each(function()
				{
					var metatags = $(this).attr('metatags').split(',');
					if(metatags.indexOf(metatagId) != -1)
					{
						changeItemStatus($(this), status);
						changedItems[itemcount++] = $(this).attr('item_id');
					}
				});
			}
		}
	});
	
	return changedItems;
}


function toggleListMenuListeners(toggle)
{
	if(toggle)
	{
		$('.continent_menu_items:not(.static)').find('.continent_menu_item span').click(function()
		{
			
			if(!$(this).parent().parent().hasClass('static'))
			{
				if(!$(this).parent().hasClass('artist_list'))
				{
					var changedItems = changeItemStatusByMetatag($(this).parent().attr('metatag_id'), $(this).parent().parent().parent().attr('continent_id'), 'inactive');
					if(changedItems.length == 1)
					{
						itemView(changedItems[0]);
					}
					else
						listView('metatag', $(this).parent().attr('metatag_id'));
				}
				else
					listView('artist_list', null);
			}
		});
		
		$('.continent_menu_header').click(function()
		{
			var continentId = $(this).parent().attr('continent_id')
			
			if(parseInt($('.continent_menu[continent_id="' + continentId + '"]').find('ul').css('max-height')) == 0)
			{
				showMenuItems(continentId);
				listView('continent', $(this).parent().attr('continent_id'));
			}
			else
				showMenuItems(-1);
			
		});
		
		
	}
	else
	{
		$('.continent_menu_items:not(.static)').find('.continent_menu_item span').unbind('click');
		$('.continent_menu_header').unbind('click');
	}
}


function toggleStaticMenuListeners(toggle)
{
	if(toggle)
	{
		$('.view_select').click(function()
		{
			if($(this).attr('view') != view)
			{
				if($(this).attr('view') == 'list')
					setViewList();
				else
					setViewHex();
				$('.view_select').toggleClass('active_select');
			}
		});
		
		
		$('.lang_select').click(function()
		{
			switchLanguage($(this).attr('lang'));
		});		
		
		
		$('.bottom_menuitem').click(function()
		{
			staticPages($(this).attr('type'), $(this).attr('itemId'));
		});
		
		
		$('#menu_button, #menu_button_small').click(function()
		{
			if($('#menu_button').attr('active') == 1)
				toggleMenu(false);
			else
				toggleMenu(true);
		});

		
		$('.continent_menu_items.static').find('.continent_menu_item span').click(function()
		{
			staticPages($(this).parent().attr('type'), $(this).parent().attr('itemId'));
		});
		
		
		$('#newsletter_item').on('click', function()
		{
			staticPages('newsletter', $(this).attr('itemId'));
		});
		
		$('#store_cart').on('click', function()
		{
			listView('cart', null);
		});
		
	}
	else
	{
		$('.view_select').unbind('click');
		$('.lang_select').unbind('click');
		$('.bottom_menuitem').unbind('click');
		$('#menu_button, #menu_button_small').unbind('click');
		$('.continent_menu_items.static').find('.continent_menu_item span').unbind('click');
		$('#newsletter_item').off('click');
		$('#store_cart').off('click');
	}
}


function staticPages(type, itemId)
{
	switch(type)
	{
		case 'press':
			listView('press', null);
			break;
		case 'foundation':
		case 'contact':
		case 'partners':
		case 'newsletter':
			itemView(itemId);
			break;
		case 'search':
			listView('search_empty', null);
			break;
	}
}


/******************************************************************************************************************************************************************************************************
 * DETAIL LISTENERS
 *****************************************************************************************************************************************************************************************************/
function showMenuItems(continentId)
{
	if(!is_mobile)
	{
		if(!$('#menu_container').visible())
		{
			$('#menu_container').fadeIn(500);
		}
		
		if(!$('.continent_menu[continent_id="' + continentId + '"]').find('.continent_menu_items').hasClass('active'))
		{
			$('.continent_menu_items:not(.static)').stop().animate({'max-height': 0}, 1000);
			$('.continent_menu_items').removeClass('active');
			$('.mainmenu_open').removeClass('mainmenu_open').addClass('mainmenu_closed');
			$('.continent_menu[continent_id="' + continentId + '"]').find('.continent_menu_items').stop().animate({'max-height': 500}, 1000);
			$('.continent_menu[continent_id="' + continentId + '"]').find('.continent_menu_items').addClass('active');
			$('.continent_menu[continent_id="' + continentId + '"]').find('.mainmenu_closed').removeClass('mainmenu_closed').addClass('mainmenu_open');
		}
	}
}




function pointInTriange(P, A, B, C) 
{
	// Compute vectors        
	function vec(from, to) 
	{  
		return [to[0] - from[0], to[1] - from[1]];  
	}
	
	var v0 = vec(A, C);
	var v1 = vec(A, B);
	var v2 = vec(A, P);
  
	// Compute dot products
  
	function dot(u, v) 
	{  
		return u[0] * v[0] + u[1] * v[1];  
	}
  
	var dot00 = dot(v0, v0);
    var dot01 = dot(v0, v1);
    var dot02 = dot(v0, v2);
    var dot11 = dot(v1, v1);
    var dot12 = dot(v1, v2);
    
    // Compute barycentric coordinates
    var invDenom = 1.0 / (dot00 * dot11 - dot01 * dot01);
    var u = (dot11 * dot02 - dot01 * dot12) * invDenom;
    var v = (dot00 * dot12 - dot01 * dot02) * invDenom;
  
    // Check if point is in triangle
    return (u >= 0) && (v >= 0) && (u + v < 1);
}	


/******************************************************************************************************************************************************************************************************
 * ITEM FADE IN
 *****************************************************************************************************************************************************************************************************/
function checkItemVisibilityDrag()
{
	var pos_x = parseInt(viewport_offset_x * -1) - 200;
	var pos_y = parseInt(viewport_offset_y * -1) - 200;
	
	$('.item.hidden:not(.teaser)').each(function()
	{
		if(parseInt($(this).css('left')) >= pos_x  && parseInt($(this).css('left')) <= pos_x + wWidth + 400 && parseInt($(this).css('top')) >= pos_y && parseInt($(this).css('top')) <= pos_y + wHeight + 400)
		{
			if(!$(this).hasClass('quad'))
			{
				$(this).fadeIn(parseInt((Math.random() * 1000) + 500));
				$(this).removeClass('hidden');
			}
			else
			{
				$(this).show();
				$(this).find('.item_quad').each(function()
				{
					$(this).fadeIn(parseInt((Math.random() * 1000) + 500));
					$(this).removeClass('hidden');
				});
			}
			//$(this).show();
		}
	});
	
	
}

function checkItemVisibilityContinent(continentId)
{
	$('.item.hidden[continent_id="' + continentId + '"]:not(.teaser)').each(function()
	{
		if(!$(this).hasClass('quad'))
		{
			$(this).fadeIn(parseInt((Math.random() * 1000) + 500));
			$(this).removeClass('hidden');
		}
		else
		{
			$(this).show();
			$(this).find('.item_quad').each(function()
			{
				$(this).fadeIn(parseInt((Math.random() * 1000) + 500));
				$(this).removeClass('hidden');
			});
		}
	});	

}






function setViewList()
{
	if(view != 'list')
	{
		view = 'list';
		toggleTooltip('');
		toggleHexMenuListeners(false);
		toggleListMenuListeners(true);
		//toggleCanvasListeners(false);
		if(nearest_continent == null || nearest_continent == 2)
			listView('home', null);
		else
		{
			if(active_menuitem != null && $('.continent_menu_item[metatag_id=' + active_menuitem + ']').parent().parent().attr('continent_id') == nearest_continent)
				listView('metatag', active_menuitem);
			else	
				listView('continent', nearest_continent);
		}
			
		
	}
}

function setViewHex()
{
	if(view != 'hex')
	{
		view = 'hex';
		hideContentOverlay();
		toggleListMenuListeners(false);
		toggleHexMenuListeners(true);
		//toggleCanvasListeners(true);
	}
}


function triangulation(mouseX, mouseY, orig_item)
{
	var x_in_triangle = mouseX - orig_item.position().left - viewport_offset_x;
	var y_in_triangle = mouseY - orig_item.position().top - viewport_offset_y; 
	var item = null;
	
	if(pointInTriange([x_in_triangle, y_in_triangle], [0,0], [0,itemHeight-1], [itemWidth-1,itemHeight/2]))
	{
		if(!orig_item.hasClass('continent_center'))
		{
			item = orig_item;
		}
	}
	else
	{
		if(pointInTriange([x_in_triangle, y_in_triangle], [0,0], [itemWidth-1,0], [itemWidth-1,(itemHeight/2)-1]))
		{
			if($('.item.rotated[row="' + parseInt(parseInt(orig_item.attr('row')) - 1) + '"][col="' + orig_item.attr('col') + '"]').length > 0)
			{
				item = $('.item.rotated[row="' + parseInt(parseInt(orig_item.attr('row')) - 1) + '"][col="' + orig_item.attr('col') + '"]');
			}
		}
		else
		{
			if($('.item.rotated[row="' + parseInt(parseInt(orig_item.attr('row')) + 1) + '"][col="' + orig_item.attr('col') + '"]').length > 0)
			{
				item = $('.item.rotated[row="' + parseInt(parseInt(orig_item.attr('row')) + 1) + '"][col="' + orig_item.attr('col') + '"]');
			}
		}
	}	
	
	return item;
}


function toggleTooltip(text, mouseX, mouseY)
{
	if(text == '' || text === undefined )
	{
		$('#tooltip_container').empty();
	}
	else
	{
		$('#tooltip_container').html('<span class="tooltip_header">' + text + '</span>');
		$('#tooltip_container').css({'left': mouseX, 'top': mouseY});
	}
}


/******************************************************************************************************************************************************************************************************
 * OVERLAY
 *****************************************************************************************************************************************************************************************************/

function showContentOverlay()
{
	var pos_x = parseInt(viewport_offset_x * -1) - 200;
	var pos_y = parseInt(viewport_offset_y * -1) - 200;

	toggleTooltip('');
	
	$('#menu_container').fadeIn(500);
	
	if((wWidth - $('#content_container').width())/2 < 350)
		toggleMenu(false);
		
	
	
	
	$('#content_loading').show();
	$('#content').hide();
	$('#content_navigation_nav').empty();
	
	$('#content_container').stop(false, false).animate({'top': '0px'}, 1500, 'easeOutExpo');
	$('#content_fade').stop(false, false).fadeIn(750);
	
	$('.item').each(function()
	{
		if(parseInt($(this).css('left')) >= pos_x  && parseInt($(this).css('left')) <= pos_x + wWidth + 400 && parseInt($(this).css('top')) >= pos_y && parseInt($(this).css('top')) <= pos_y + wHeight + 400)
		{
			$(this).find('.inactive').fadeOut(parseInt((Math.random() * 650) + 100));
			$(this).find('.selected').fadeOut(parseInt((Math.random() * 650) + 100));
			$(this).find('.original').fadeOut(parseInt((Math.random() * 650) + 100));
			$(this).find('.blur').fadeIn(parseInt((Math.random() * 650) + 100));
			$(this).attr('status','blur');
		}
	});
	toggleContentOverlayListeners(false);
	toggleContentOverlayListeners(true);
	toggleMiniMenu();
	
}


function toggleMenu(toggle)
{
	if(!is_mobile)
	{
		if(!toggle)
		{
			$('#menu_button').attr('active', 0);
			$('.continent_menu').fadeOut(300);
		}
		else
		{
			$('#menu_button').attr('active', 1);
			$('.continent_menu').fadeIn(300);
		}
	}
	
}



function hideContentOverlay()
{
	location.hash = "";
	
	activeOverlay = [];
	
	toggleMenu(true);
	if(nearest_continent == null)
		$('#menu_container').fadeOut(500);
	
	if(view == 'hex')
	{
		var pos_x = parseInt(viewport_offset_x * -1) - 200;
		var pos_y = parseInt(viewport_offset_y * -1) - 200;
		
		$('#content_container').stop(true, false).animate({'top': '100%'}, 1500, 'easeOutExpo', function()
		{
			$('#content').empty();
		});
		$('#content_fade').stop(true, false).fadeOut(750);
		$('.item[status="blur"]').each(function()
		{
			$(this).attr('status','inactive');
			$(this).find('.inactive').fadeIn(parseInt((Math.random() * 650) + 100));
			$(this).find('.original').fadeIn(parseInt((Math.random() * 650) + 100));
			$(this).find('.blur').fadeOut(parseInt((Math.random() * 650) + 100));
		});	
		
		toggleContentOverlayListeners(false);
		toggleMiniMenu();
	}
}

function toggleContentOverlayListeners(toggle)
{
	if(toggle)
	{
		$('#content_fade').click(function()
		{
			hideContentOverlay();
		});
		
		$('#content_search_button').click(function()
		{
			var params = "";
			$('.content_search_filter').find('input:checked').each(function()
			{
				if(params != '')
					params += ';';
				params += $(this).attr('continent_id');
				
			});
				
			listView('search', $('#content_search_input').val(), params);
		});
		
		$("#content_search_input").keyup(function(e)
		{ 
		    var code = e.which; 
		    if(code==13)
		    {
		    	e.preventDefault();
		    	$('#content_search_button').click();
		    }
		});
		
		$('.content_navigation_button').click(function()
		{
			switch($(this).attr('type'))
			{
				case 'search':
					toggleOverlaySearch(!($('#content_search').height() > 0));
					break;
				case 'facebook':
					facebookShare(activeOverlay[1]);
					break;
				case 'twitter':
					twitterShare(activeOverlay[1]);
					break;
				case 'hideSplash':
					hideContentOverlay();
					break;
			}
		});
		
	}
	else
	{
		$('#content_container').unbind('mousewheel');
		$('#content_fade').unbind('click');
		$('#content_search_button').unbind('click');
		$('.content_navigation_button').unbind('click');
		$('.navpoint').unbind('click');
		$("#content_search_input").unbind('keyup');
	}
}


function toggleOverlaySearch(toggle)
{
	if(toggle)
		$('#content_search').stop().animate({'height': searchOverlayHeight, 'padding-top': searchOverlayMargin}, 150);
	else
		$('#content_search').stop().animate({'height': 0, 'padding-top': 0}, 150);
}




function fitImage(image, iWidth, iHeight, wWidth, wHeight)
{
	
	var new_height = 0;
	var new_width = 0;
	
	var ratio = iWidth / iHeight;

	if(wWidth > wHeight)
	{
		new_height = wHeight;
		new_width = parseInt(wHeight * ratio);
		if(new_width < wWidth)
		{
			new_width = wWidth;
			new_height = parseInt(wWidth / ratio);
			left = 0;
			top = (parseInt((new_height-wHeight) /-2));
		}
		else
		{
			top = 0;
			left = parseInt((new_width - wWidth) / -2);		
		}
	}
	else
	{
		new_width = wWidth;
		new_height = parseInt(wWidth / ratio);
		if(new_height < wHeight)
		{
			new_height = wHeight;
			new_width = parseInt(wHeight * ratio);
			top = 0;
			left = parseInt((new_width - wWidth) / -2);
		}
		else
		{
			left = 0;
			top = parseInt((new_height - wHeight) / -2);	
		}
	}

	image.css({'width' : new_width, 'height': new_height, 'left': left, 'top': top});
		
}


/******************************************************************************************************************************************************************************************************
 * LANGUAGE
 *****************************************************************************************************************************************************************************************************/
function switchLanguage(new_language)
{
	
	if(language != new_language)
	{
		// change language variable and highlight new language
		language = new_language;
		$('.lang_select').toggleClass('active_select');
		docCookies.setItem('tba21_lang', language, Infinity, null, null , false);
	
		
		// change language in frontend
		$('.direction_pointer').each(function()
		{
			$(this).text($(this).attr('text_' + language));
		});
		
		$('.bottom_menuitem').each(function()
		{
			$(this).text($(this).attr('text_' + language));
		});
		
		$('.continent_menu_item').each(function()
		{
			$(this).find('span').text($(this).attr('text_' + language));
		});
		
		$('.continent_menu_header').each(function()
		{
			$(this).find('span').text($(this).attr('text_' + language));
		});
		
		/*$('.view_select').each(function()
		{
			$(this).text($(this).attr('text_' + language));
		});*/
		
		$('.continent_center').each(function()
		{
			$(this).text($(this).attr('text_' + language));
		});
		
		$('#content_search_button').text($('#content_search_button').attr('text_' + language));
		
		$('#orientation_overlay div').text($('#orientation_overlay div').attr('text_' + language));
		
		// update overlay content
		if(activeOverlay != [])
		{
			switch(activeOverlay[0])
			{
				case 'item':
					itemView(activeOverlay[1]);
					break;
				
				case 'list':
					listView(activeOverlay[1], activeOverlay[2], activeOverlay[3]);
					break;
			}
		}
	}
}

/******************************************************************************************************************************************************************************************************
 * ITEM DETAIL
 *****************************************************************************************************************************************************************************************************/
function itemView(itemId, isSplash)
{
	activeOverlay = ['item', itemId, '', '', language];
	showContentOverlay();
	
	$('#content_navigation').animate({'opacity': 0}, 150);
	
	if(isSplash === undefined)
		isSplash = 0;
	
	toggleOverlaySearch(false);
	toggleListListeners(false);
	toggleItemDetailListeners(false);
	$.ajax(
	{
		url: rootUrl + 'Frontend/detail/' + itemId,
		data: {'isSplash': isSplash},
		method: 'POST',
		success: function(data)
		{
			var ret = $.parseJSON(data);
			
			if(parseInt(ret.special) == 0)
			{
				$('#content').empty();
				if(ret.success)
					html = ret.html;
				else
					html = "Error!";
				
				$('#content').html(html);
				$('#content_navigation_nav').empty().html(ret.nav);
					
				
				setHash('item--' + ret.prettyurl + '--' + itemId);
				
				triggerGATracker('item', ret.prettyurl);
				
				resize_col($('#content_column_left'));
				resize_col($('#content_column_right'));
				
				imagesLoaded($('#content'), function()
				{
					imagesLoaded($('.gallery_item img'), function()
					{
						/*$('.gallery_item img').each(function()
						{
							fitImage($(this), $(this).get(0).naturalWidth, $(this).get(0).naturalHeight, $(this).parent().width(), $(this).parent().height());
						});*/
						
						imagesLoaded($('.content_related_item img'), function()
						{
							/*$('.content_related_item img').each(function()
							{
								fitImage($(this), $(this).get(0).naturalWidth, $(this).get(0).naturalHeight, $(this).parent().width(), $(this).parent().height());
							});*/
							
							$('#content_loading').fadeOut(300, function()
							{
								$('#content').fadeIn(300);
								
								updateNavigation(ret, 'item');
								
								resize_col($('#content_column_left'));
								resize_col($('#content_column_right'));
							});
						});
					});
				});
				
				toggleItemDetailListeners(true);
			}
			else
			{
				showSpecial(ret.special, ret.specialData);
			}
		}
	});		
}

function toggleItemDetailListeners(toggle)
{
	if(toggle)
	{
		if(!is_mobile && !is_ipad)
		{
			$('.content_related_item').hover(function()
			{
				$(this).find('.content_related_item_overlay').stop().animate({'bottom': 0}, 150);
			},
			function()
			{
				$(this).find('.content_related_item_overlay').stop().animate({'bottom': -200}, 150);
			});
		}
		
		$('.content_related_item').click(function()
		{
			itemView($(this).attr('item_id'));
		});
		
		$('.gallery_item').click(function()
		{
			$(".gallery_item").fancybox(
			{
				prevEffect		: 'none',
				nextEffect		: 'none',
				closeBtn		: false,
				helpers		: 
				{
					title	: { type : 'inside' },
					buttons  : {}
				}
			});
		});
		
		$('#content_related_tags span').click(function()
		{
			if($(this).attr('metatag_id') !== undefined)
				listView('metatag', $(this).attr('metatag_id'));
		});
		
		$('.press_nologin').click(function()
		{
			listView('press', null);
		});
		
		$('#registration_form').on('submit', function(e)
		{
			e.preventDefault();
			submitRegistration($(this));
		});
		
		$('.store_add').on('click', function()
		{
			addProductToCart($(this).attr('magento_id'));
		});
		
		$('.store_tocart').on('click', function()
		{
			listView('cart', null);
		});
		
		
	}
	else
	{
		$('.content_related_item').unbind('hover');
		$('.content_related_item').unbind('click');
		$('.gallery_item').unbind('click');
		$('#content_related_tags span').unbind('click');
		$('.press_nologin').unbind('click');
		$('#registration_form').off('submit');
		$('.store_add').off('click');
	}
}

function resize_col($col)
{
	var min_height = 0;
	$col.find('.module').each(function()
	{
		if(min_height < parseInt($(this).height()) + $(this).position().top + 2)
			min_height = parseInt($(this).height()) + $(this).position().top + 2
	});
	
	if(min_height == 0)
		min_height = 40;
	
	$col.css({'min-height': min_height});
	return min_height;
}



/******************************************************************************************************************************************************************************************************
 * LIST 
 *****************************************************************************************************************************************************************************************************/
function listView(listType, listId, params)
{
	$('#content_navigation').animate({'opacity': 0}, 150);
	
	if(params === undefined)
		params = "";
	
	if(activeOverlay[0] == 'list' && listType == activeOverlay[1] && (activeOverlay[2] !== undefined && listId == activeOverlay[2]) && params == activeOverlay[3] && language == activeOverlay[4])
		var offset = $('#content_columns').attr('offset') !== undefined ? $('#content_columns').attr('offset') : null;
	else
		var offset = null;	
	
	activeOverlay = ['list', listType, listId, params, language];
	
	if(offset == null)
		showContentOverlay();

	toggleItemDetailListeners(false);
	toggleListListeners(false);
	
	
	$.ajax(
	{
		url: rootUrl + 'Frontend/listview',
		data: { 'listType': listType, 'listId': listId, 'offset': offset, 'params': params},
		method: 'POST',
		cache: false,
		success: function(data)
		{
			var ret = $.parseJSON(data);
			if(ret.success)
			{
				html = ret.html;
			}
			else
			{
				html = "Error!";
			}
			
			if(ret.html == ret.nav && ret.nav == ret.append && ret.append == ret.prettyurl)
			{
				itemView(ret.html, false);
			}
			else
			{
				setHash(ret.prettyurl);
				triggerGATracker(ret.trackerData.cat, ret.trackerData.value);
				
				if(listType == 'search_empty' || listType == 'search')
					toggleOverlaySearch(true);
				else
					toggleOverlaySearch(false);
					
				
				if(ret.append == 0)
				{
					$('#content').empty();
					$('#content').html(html);
				}
				else
				{
					$('#content_columns').attr('offset', ret.append);
					$('#content_columns').append(html);
				}
				
	
				
				
				imagesLoaded($('.list_item_img'), function()
				{
					$('.list_item_img').each(function()
					{
						fitImage($(this), $(this).get(0).naturalWidth, $(this).get(0).naturalHeight, $(this).parent().width(), $(this).parent().height());
					});
					
					$('#content_loading').fadeOut(150, function()
					{
						$('#content').fadeIn(150);
						updateNavigation(ret, 'list');
					});
				});
				
				toggleListListeners(true);
				
				// prevent scroll event from firing when the list doesnt return any more results
				if(ret.html.replace('\r\n', '').trim() == '')
				{
					$('#content_container').unbind('scroll');
				}
			}	
		}
	});	
	
	if(offset == null)
	{
		$('#content_loading').show();
		$('#content').hide();
		$('#content_navigation_nav').empty();
	}
}


function toggleListListeners(toggle)
{
	toggleCheckoutListeners(toggle);

	
	if(toggle)
	{
		$('.list_item').click(function()
		{
			itemView($(this).attr('item_id'));
		});
		
		$('.artist_list_artist span').click(function()
		{
			listView('metatag', $(this).parent().attr('metatag_id'));
		});
		
		$('#press_login_button').click(function()
		{
			press_login($('#press_email').val());
		});
		
		$('#press_reg_button').click(function()
		{
			press_registration($('#press_email').val());
		});
		
		$('#press_logout').click(function()
		{
			press_logout();
		});
		
		$('.presskit span').on('click', function()
		{
			itemView($(this).parent().attr('item_id'), false);
		});
		
		$('#content_container').scroll(function()
		{
			if($(this).scrollTop() + wHeight + 50 > $('#content').height() -100 && activeOverlay[0] == 'list' && activeOverlay[1] != 'artist_list' &&  activeOverlay[1] != 'checkout' &&  activeOverlay[1] != 'press' && activeOverlay[1] != 'cart')
				listView(activeOverlay[1], activeOverlay[2], activeOverlay[3]);
		});
		
		$('.cart_item_remove').on('click', function()
		{
			removeItemFromCart($(this).parent().parent().attr('rowid'));
		});
		
		$('.cart_item_qty_btn').on('click', function()
		{
			if($(this).hasClass('less'))
				updateItemCartQty($(this).parent().parent().attr('rowid'), -1);
			else
				updateItemCartQty($(this).parent().parent().attr('rowid'), 1);
		});	
		
		$('#cart_checkout').on('click', function()
		{
			listView('checkout', null);
		});
		
	}
	else
	{
		$('.navpoint').unbind('click');
		$('.artist_list_artist span').unbind('click');
		$('#press_login_button').unbind('click');
		$('#press_reg_button').unbind('click');
		$('#content_container').unbind('scroll');
		$('.list_item').unbind('click');
		$('#press_logout').unbind('click');
		$('.presskit span').off('click');
		$('#chart_checkout').off('click');
	}
}



/******************************************************************************************************************************************************************************************************
 * COOKIE INFO
 *****************************************************************************************************************************************************************************************************/

function toggleCookieInfoListeners(toggle)
{
	if(toggle)
	{
		$('#cookie_info_btn').on('click', function()
		{
			docCookies.setItem('tba21_cookie_accepted', 1, Infinity, null, null , false);
			$('#cookie_info').remove();
		});
	}
	else
	{
		$('#cookie_info_btn').off('click');
	}
}


/******************************************************************************************************************************************************************************************************
 * SPECIAL PAGE
 *****************************************************************************************************************************************************************************************************/

function showSpecial(special, data)
{
	switch(parseInt(special))
	{
		case 1:
			$('body').append(data);
			$('#umb_close').on('click', function()
			{
				hideContentOverlay();
				$('#ute_meta_bauer').fadeOut(250, function()
				{
					$('#ute_meta_bauer').remove();
				});
			});
			break;
		case 2:
			window.location.href = "https://www.tba21.org/#tag--Ephemeropteræ--23";
			break;
	}
	
	console.log('special');
}