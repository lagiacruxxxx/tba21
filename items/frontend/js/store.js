

function updateCart()
{
	$.ajax(
	{
		url: rootUrl + 'Shop/getCartItemCount',
		data: {},
		method: 'GET',
		success: function(data)
		{
			var ret = $.parseJSON(data);
			$('#store_cart_itemcount').text(ret.count);
			$('.module_store_cart').empty().text('(' + ret.count + ')');
			if(ret.count > 0)
				$('#store_cart').show();
			else
				$('#store_cart').hide();
		}
	});	
}

function addProductToCart(productId)
{
	$.ajax(
	{
		url: rootUrl + 'Shop/addToCart/' + productId,
		data: {},
		method: 'POST',
		success: function(data)
		{
			var ret = $.parseJSON(data);
			if(ret.success)
			{
				$('#store_cart_itemcount').text(ret.count);
				$('.module_store_cart').empty().text('(' + ret.count + ')');
				if(ret.count > 0)
					$('#store_cart').show();
				else
					$('#store_cart').hide();
			}
		}
	});
}

function removeItemFromCart(rowId)
{
	$.ajax(
	{
		url: rootUrl + 'Shop/removeFromCart/' + rowId,
		data: {},
		method: 'POST',
		success: function(data)
		{
			var ret = $.parseJSON(data);
			if(ret.success)
			{
				updateCart();
				$('.cart_item[rowid="' + rowId + '"]').fadeOut(150, function()
				{
					$(this).remove();
				});	
				$('#cart_total').empty().text(ret.total);
				if($('.cart_item').length <= 0)
				{
					$('#cart_total').fadeOut(150);
					$('#cart_empty').fadeIn(150);
					$('#cart_checkout').fadeOut(150);
				}
			}
			$('#store_cart_itemcount').text(ret.count);
			$('.module_store_cart').empty().text('(' + ret.count + ')');

			if(ret.count > 0)
				$('#store_cart').show();
			else
				$('#store_cart').hide();
		}
	});
}

function updateItemCartQty(rowId, change)
{
	$.ajax(
	{
		url: rootUrl + 'Shop/updateItemCartQty/',
		data: {'rowId': rowId, 'change': change},
		method: 'POST',
		success: function(data)
		{
			var ret = $.parseJSON(data);
			if(ret.success)
			{
				updateCart();
				if(ret.count > 0)
				{
					$('.cart_item[rowid="' + rowId + '"]').find('.cart_item_qty_qty').empty().text(ret.count);
					$('.cart_item[rowid="' + rowId + '"]').find('.cart_item_price').empty().text(ret.price);
				}
				else
					$('.cart_item[rowid="' + rowId + '"]').fadeOut(150, function()
					{
						$(this).remove();
						$('#cart_total').empty().text(ret.total);
						if($('.cart_item').length <= 0)
						{
							$('#cart_total').fadeOut(150);
							$('#cart_empty').fadeIn(150);
							$('#cart_checkout').fadeOut(150);
						}
					});
			}
		}
	});	
}

function toggleCheckoutListeners(toggle)
{
	if(toggle)
	{
		$('#finish_checkout span').on('click', function()
		{
			verifyCheckoutData();
		});
		
		$('#confirm_order span').on('click', function()
		{
			listView('shopFinished', null);
		});
		
		$('#checkout_country').on('change', function()
		{
			updateShipmentSelect();
		});
	}
	else
	{
		$('#finish_checkout span').off('click');
		$('#confirm_order span').off('click');
	}
}

function updateShipmentSelect()
{
	$('.checkout_shipment').hide();
	$('.checkout_shipment').attr('active', '0');
	var loc = $('#checkout_country').find(":selected").attr('location');
	
	$('.checkout_shipment[location="' + loc + '"]').show();
	$('.checkout_shipment[location="' + loc + '"]').attr('active', '1');
}


function verifyCheckoutData()
{
	$.ajax(
	{
		url: rootUrl + 'Shop/verifyCheckoutData/',
		data: 
		{
			'firstname': $('#checkout_firstname').val(),
			'lastname': $('#checkout_lastname').val(),
			'email': $('#checkout_email').val(),
			'phone': $('#checkout_phone').val(),
			'street': $('#checkout_street').val(),
			'zip': $('#checkout_zipcode').val(),
			'city': $('#checkout_city').val(),
			'country': $('#checkout_country').val(),
			'accept_tos': $('#checkout_terms_accept').is(':checked') ? 1 : 0,
			'paymentType': $('#payment_type').val(),
			'shipmentType': $('.checkout_shipment[active="1"]').val(),
		},
		method: 'POST',
		success: function(data)
		{
			var ret = $.parseJSON(data);
			if(ret.success)
			{
				$('#checkout_errors').empty();
				storeData($('#payment_type').val());
			}
			else
			{
				$('#checkout_errors').empty().append(ret.message);
			}
		}
	});	
}


function startPayment()
{
	$.ajax(
	{
		url: rootUrl + 'Shop/startPayment/',
		data: 
		{

		},
		method: 'POST',
		success: function(data)
		{
			var ret = $.parseJSON(data);
			if(ret.success)
			{
				
			}
			else
			{
				$('#checkout_errors').empty().append(ret.message);
			}
		}
	});
}




var paymentType = "";

// function for storing sensitive data to the Wirecard data storage
function storeData(aPaymentType) {
	
	// sets the selected payment type where sensitive data should be stored
    paymentType = aPaymentType;
    // creates a new JavaScript object containing the Wirecard data storage functionality
    var dataStorage = new WirecardCEE_DataStorage();
    // initializes the JavaScript object containing the payment specific information and data
    var paymentInformation = {};
    if (aPaymentType == "CreditCard") {
        if (!document.getElementById('checkout_cc_number')) 
        {
            dataStorage.storeCreditCardInformation(null, callbackFunction);
        } else {
            paymentInformation.pan = document.getElementById('checkout_cc_number').value;
            paymentInformation.expirationMonth = document.getElementById('checkout_cc_exp_month').value;
            paymentInformation.expirationYear = document.getElementById('checkout_cc_exp_year').value;
            paymentInformation.cardholdername = document.getElementById('checkout_cc_owner').value;
            paymentInformation.cardverifycode = document.getElementById('checkout_cc_code').value;
            paymentInformation.issueMonth = '';
            paymentInformation.issueYear = '';
            paymentInformation.issueNumber = '';
            // stores sensitive data to the Wirecard data storage
            dataStorage.storeCreditCardInformation(paymentInformation, callbackFunction);
        }
    }
   
}

// callback function for displaying the results of storing the
// sensitive data to the Wirecard data storage
callbackFunction = function (aResponse) 
{
	$('.shop_validation_error').empty();
	
	// checks if response status is without errors
    if (aResponse.getStatus() == 0) 
    {
    	var info = aResponse.getAnonymizedPaymentInformation();
    	$.ajax(
		{
			url: rootUrl + 'Shop/createOrder/',
			data: 
			{
				'firstname': $('#checkout_firstname').val(),
				'lastname': $('#checkout_lastname').val(),
				'email': $('#checkout_email').val(),
				'phone': $('#checkout_phone').val(),
				'street': $('#checkout_street').val(),
				'zip': $('#checkout_zipcode').val(),
				'city': $('#checkout_city').val(),
				'country': $('#checkout_country').val(),
				'accept_tos': $('#checkout_terms_accept').is(':checked') ? 1 : 0,
				'paymentType': $('#payment_type').val(),
				'shipmentType': $('.checkout_shipment[active="1"]').val(),
				'storageResponse': info, 
				'storageId': aResponse.response.storageId,
			},
			method: 'POST',
			success: function(data)
			{
				var ret = $.parseJSON(data);
				if(ret.success)
				{
					listView('shopConfirm', null);
				}
				else
				{
					
				}
			}
		});	    	
    }
    else 
    {
    	switch(paymentType)
    	{
	    	case 'CreditCard':
	    		$('#cc_error').empty().html(cc_error_text);
	    		break;
    	}
    }
}

