
function submitRegistration(form)
{
	switch(form.attr('type'))
	{
		case 'newsletter':
			submitNewsletterSubscription(form);
			break;
			
		case 'newsletter_unsub':
			submitNewsletterUnsub(form);
			break;
	}
}


function submitNewsletterSubscription(form)
{
	$.ajax(
	{
		url: rootUrl + 'Authentication/subscribeNewsletter/',
		data: form.serialize(),
		method: 'POST',
		success: function(data)
		{
			var ret = $.parseJSON(data);
			
			if(ret.success)
			{
				form.hide();
				$('#newsletter_reg_error').empty().append(ret.message);
			}
			else
			{
				$('#newsletter_reg_error').empty().append(ret.message);
			}
		},
	});
}


function submitNewsletterUnsub(form)
{
	$.ajax(
	{
		url: rootUrl + 'Authentication/unsubscribeNewsletter/',
		data: form.serialize(),
		method: 'POST',
		success: function(data)
		{
			var ret = $.parseJSON(data);
			
			if(ret.success)
			{
				form.hide();
				$('#newsletter_unsub_error').empty().append(ret.message);
			}
			else
			{
				$('#newsletter_unsub_error').empty().append(ret.message);
			}
			resize_col($('#content_column_left'));
			resize_col($('#content_column_right'));
			
		},
	});
}