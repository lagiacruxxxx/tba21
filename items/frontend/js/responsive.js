var wWidth;
var wHeight;
var content_container_width;

$(document).ready(function()
{	
	resize();
	center_canvas(false);
	//adjustContinentPosition();
	positionTeaserItems();
	calculateSnapPoints();
	checkItemVisibilityDrag();
	
	$(window).resize(function()
	{
		resize();
	});
});

function resize()
{
	wWidth = $(document).width();
	wHeight = $(document).height();
	
	
	place_tba_center();
	place_bottom_menu();
	
	$('#content_container').css({'left': (wWidth-contentContainerWidth)/2});
	$('#splashpage').css({'left': (wWidth-contentContainerWidth)/2});
	
	if(activeOverlay != [])
	{
		if((wWidth - $('#content_container').width())/2 < 350)
			toggleMenu(false);
	}
	
	if(is_mobile)
		resize_mobile();
	
	
	toggleMiniMenu();	
}


function place_tba_center()
{
	$('#center').css({'left': (canvasWidth - $('#center').width())/2, 'top': (canvasHeight - $('#center').height())/2});
}


function center_canvas(animate)
{
	var canvas = $('#canvas');
	viewport_offset_x = ((canvasWidth - wWidth)/2)*-1;
	viewport_offset_y = ((canvasHeight - wHeight)/2)*-1;
	nearest_continent = null;
	
	if(animate)
		$('#canvas').animate({'left': viewport_offset_x, 'top': viewport_offset_y}, 2000, 'easeInOutExpo');
	else
		$('#canvas').css({'left': viewport_offset_x, 'top': viewport_offset_y});
	
	snap_points[4] = [viewport_offset_x, viewport_offset_y]; 
}


function calculateSnapPoints()
{
	$('.continent_center').each(function()
	{
		snap_points[$(this).attr('continent_id')] = 
		[
		 	(parseInt($(this).css('left')) - parseInt(wWidth/2) + parseInt(itemWidth/2))*-1,
		 	(parseInt($(this).css('top')) - parseInt(wHeight/2) + parseInt(itemHeight/2))*-1,
		];
	});
}

function place_bottom_menu()
{
	$('#bottom_menu').css({'margin-left': (wWidth-$('#bottom_menu').width())/2, 'margin-right': (wWidth-$('#bottom_menu').width())/2});
}


function adjustContinentPosition()
{
	var center_x = $('#center').position().left + $('#center').width()/2;
	var center_y = $('#center').position().top + $('#center').height()/2;
	
	
	var dist = 99999;
	var new_dist = 0;
	var continentId = $(this).attr('continent_id');
	var nearest_item = null;
	

	
	$('.item[continent_id=1]:not(.item_quad):not(.continent_center)').each(function()
	{
		var item_x = parseInt($(this).css('left')) + itemWidth/2;
		var item_y = parseInt($(this).css('top')) + itemHeight/2;
		
		new_dist = lineDistance(center_x, center_y, item_x, item_y);
		
		if(new_dist < dist)
		{
			dist = new_dist;
			nearest_item = $(this);
		}
	});
	nearest_item.show().removeClass('hidden');
	
	var target_x = Math.floor((wWidth/2) / itemWidth) + 19;
	var target_y = 18 - Math.floor((wHeight/2) / itemHeight);

	moveContinent(1, target_x - parseInt(nearest_item.attr('col')), target_y - parseInt(nearest_item.attr('row')));
	
	
	
	dist = 99999;
	nearest_item = null;
	$('.item[continent_id=0]:not(.item_quad):not(.continent_center)').each(function()
	{
		var item_x = parseInt($(this).css('left')) + itemWidth/2;
		var item_y = parseInt($(this).css('top')) + itemHeight/2;
		
		new_dist = lineDistance(item_x, item_y, center_x, center_y);
		
		if(new_dist < dist)
		{
			dist = new_dist;
			nearest_item = $(this);
		}
	});
	
	//console.log(neare)
	nearest_item.show().removeClass('hidden');
	
	var target_x = 20 - Math.floor((wWidth/2) / itemWidth);
	var target_y = 18 - Math.floor((wHeight/2) / itemHeight);
	
	moveContinent(0, target_x - parseInt(nearest_item.attr('col')), target_y - parseInt(nearest_item.attr('row')));

	
	dist = 99999;
	nearest_item = null;
	$('.item[continent_id=3]:not(.item_quad):not(.continent_center)').each(function()
	{
		var item_x = parseInt($(this).css('left')) + itemWidth/2;
		var item_y = parseInt($(this).css('top')) + itemHeight/2;
		
		new_dist = lineDistance(item_x, item_y, center_x, center_y);
		
		if(new_dist < dist)
		{
			dist = new_dist;
			nearest_item = $(this);
		}
	});
	nearest_item.show().removeClass('hidden');
	
	var target_x = Math.floor((wWidth/2) / itemWidth)  + 19;
	var target_y = Math.floor((wHeight/2) / itemHeight) + 19;

	moveContinent(3, target_x - parseInt(nearest_item.attr('col')), target_y - parseInt(nearest_item.attr('row')));
	
	
	restoreRaster();
}


function moveContinent(continentId, move_x, move_y)
{
	$('.item[continent_id="' + continentId + '"]:not(.item_quad)').each(function()
	{
		var item = $(this);
		var new_col = parseInt(item.attr('col')) + move_x;
		var new_row = parseInt(item.attr('row')) + move_y;
		var new_left = parseInt(item.css('left')) + (itemWidth*move_x);
		var new_top = parseInt(item.css('top')) + ((itemHeight/2)*move_y);

		if($('.item[status="empty"][col="' + new_col + '"][row="' + new_row + '"]').length > 0)
			$('.item[status="empty"][col="' + new_col + '"][row="' + new_row + '"]').remove();
		
		item.attr('col', new_col);
		item.attr('row', new_row);
		
		item.css({left: new_left, top: new_top});
	});
}

function positionTeaserItems()
{
	// PROGRAM
	var target_x = Math.floor((wWidth/2) / itemWidth) + 20;
	var target_y = 18 - Math.floor((wHeight/2) / itemHeight);
	
	if((wWidth/2) % itemWidth < 0.3 * itemWidth)
		target_x -= 1;
		
	if((wHeight/2) % itemHeight < 0.2 * itemHeight)
		target_y += 1;
	
	positionTeaserItem(target_x, target_y, 1);
	
	
	// THE CURRENT
	var target_x = Math.floor((wWidth/2) / itemWidth) + 20;
	var target_y = 20 + Math.floor((wHeight/2) / itemHeight);
	
	if((wWidth/2) % itemWidth < 0.3 * itemWidth)
		target_x -= 1;
		
	if((wHeight/2) % itemHeight < 0.2 * itemHeight)
		target_y += 1;
	
	positionTeaserItem(target_x, target_y, 3);
	
	
	// COLLECTION
	var target_x = 19 - Math.floor((wWidth/2) / itemWidth);
	var target_y = 18 - Math.floor((wHeight/2) / itemHeight);
	
	if((wWidth/2) % itemWidth < 0.3 * itemWidth)
		target_x += 1;
		
	if((wHeight/2) % itemHeight < 0.2 * itemHeight)
		target_y += 1;
	
	positionTeaserItem(target_x, target_y, 0);	
	
}

function positionTeaserItem(target_x, target_y, continentId)
{
	var target = $('.item[col="' + target_x + '"][row="' + target_y + '"]');
		
	target.remove();
	
	var teaser = $('.item[row=100][col="' + continentId + '"]');
	
	teaser.attr('col', target_x);
	teaser.attr('row', target_y);
	teaser.css({left: target.css('left'), top: target.css('top')});
	if(target_y % 2 == 1)
		teaser.addClass('rotated');
	
	teaser.addClass('teaser');
	
	teaser.attr('continentId', 5);
	teaser.show();
	teaser.find('.item_quad').show();	
}

function restoreRaster()
{
	
	var empty_base = $('.item[status="empty"][col=0][row=0]');
	
	for(var row = 0; row < canvasRows ; row++)
	{
		for(var col = 0 ; col < canvasCols ; col++)
		{
			if($('.item[col="' + col + '"][row="' + row + '"]').length == 0)
			{
				var new_empty = empty_base.clone();
				new_empty.attr('col', col);
				new_empty.attr('row', row);
				new_empty.css({left: col * itemWidth, top: row*(itemHeight/2)});
				if(row % 2 == 1)
					new_empty.addClass('rotated');
				
				$('#canvas').append(new_empty);
			}
		}
	}
}

function lineDistance( x1, y1, x2, y2 )
{
	var xs = 0;
	var ys = 0;
 
	xs = x2 - x1;
	xs = xs * xs;
 
	ys = y2 - y1;
	ys = ys * ys;
 
    return Math.sqrt( xs + ys );
}


function toggleMiniMenu()
{
	if(!is_mobile)
	{
		if(wWidth <= 1170 && activeOverlay.length > 0)
		{
			$('#content_navigation_menu').fadeIn(150);
			$('#menu_logo').fadeOut(150);
			$('#menu_button').fadeOut(150);
		}
		else
		{
			$('#content_navigation_menu').fadeOut(150);
			$('#menu_logo').fadeIn(150);
			$('#menu_button').fadeIn(150);
		}
	}
	
}