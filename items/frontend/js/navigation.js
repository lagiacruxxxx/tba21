var _ignoreHashChange = false;
var revertHashChange = false;

$(document).ready(function()
{	
	toggleHashListeners(true);
});


function hashChange(hash)
{
	hash = hash.split('--');
	switch(hash[0])
	{
		case '#item':
			itemView(hash[2]);
			break;
		case '#tag':
			listView('metatag', hash[2]);
			break;
		case '#home':
			listView('home', null);
			break;
		case '#artists':
			listView('artist_list', null);
			break;
		case '#continent':
			listView('continent', hash[2]);
			break;
		case '#press':
			listView('press', null);
			break;
		case '#search':
			if(hash[1] !== undefined)
				listView('search', hash[1]);	
			else
				listView('search_empty', null);	
			break;
		case '#ocean':
			$('#direction_pointer_academy').click();
			break;
		case '#collection':
			$('#direction_pointer_collection').click();
			break;
		case '#program':
			$('#direction_pointer_program').click();
			break;
		case '#media':
			$('#direction_pointer_production').click();
			break;
		case '#cart':
			listView('cart', null);
			break;
		case '#checkout':
			listView('checkout', null);
			break;	
			
		case '#confirm':
			listView('shopConfirm', null);
			break;
			
		case '':
			hideContentOverlay();
			break;
	}
}


function setHash(new_hash)
{
	if(window.location.hash != '#' + new_hash)
	{
		_ignoreHashChange = true;
		revertHashChange = true;
		window.location.hash = new_hash;
	}
}


function triggerGATracker(category, value)
{
	ga('send', 'event', category, 'click', value);
}


function toggleHashListeners(toggle)
{
	if(toggle)
	{
		$(window).on('hashchange', function() 
		{
			if(!_ignoreHashChange)
				hashChange(location.hash);
			
			_ignoreHashChange = false;
		});
	}
	else
		$(window).unbind('hashchange');
}



function navpoint_navigation(navpoint)
{
	switch(navpoint.attr('nav_type'))
	{
		case 'continent':
			listView('continent', navpoint.attr('nav_id'));
			break;
		case 'artistlist':
			listView('artist_list', null);
			break;
		case 'metatag':
			listView('metatag', navpoint.attr('nav_id'));
			break;
		case 'press':
			listView('press', null);
			break;
			
	}
}


function updateNavigation(ret, type)
{
	$('#content_navigation_nav').empty().html(ret.nav);
	
	if(type == 'list')
	{
		$('.content_navigation_button[type="twitter"]').hide();
		$('.content_navigation_button[type="facebook"]').hide();
		$('.content_navigation_button[type="search"]').show();
	}
	else
	{
		$('.content_navigation_button[type="twitter"]').show();
		$('.content_navigation_button[type="facebook"]').show();
		$('.content_navigation_button[type="search"]').hide();
	}
		
	if(view == 'hex')
		$('.content_navigation_button[type="hideSplash"]').show();
	else
		$('.content_navigation_button[type="hideSplash"]').hide();
	
	$('#content_navigation').animate({'opacity': 1}, 150);
	
	$('.navpoint').click(function()
	{
		navpoint_navigation($(this));
	});
}