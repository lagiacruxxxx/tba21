
function press_login(email)
{
	$.ajax(
	{
		url: rootUrl + 'Authentication/press_login/',
		data: {email: email},
		method: 'POST',
		success: function(data)
		{
			var ret = $.parseJSON(data);
			$('#press_login_error').empty();
			if(!ret.success)
				$('#press_login_error').text(ret.message);
			else
				listView('press', null);
		}
	});		
}


function press_registration()
{
	$.ajax(
	{
		url: rootUrl + 'Authentication/press_registration/',
		data: 
		{
			firstname: $('#press_reg_firstname').val(),
			lastname: $('#press_reg_lastname').val(),
			publication: $('#press_reg_publication').val(),
			email: $('#press_reg_email').val(),
		},
		method: 'POST',
		success: function(data)
		{
			var ret = $.parseJSON(data);
			$('#press_reg_error').empty();
			$('#press_reg_error').html(ret.message);
		}
	});	
}


function press_logout()
{
	$.ajax(
	{
		url: rootUrl + 'Authentication/press_logout/',
		data: {	},
		method: 'POST',
		success: function(data)
		{
			var ret = $.parseJSON(data);
			listView('press', null);
		}
	});
}