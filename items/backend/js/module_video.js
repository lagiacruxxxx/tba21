

function new_module_video(id, parent)
{
	var new_elem = 
	{
		module: null,
		type: 'video',
		code: '',
		id: id,
		parent: parent,
		
		
		// MODULE BASIC
		init: function(parent)
		{
			this.module = $('.module[module_id=' + this.id + ']');
			this.bindListeners();
			igniteDragging();
		},
		
		getPrototypeHTML: function()
		{
			var html = '<div module_id=' + this.id + ' class="module module_video has_placeholder" data-text="Insert video here" style="top: ' + (Math.ceil(resize_col(this.parent)/10) * 10) + 'px;" code="754pqkdorhs" start="0"><div class="module_video_overlay"></div><iframe class="module_video_iframe" src="https://www.youtube.com/embed/754pqkdorhs?start=0&wmode=transparent" frameborder="0" allowfullscreen="" wmode="Opaque"></iframe></div>';
			return html;
		},
		
		bindListeners: function()
		{
			this.module.dblclick(function()
			{
				active_module = $(this);
				
				$('#popup_module_video').find('.popup_cancel_button').unbind('click');
				$('#popup_module_video').find('.popup_save_button').unbind('click');
				$('#popup_module_video').find('.popup_delete_button').unbind('click');
				$('#popup_module_video').find('.popup_switch_button').unbind('click');

				$('#popup_module_video').find('#video_code_input').val(active_module.attr('code'));
				$('#popup_module_video').find('#video_start_input').val(active_module.attr('start'));
				$('#popup_module_video').show();
				
				$('#popup_module_video').find('.popup_save_button').click(function()
				{
					active_module.attr('code', $('#popup_module_video').find('#video_code_input').val());
					active_module.attr('start', $('#popup_module_video').find('#video_start_input').val());
					active_module.find('iframe').attr('src', 'https://www.youtube.com/embed/' + $('#popup_module_video').find('#video_code_input').val() + '?start=' + $('#popup_module_video').find('#video_start_input').val() + '&amp;wmode=transparent');
					$('#popup_module_video').hide();
				});
				
				$('#popup_module_video').find('.popup_cancel_button').click(function()
				{
					$('#popup_module_video').hide();
				});
				
				$('#popup_module_video').find('.popup_delete_button').click(function()
				{
					var parent = active_module.parent();
					modules.splice(active_module.attr('module_id'),1);
					active_module.remove();
					resize_col(parent);
					$('#popup_module_video').hide();
				});
				
				$('#popup_module_video').find('.popup_switch_button').click(function()
				{
					if(active_module.parent().attr('id') == 'col_left')
						active_module.appendTo($('#col_right'));
					else
						active_module.appendTo($('#col_left'));
					
					resize_col($('#col_right'));
					resize_col($('#col_left'));
					
					modules[active_module.attr('module_id')].parent = active_module.parent(); 
					
					$('#popup_module_video').hide();
				});
						
			});
		},
		
		unbindListeners: function()
		{
			this.module.unbind('dblclick');
		},
		
		getSaveData: function()
		{
			var ret = 
			{
				column: this.parent.attr('id'),
				top: parseInt(this.module.position().top),
				code: this.module.attr('code'),
				start: this.module.attr('start'),
				type: 'video',
			};
			
			return ret;
		},
		
	}
	
	return new_elem;
}