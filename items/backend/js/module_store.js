

function new_module_store(id, parent)
{
	var new_elem = 
	{
		module: null,
		type: 'store',
		text: '',
		id: id,
		parent: parent,
		
		
		// MODULE BASIC
		init: function(parent)
		{
			this.module = $('.module[module_id=' + this.id + ']');
			this.bindListeners();
			igniteDragging();
		},
		
		getPrototypeHTML: function()
		{
			var html = '<div module_id=' + this.id + ' class="module module_store has_placeholder" data-text="Enter data here" style="top: ' + (Math.ceil(resize_col(this.parent)/10) * 10) + 'px;"></div>';
			return html;
		},
		
		bindListeners: function()
		{
			this.module.dblclick(function()
			{
				active_module = $(this);
				
				$('#popup_module_store').find('.popup_cancel_button').unbind('click');
				$('#popup_module_store').find('.popup_save_button').unbind('click');
				$('#popup_module_store').find('.popup_delete_button').unbind('click');
				$('#popup_module_store').find('.popup_switch_button').unbind('click');

				$('#popup_module_store').find('textarea').val(active_module.find('.module_store_text').text());
				$('#popup_module_store').find('#module_store_product').val(active_module.find('.store_add').attr('magento_id'));
				
				$('#popup_module_store').show();
				
				$('#popup_module_store').find('.popup_save_button').click(function()
				{
					var html = '<div class="module_store_text">' + $('#module_store_editor').val() + '</div>';
					html += '<div class="module_store_button store_add" magento_id="' + $('#module_store_product option:selected').val() + '">Add to cart</div>';
					html += '<div class="module_store_button store_checkout">Checkout</div>';
					active_module.empty();
					active_module.append(html);
					resize_col(active_module.parent());
					$('#popup_module_store').hide();
				});
				
				$('#popup_module_store').find('.popup_cancel_button').click(function()
				{
					$('#popup_module_store').hide();
				});
				
				$('#popup_module_store').find('.popup_delete_button').click(function()
				{
					var parent = active_module.parent();
					modules.splice(active_module.attr('module_id'),1);
					active_module.remove();
					resize_col(parent);
					$('#popup_module_store').hide();
				});
				
				$('#popup_module_store').find('.popup_switch_button').click(function()
				{
					if(active_module.parent().attr('id') == 'col_left')
						active_module.appendTo($('#col_right'));
					else
						active_module.appendTo($('#col_left'));
					
					resize_col($('#col_right'));
					resize_col($('#col_left'));
					
					modules[active_module.attr('module_id')].parent = active_module.parent(); 
					
					$('#popup_module_store').hide();
				});
						
			});
		},
		
		unbindListeners: function()
		{
			this.module.unbind('dblclick');
		},
		
		getSaveData: function()
		{
			var ret = 
			{
				column: this.parent.attr('id'),
				top: parseInt(this.module.position().top),
				content: this.module.find('.module_store_text').text(),
				type: 'store',
				magento_id: this.module.find('.store_add').attr('magento_id'),
			};
			
			return ret;
		},
	
		load: function(properties)
		{
	
		},		
		
	}
	
	return new_elem;
}