

function new_module_image(id, parent)
{
	var new_elem = 
	{
		module: null,
		type: 'image',
		filename: '',
		id: id,
		parent: parent,
		
		
		// MODULE BASIC
		init: function(parent)
		{
			this.module = $('.module[module_id=' + this.id + ']');
			this.parent = this.module.parent();
			this.bindListeners();
			igniteDragging();
		},
		
		getPrototypeHTML: function()
		{
			var html = '<div module_id=' + this.id + ' class="module module_image" data-text="Enter text here" style="top: ' + (Math.ceil(resize_col(this.parent)/10) * 10) + 'px;"><img src="' + rootUrl + 'items/uploads/module_image/module_image_preview.png" /><input type="file"  id="module_img_upload_' + this.id + '" accept=".png,.jpg,.jpeg" uploadpath="items/uploads/module_image"></div>';
			return html;
		},
		
		bindListeners: function()
		{
			this.module.find('img').dblclick(function()
			{
				active_module = $(this).parent();
				if(modules[active_module.attr('module_id')].filename == '')
					$(this).parent().find('input').click();
				else
				{
					$('#popup_module_image').find('.popup_cancel_button').unbind('click');
					$('#popup_module_image').find('.popup_delete_button').unbind('click');
					$('#popup_module_image').find('.popup_switch_button').unbind('click');

					$('#popup_module_image').find('img').attr('src', rootUrl + 'items/uploads/module_image/' + modules[active_module.attr('module_id')].filename);
					$('#popup_module_image').show();
					
					$('#popup_module_image').find('.popup_cancel_button').click(function()
					{
						$('#popup_module_image').hide();
					});
					
					$('#popup_module_image').find('.popup_delete_button').click(function()
					{
						var parent = active_module.parent();
						modules.splice(active_module.attr('module_id'),1);
						active_module.remove();
						resize_col(parent);
						$('#popup_module_image').hide();
					});
					
					$('#popup_module_image').find('.popup_switch_button').click(function()
					{
						if(active_module.parent().attr('id') == 'col_left')
							active_module.appendTo($('#col_right'));
						else
							active_module.appendTo($('#col_left'));
						
						resize_col($('#col_right'));
						resize_col($('#col_left'));
						
						modules[active_module.attr('module_id')].parent = active_module.parent();
						
						$('#popup_module_image').hide();
					});
				}
			});
			
			this.module.find('input').change(function()
			{
				var uploadpath = $(this).attr('uploadpath');
				var xhr = new XMLHttpRequest();		
				var fd = new FormData;
				var files = this.files;
				
				fd.append('data', files[0]);
				fd.append('filename', files[0].name);
				fd.append('uploadpath', uploadpath);
				
				xhr.addEventListener('load', function(e) 
				{
					var ret = $.parseJSON(this.responseText);
					
					if(ret.success)
					{
						modules[active_module.attr('module_id')].filename = ret.filename;
						active_module.find('img').attr('src', rootUrl + 'items/uploads/module_image/' + ret.filename);
						resize_col(active_module.parent());
					}
					else
					{
						alert('Error while uploading');
					}
			    });
				
				xhr.open('post', rootUrl + 'entities/Item/upload_image');
				xhr.send(fd);
			});
		},
		
		unbindListeners: function()
		{

		},
		
		getSaveData: function()
		{
			var ret = 
			{
				column: this.parent.attr('id'),
				top: parseInt(this.module.position().top),
				fname: this.filename,
				type: 'image',
			};
			
			return ret;
		},
	
		load: function(properties)
		{
			this.setText(properties.content);
			this.setMarginBottom(properties.margin_bottom);
			this.setMarginTop(properties.margin_top);
			this.setFontColor(properties.font_color);
			this.setFontSize(properties.font_size);
			this.setTextAlign(properties.align);
			this.setSidebarImage(properties.right_side_img);
			this.setSidebarText(properties.right_side_img_text);
		},		
		
	}
	
	return new_elem;
}