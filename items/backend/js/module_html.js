

function new_module_html(id, parent)
{
	var new_elem = 
	{
		module: null,
		type: 'html',
		html_code: '',
		id: id,
		parent: parent,
		
		
		// MODULE BASIC
		init: function(parent)
		{
			this.module = $('.module[module_id=' + this.id + ']');
			this.bindListeners();
			igniteDragging();
		},
		
		getPrototypeHTML: function()
		{
			var html = '<div module_id=' + this.id + ' class="module module_text has_placeholder" style="top: ' + (Math.ceil(resize_col(this.parent)/10) * 10) + 'px;"><div class="module_html_overlay"></div><div data-text="Enter HTML here" class="module_html_content"></div></div>';
			return html;
		},
		
		bindListeners: function()
		{
			this.module.dblclick(function()
			{
				active_module = $(this);
				
				$('#popup_module_html').find('.popup_cancel_button').unbind('click');
				$('#popup_module_html').find('.popup_save_button').unbind('click');
				$('#popup_module_html').find('.popup_delete_button').unbind('click');
				$('#popup_module_html').find('.popup_switch_button').unbind('click');

				$('#popup_module_html').find('textarea').val(modules[active_module.attr('module_id')].html_code);
				$('#popup_module_html').show();
				
				$('#popup_module_html').find('.popup_save_button').click(function()
				{
					modules[active_module.attr('module_id')].html_code = $('#popup_module_html').find('textarea').val();
					active_module.find('.module_html_content').html($('#popup_module_html').find('textarea').val());
					resize_col(active_module.parent());
					$('#popup_module_html').hide();
				});
				
				$('#popup_module_html').find('.popup_cancel_button').click(function()
				{
					$('#popup_module_html').hide();
				});
				
				$('#popup_module_html').find('.popup_delete_button').click(function()
				{
					var parent = active_module.parent();
					modules.splice(active_module.attr('module_id'),1);
					active_module.remove();
					resize_col(parent);
					$('#popup_module_html').hide();
				});
				
				$('#popup_module_html').find('.popup_switch_button').click(function()
				{
					if(active_module.parent().attr('id') == 'col_left')
						active_module.appendTo($('#col_right'));
					else
						active_module.appendTo($('#col_left'));
					
					resize_col($('#col_right'));
					resize_col($('#col_left'));
					
					modules[active_module.attr('module_id')].parent = active_module.parent(); 
					
					$('#popup_module_html').hide();
				});
						
			});
		},
		
		unbindListeners: function()
		{
			this.module.unbind('dblclick');
		},
		
		getSaveData: function()
		{
			var ret = 
			{
				column: this.parent.attr('id'),
				top: parseInt(this.module.position().top),
				html: this.html_code,
				type: 'html',
			};
			
			return ret;
		},
	
		load: function(properties)
		{
	
		},		
		
	}
	
	return new_elem;
}