

function new_module_bulletpoint(id, parent)
{
	var new_elem = 
	{
		module: null,
		type: 'bulletpoint',
		text: '',
		id: id,
		parent: parent,
		
		
		// MODULE BASIC
		init: function(parent)
		{
			this.module = $('.module[module_id=' + this.id + ']');
			this.parent = this.module.parent();
			this.bindListeners();
			igniteDragging();
		},
		
		getPrototypeHTML: function()
		{
			var html = '<div module_id=' + this.id + ' class="module module_bulletpoint has_placeholder" data-text="Enter text here" style="top: ' + (Math.ceil(resize_col(this.parent)/10) * 10) + 'px;"></div>';
			return html;
		},
		
		bindListeners: function()
		{
			this.module.dblclick(function()
			{
				active_module = $(this);
				
				$('#popup_module_bulletpoint').find('.popup_cancel_button').unbind('click');
				$('#popup_module_bulletpoint').find('.popup_save_button').unbind('click');
				$('#popup_module_bulletpoint').find('.popup_delete_button').unbind('click');
				$('#popup_module_bulletpoint').find('.popup_switch_button').unbind('click');

				$('#popup_module_bulletpoint').find('textarea').val(active_module.find('.enum_body').html());
				$('#popup_module_bulletpoint').find('input[type="text"]').val(active_module.find('.enum_head span').html());
				$('#popup_module_bulletpoint').show();
				
				$('#popup_module_bulletpoint').find('.popup_save_button').click(function()
				{
					var html = '<div class="enum_head"><span>' + $('#popup_module_bulletpoint').find('input[type="text"]').val() + '</span></div>';
					html += '<div class="enum_body">' + $('#popup_module_bulletpoint').find('textarea').val() + '</div>';
					
					active_module.empty();
					active_module.append(html);
					
					resize_col(active_module.parent());
					$('#popup_module_bulletpoint').hide();
				});
				
				$('#popup_module_bulletpoint').find('.popup_cancel_button').click(function()
				{
					$('#popup_module_bulletpoint').hide();
				});
				
				$('#popup_module_bulletpoint').find('.popup_delete_button').click(function()
				{
					var parent = active_module.parent();
					modules.splice(active_module.attr('module_id'),1);
					active_module.remove();
					resize_col(parent);
					$('#popup_module_bulletpoint').hide();
				});
				
				$('#popup_module_bulletpoint').find('.popup_switch_button').click(function()
				{
					if(active_module.parent().attr('id') == 'col_left')
						active_module.appendTo($('#col_right'));
					else
						active_module.appendTo($('#col_left'));
					
					resize_col($('#col_right'));
					resize_col($('#col_left'));
					
					modules[active_module.attr('module_id')].parent = active_module.parent();
					
					$('#popup_module_bulletpoint').hide();
				});
						
			});
		},
		
		unbindListeners: function()
		{
			this.module.unbind('dblclick');
		},
		
		getSaveData: function()
		{
			var ret = 
			{
				column: this.parent.attr('id'),
				top: parseInt(this.module.position().top),
				header: this.module.find('.enum_head span').text(),
				content: this.module.find('.enum_body').html(),
				type: 'bulletpoint',
			};
			
			return ret;
		},
	
		load: function(properties)
		{
	
		},		
		
	}
	
	return new_elem;
}