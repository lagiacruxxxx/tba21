<?php
defined('BASEPATH') OR exit('No direct script access allowed');

define('TRIANGLE_TYPE_RIGHT', 0);
define('TRIANGLE_TYPE_TOPLEFT', 1);
define('TRIANGLE_TYPE_BOTLEFT', 2);


define('CONTINENT_COLLECTION', 0);
define('CONTINENT_PROGRAM', 1);
define('CONTINENT_PRODUCTION', 2);
define('CONTINENT_ACADEMY', 3);
define('CONTINENT_NOCONTINENT', 5);


define('TILE_TYPE_EMPTY', 0);
define('TILE_TYPE_CONTINENT_CENTER', 1);
define('TILE_TYPE_ITEM', 2);

define('ARTWORK_SEARCH_BY_ARTWORK', 0);
define('ARTWORK_SEARCH_BY_ARTIST', 1);

define('LANG_EN', 0);
define('LANG_DE', 1);

define('LISTVIEW_LIMIT', 12);

define('DETAIL_TYPE_IMAGE', 0);
define('DETAIL_TYPE_HTML', 1);

define('SPLASH_PROGRAM_LIST', -1);
define('SPLASH_COLLECTION_LIST', -2);
define('SPLASH_PRODUCTION_LIST', -3);
define('SPLASH_ACADMEY_LIST', -4);


$config['besc'] = false;
$config['continentNames'] = array(
    LANG_EN => array(
        CONTINENT_COLLECTION => 'COLLECTION',
        CONTINENT_PROGRAM => 'PROGRAM',
        CONTINENT_PRODUCTION => 'MEDIA',
        CONTINENT_ACADEMY => 'OCEAN',
    ),
    LANG_DE => array(
        CONTINENT_COLLECTION => 'SAMMLUNG',
        CONTINENT_PROGRAM => 'PROGRAMM',
        CONTINENT_PRODUCTION => 'MEDIA',
        CONTINENT_ACADEMY => 'OCEAN',
    )
);

$config['staticNames'] = array(
    LANG_EN => array(
        'foundation' => 'FOUNDATION',
        'press' => 'PRESS',
        'contact' => 'CONTACT',
        'search' => 'SEARCH',
        'hexview' => 'HEX VIEW',
        'listview' => 'LIST VIEW',
        'artists' => 'All artists',
        'search_button' => 'Search',
        'mobile_orientationcheck' =>'Please use your device in portrait mode to continue.',
        'partners' => 'SPONSORS & PARTNERS',
    ),
    LANG_DE => array(
        'foundation' => 'STIFTUNG',
        'press' => 'PRESSE',
        'contact' => 'KONTAKT',
        'search' => 'SUCHE',
        'hexview' => 'HEX ANSICHT',
        'listview' => 'LISTEN ANSICHT',
        'artists' => 'Alle Künstler',
        'search_button' => 'Suchen',
        'mobile_orientationcheck' =>'Verwenden Sie bitte die Portraitansicht um mit dem Kalender fortzufahren.',
        'partners' => 'SPONSOREN & PARTNER',
    )    
);


$config['settingsId'] = 1;
$config['shopSettingsId'] = 1;


define('SPECIAL_UTE_META_BAUER', 1);
define('SPECIAL_EPHEMEROPTERAE', 2);


define('ORDER_STATUS_CREATED', 0);
define('ORDER_STATUS_INITIATED', 1);
define('ORDER_STATUS_SUCCESS', 2);
define('ORDER_STATUS_CANCEL', 3);
define('ORDER_STATUS_FAILURE', 4);
define('ORDER_STATUS_PENDING', 5);
define('ORDER_STATUS_DELIVERY_SENT', 6);


define('SHIPMENT_TYPE_STANDARD', 0);
define('SHIPMENT_TYPE_EXPRESS', 1);

define('SHIPMENT_CLASS_LIGHT', 0);
define('SHIPMENT_CLASS_HEAVY', 1);
