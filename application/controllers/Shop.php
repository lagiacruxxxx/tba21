<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Shop extends MY_Controller 
{
    protected $shop = null;
    
    function __construct()
    {
        parent::__construct();
        
        require_once (APPPATH . 'libraries/MyShop.php');
        $this->shop = new MyShop();
    }    
    
    public function getCartItemCount()
    {
        echo json_encode(array(
            'success' => true,
            'count' => $this->shop->getCartItemCount()));
    }
    
    public function addToCart($productId)
    {
        $this->shop->addToCart($productId);
        $this->getCartItemCount();
    }

    public function removeFromCart($rowId)
    {
        $this->shop->removeFromCart($rowId);
        echo json_encode(array(
            'success' => true,
            'count' => $this->shop->getCartItemCount(),
            'total' => $this->shop->getCartTotal(),
        ));
    }
    
    public function updateItemCartQty()
    {
        $ret = $this->shop->updateItemCartQty($this->input->post('rowId'), $this->input->post('change'));
        echo json_encode(array(
            'success' => true,
            'rowid' => $ret['rowId'],
            'count' => $ret['count'],
            'price' => $ret['price'],
            'total' => $this->shop->getCartTotal(),
        ));
    }
    
    public function verifyCheckoutData()
    {
        $this->shop->verifyCheckoutData();
    }
    
    public function terms()
    {
        echo $this->shop->termsView();
    }
    
    public function createOrder()
    {
        $this->shop->createOrder();
    }
	
	public function startPayment()
	{
		$this->shop->initPayment();
	}
	
	public function confirm()
	{
		$this->shop->confirmPayment();
	}
	
	public function return_success()
	{
		$this->shop->successView();
	}
	
	public function return_cancel()
	{
		$this->shop->cancelView();
	}
	
	public function return_failure()
	{
		$this->shop->failureView();
	}
	
	public function return_pending()
	{
		$this->shop->pendingView();
	}
	
}