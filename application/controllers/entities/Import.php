<?php defined('BASEPATH') OR exit('No direct script access allowed');

require(APPPATH.'controllers/Backend.php');

class Import extends Backend 
{
    protected $db2 = null;
    
    function __construct()
    {
        parent::__construct();
        $this->_connectToOutsideDB();
        $this->load->model('entities/Import_model', 'em');
        $this->em->setDB($this->db2);
    }  
    
    protected function _connectToOutsideDB()
    {
        $this->db2 = $this->load->database('artwork', true);
    }
    
    
    public function import_item()
    {
        $data = array();
        
        $this->page('backend/import_item', $data);
    }

    
    public function searchArtwork()
    {
        $searchString = $this->input->post('searchString');
        $searchBy = $this->input->post('searchBy');
        $artworks = array();
        
        
        switch($searchBy)
        {
            case ARTWORK_SEARCH_BY_ARTWORK:
                $artworkList = $this->em->getArtworksByName($searchString);
                break;
            case ARTWORK_SEARCH_BY_ARTIST:
                $artists = array();
                foreach($this->em->getArtistsByName($searchString)->result_array() as $artist)
                {
                    $artists[] = $artist['id'];
                    
                }
                if(count($artists) > 0)
                    $artworkList = $this->em->getArtworksByArtists($artists);
                else
                    $artworkList = $this->em->getArtworksByArtists(array(-1));
                break;
        }
        
        
        foreach($artworkList->result() as $artwork)
        {
            $imported = $this->em->alreadyImported($artwork->id);
            $artworks[] = array(
                'id' => $artwork->id,
                'title' => $artwork->cleartitle,
                'production_date' => $artwork->production_date,
                'artist' => $artwork->name,
                'imported' => $imported->num_rows() == 0 ? 'NO' : 'YES', 
                'itemId' => $imported->num_rows() == 0 ? 'null' : $imported->row()->id,  
                'detailimg' => $imported->num_rows() == 0 ? 'null' : $imported->row()->detail_img,
            );
        }
        
        echo json_encode
        (
            array
            (
                'success' => true,
                'artworks' => $artworks
            )
        );
    }
    
    public function importArtwork()
    {
        $artworksString = $this->input->post('artworks');
        
        $artworks = explode(';', $artworksString);
        
        
        foreach($artworks as $artworkId)
        {
            
            $artwork = $this->em->getArtworkById($artworkId)->row();     
            $artist = $this->em->getArtistById($artwork->artist_id)->row();   

            $item = array(
                'artwork_id' => $artwork->id,
                'name' => $artwork->cleartitle,
                'continent_id' => CONTINENT_COLLECTION,
                'lang' => LANG_EN,
            );
            
            $item['header']  = '<span style="padding-right: ' . strlen($artist->clearname) . 'px;">' . $artist->clearname . '</span><br/>';
            $item['header'] .= '<span style="padding-right: ' . strlen($artwork->cleartitle . ', ' . $artwork->production_date) . 'px;">' . $artwork->cleartitle . ', ' . $artwork->production_date . '</span>';

            
            $content = $artwork->media_material_description != "" ? $artwork->clearmaterial . '<br>' : "";
            $content .= $artwork->dimensions_notes != "" ? $artwork->cleardimensions . '<br><br>' : "";
            
            if($this->em->getTextsByArtworkId($artworkId)->num_rows() > 0)
                $content .= strip_tags($this->em->getTextsByArtworkId($artworkId)->row()->clearcontent);
            
            $text = array(
                'content' => $content,
                'item_id' => '',
                'column_id' => 0,
                'top' => 0
            );
            
            $photo = $this->em->getDetailImgFromArtwork($artworkId);
            if($photo->num_rows() == 1)
            {
                $photo = $photo->row();
                $rnd = rand_string(12);
                
                $serverFile = time() . "_" . $rnd . ".jpg";
                
                $img = file_get_contents('http://www.tb-cms.org/data/artwork/' . $artworkId . '/' . $photo->id . '_artwork_detail.jpg');
                file_put_contents(getcwd() . '/items/uploads/detailimg/' . $serverFile, $img);
                
                $item['detail_img'] = $serverFile;
                $item['detail_img_credits'] = str_replace(array("\r\n", "\r", "\n"), "", $photo->clearcredits) . '  © ' . str_replace(array("\r\n", "\r", "\n"), "", $photo->clearphotographer);
                $item['detail_type'] = DETAIL_TYPE_IMAGE;                
            }
            else
            {
                $item['detail_img'] = 'image_upload_placeholder.png';
                $item['detail_img_credits'] = "";
                $item['detail_type'] = DETAIL_TYPE_IMAGE;                
            }
            
            $gallery = array();
            foreach($this->em->getGalleryImgFromArtwork($artworkId)->result() as $galleryitem)
            {
                $rnd = rand_string(12);
                
                $ext = pathinfo($galleryitem->fname, PATHINFO_EXTENSION);
                
                $serverFile = time() . "_" . $rnd . "." . $ext;
                
                $img = file_get_contents('http://www.tb-cms.org/data/artwork/' . $artworkId . '/' . $galleryitem->id . '_artwork_detail.jpg');
                file_put_contents(getcwd() . '/items/uploads/gallery/' . $serverFile, $img);
                
                $gallery[] = array(
                    'fname' => $serverFile,
                    'credits' => str_replace(array("\r\n", "\r", "\n"), "", $photo->clearcredits) . '  © ' . str_replace(array("\r\n", "\r", "\n"), "", $photo->clearphotographer),
                );
            }
            
            
            if($this->em->alreadyImported($artworkId)->num_rows() == 0)
                $this->import($item, $text, $gallery);
            else
                $this->update($artworkId, $item, $text, $gallery);
        }
        
        echo json_encode
        (
            array
            (
                'success' => true,
            )
        );
    }
    
    
    public function import($item, $text, $gallery)
    {
        $itemId = $this->em->insertArtwork($item);
        
        $text['item_id'] = $itemId;
        $this->em->insertTextModule($text);
        
        $batch = array();
        foreach($gallery as $gal)
        {
            $batch[] = array(
                'fname' => $gal['fname'],
                'item_id' => $itemId,
                'credits' => $gal['credits'],
            );
        }
        if(count($batch) > 0)
            $this->em->insertGalleryItems($batch);
    }
    
    public function update($artworkId, $item, $text, $gallery)
    {
        $this->em->updateArtwork($item, $artworkId);
        $itemId = $this->em->getItemByArtworkId($artworkId)->row()->id;
        
        $this->em->deleteModulesText($itemId);
        $text['item_id'] = $itemId;
        $this->em->insertTextModule($text);
        
        $batch = array();
        foreach($gallery as $gal)
        {
            $batch[] = array(
                'fname' => $gal['fname'],
                'item_id' => $itemId,
                'credits' => $gal['credits'],
            );
        }
        $this->em->deleteGalleryItems($itemId);
        if(count($batch) > 0)
            $this->em->insertGalleryItems($batch);
        
    }
    
    
    private function _enc($string)
    {
        $string = str_replace('âˆ…', 'Ø', $string);
        $string = str_replace('â€™', '\'', $string);
        $string = str_replace('â€“', '-', $string);
        $string = str_replace('Â°', '°', $string);
        
        if(mb_detect_encoding($string) == 'UTF-8')
        {
            //echo "not utf8" . $string;
            $string = mb_convert_encoding($string, 'UTF-8');
            //echo "not utf8" . $string;
        }
        //var_dump($string);*/
        return $string;
    }
    
    private function _stripAnchors($html)
    {
        $xml = new DOMDocument();
        $xml->loadHTML($html);
         
        $links = $xml->getElementsByTagName('a');
         
        //var_dump($links->item(0)->getAttribute('href'));
        //Loop through each <a> tags and replace them by their text content
        for ($i = $links->length - 1; $i >= 0; $i--)
        {
            if(strpos($links->item(0)->getAttribute('href'), 'tba21.org') !== false)
            {
                $linkNode = $links->item($i);
                $lnkText = $linkNode->textContent;
                $newTxtNode = $xml->createTextNode($lnkText);
                $linkNode->parentNode->replaceChild($newTxtNode, $linkNode);
            }
        }
         
        return $xml->textContent;
        //return $html;
    }
}

