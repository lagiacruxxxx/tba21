<?php defined('BASEPATH') OR exit('No direct script access allowed');

require(APPPATH.'controllers/Backend.php');

class Shop extends Backend 
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Shop_model', 'em');
    }  

	public function items()
	{
		$bc = new besc_crud();
		$bc->table('shop_item');
		$bc->primary_key('id');
		$bc->title('Shop items');
		
		
		
		$bc->columns(array
	    (
	        'name' => array(
                'db_name' => 'name',
	            'type' => 'text',
	            'display_as' => 'Name',
	            'validation' => 'required|max_length[255]|is_unique[shop_item.name]',
	        ),
	        
	        'image' => array(
	            'db_name' => 'image',
	            'type' => 'image',
	            'display_as' => 'Image',
	            'col_info' => 'filetypes: .png, .jpg, .jpeg<br/>150x150 px',
	            'accept' => '.png,.jpg,.jpeg',
	            'uploadpath' => 'items/uploads/shop',
	            'validation' => 'required',
	        ),
	        
	        'description' => array(
	            'db_name' => 'description',
	            'type' => 'multiline',
	            'display_as' => 'Description',
	            'height' => 200,
	        ),
	        
	        'price' => array(
	            'db_name' => 'price',
	            'type' => 'text',
	            'display_as' => 'Price',
	        ),
	        
	        'weight' => array(
	            'db_name' => 'weight',
	            'type' => 'text',
	            'display_as' => 'Weight',
	            'col_info' => 'in grams',
	        ),
	        
	        'in_stock' => array(
	            'db_name' => 'in_stock',
	            'type' => 'select',
	            'display_as' => 'In stock',
	            'options' => array(
                    array('key' => 0, 'value' => 'Out of stock'),
	                array('key' => 1, 'value' => 'Available'),
	            ),
	        ),
	        
            'mailtext_en' => array(
                'db_name' => 'mailtext_en',
                'type' => 'ckeditor',
                'display_as' => 'Mailtext EN',
                'height' => 200,
            ),	
            
            'mailtext_de' => array(
                'db_name' => 'mailtext_de',
                'type' => 'ckeditor',
                'display_as' => 'Mailtext DE',
                'height' => 200,
            ),			
		));
		
		$data['crud_data'] = $bc->execute();
		$this->page('backend/crud', $data);		
	}
	
	public function settings()
	{
	    if($this->uri->segment_array()[4] == 'list' || !isset($this->uri->segment_array()[4]))
	        redirect('backend/index');	    
	    
	    $bc = new besc_crud();
	    $bc->table('shop_settings');
	    $bc->primary_key('id');
	    $bc->title('Shop settings');
	
	
	    $bc->columns(array
        (
            'tos_de' => array(
                'db_name' => 'tos_de',
                'type' => 'ckeditor',
                'display_as' => 'Terms DE',
                'height' => 400,
            ),
            
            'tos_en' => array(
                'db_name' => 'tos_en',
                'type' => 'ckeditor',
                'display_as' => 'Terms EN',
                'height' => 400,
            ),
            
            
            'email_order_confirmation_de' => array(
                'db_name' => 'email_order_confirmation_de',
                'type' => 'ckeditor',
                'display_as' => 'Confirmation E-mail DE',
                'height' => 400,
            ),
             
            'email_order_confirmation_en' => array(
                'db_name' => 'email_order_confirmation_en',
                'type' => 'ckeditor',
                'display_as' => 'Confirmation E-mail EN',
                'height' => 400,
            ),
            
            'supportemail' => array(
                'db_name' => 'supportemail',
                'type' => 'text',
                'display_as' => 'Support E-mail',
                'validation' => 'required|valid_email',
            ),
        ));

	
	    $data['crud_data'] = $bc->execute();
	    $this->page('backend/crud', $data);
	}


	public function orders()
	{
		$bc = new besc_crud();
	    $bc->table('shop_order');
	    $bc->primary_key('id');
	    $bc->title('Orders');
	
		$orderstati = array(
			array('key' => ORDER_STATUS_CREATED, 'value' => 'CREATED'),
			array('key' => ORDER_STATUS_INITIATED, 'value' => 'INITIATED'),
			array('key' => ORDER_STATUS_SUCCESS, 'value' => 'PAYMENT SUCCESS'),
			array('key' => ORDER_STATUS_CANCEL, 'value' => 'CANCEL'),
			array('key' => ORDER_STATUS_FAILURE, 'value' => 'FAILURE'),
			array('key' => ORDER_STATUS_PENDING, 'value' => 'PENDING'),
			array('key' => ORDER_STATUS_DELIVERY_SENT, 'value' => 'DELIVERY SENT'),
		);
		
		$shipmenttypes = array(
			array('key' => SHIPMENT_TYPE_STANDARD, 'value' => 'STANDARD'),
			array('key' => SHIPMENT_TYPE_EXPRESS, 'value' => 'EXPRESS'),
		);
	
		$bc->list_columns(array('orderstatus', 'orderstatus_ts', 'firstname', 'lastname', 'email', 'shipmentType', 'paymentType', 'total'));
	
		$bc->filter_columns(array('orderstatus', 'shipmentType'));
	
		$bc->custom_buttons(array(
		    array(
		        'name' => 'Set status DELIVERY SENT',
                'icon' => site_url('items/backend/img/icon_delivery.png'),
                'add_pk' => true,
                'url' => 'setDelivered'),
                
		    array(
		        'name' => 'Items',
                'icon' => site_url('items/backend/img/icon_itemlist.png'),
                'add_pk' => true,
                'url' => 'itemlist'),				
		));
		
	    $bc->columns(array
        (
        
            
            'id' => array(
                'db_name' => 'id',
                'type' => 'text',
                'display_as' => 'Order number',
            ),
            		
            'orderstatus' => array(
                'db_name' => 'orderstatus',
                'type' => 'select',
                'display_as' => 'Status',
                'options' => $orderstati,
            ),
            
            'orderstatus_ts' => array(
                'db_name' => 'orderstatus_ts',
                'type' => 'text',
                'display_as' => 'last statuschange',
            ),
            
            
            'firstname' => array(
                'db_name' => 'firstname',
                'type' => 'text',
                'display_as' => 'Customer firstname',
            ),
            
            'lastname' => array(
                'db_name' => 'lastname',
                'type' => 'text',
                'display_as' => 'Customer lastname',
            ),
            
            'email' => array(
                'db_name' => 'email',
                'type' => 'text',
                'display_as' => 'E-mail',
            ),
            
            'phone' => array(
                'db_name' => 'phone',
                'type' => 'text',
                'display_as' => 'Phone',
            ),
            
            'street' => array(
                'db_name' => 'street',
                'type' => 'text',
                'display_as' => 'Street',
            ),
            
            'zip' => array(
                'db_name' => 'zip',
                'type' => 'text',
                'display_as' => 'ZIP',
            ),
            
			'city' => array(
                'db_name' => 'city',
                'type' => 'text',
                'display_as' => 'City',
            ),
            
			'country_id' => array(
                'db_name' => 'country_id',
                'type' => 'text',
                'display_as' => 'Country',
            ),
            
            'shipmentType' => array(
                'db_name' => 'shipmentType',
                'type' => 'select',
                'display_as' => 'Shipment',
                'options' => $shipmenttypes,
            ),
            
            'paymentType' => array(
                'db_name' => 'paymentType',
                'type' => 'text',
                'display_as' => 'Payment',
            ),
            
            'total' => array(
                'db_name' => 'total',
                'type' => 'text',
                'display_as' => 'Total amount (EUR)',
            ),
            
			'invoice' => array(
                'db_name' => 'invoice',
                'type' => 'text',
                'display_as' => 'Invoice',
            ),
            
        ));

	
	    $data['crud_data'] = $bc->execute();
	    $this->page('backend/crud', $data);		
	}

	public function setDelivered($orderId)
	{
		$this->em->updateOrderStatus($orderId, ORDER_STATUS_DELIVERY_SENT);
		redirect(site_url('entities/Shop/orders'));
	}
	
	public function itemlist($orderId)
	{
	    $bc = new besc_crud();
	    $bc->table('shop_order_item_view');
	    $bc->primary_key('id');
	    $bc->title("Items for Order $orderId");
		
		$bc->where("order_id = $orderId");
		
		$bc->unset_add();
		$bc->unset_edit();
		$bc->unset_delete();
		
	    $bc->columns(array
        (
            'name' => array(
                'db_name' => 'name',
                'type' => 'text',
                'display_as' => 'Name',
            ),
            
            'amount' => array(
                'db_name' => 'amount',
                'type' => 'text',
                'display_as' => 'Amount',
            ),
        ));
	
	    $data['crud_data'] = $bc->execute();
	    $this->page('backend/crud', $data);
	}
	
	public function shipment($shipmentType)
	{
	    $bc = new besc_crud();
	    $bc->table('shop_shipment');
	    $bc->primary_key('id');
	    $bc->title("Shipment costs " . $shipmentType == SHIPMENT_TYPE_STANDARD ? 'STANDARD' : 'EXPRESS');
		
		$bc->where("shipment_type = $shipmentType");
		
		$bc->unset_add();
		$bc->unset_delete();
		
	    $bc->columns(array
        (
            'shipment_type' => array(
                'db_name' => 'shipment_type',
                'type' => 'select',
                'display_as' => 'Shipment type',
                'options' => array(
                	array(
                		'key' => SHIPMENT_TYPE_STANDARD,
                		'value' => 'STANDARD',
					),
					array(
                		'key' => SHIPMENT_TYPE_EXPRESS,
                		'value' => 'EXPRESS',
					),
				),
            ),
            
            'shipment_class' => array(
                'db_name' => 'shipment_class',
                'type' => 'select',
                'display_as' => 'Shipment class',
                'options' => array(
                	array(
                		'key' => SHIPMENT_CLASS_LIGHT,
                		'value' => '<= 2kg',
					),
					array(
                		'key' => SHIPMENT_CLASS_HEAVY,
                		'value' => '> 2kg',
					),
				),
            ),
            
	        'austria' => array(
	            'db_name' => 'austria',
	            'type' => 'text',
	            'display_as' => 'within austria',
	            'col_info' => 'in EUR',
	        ),
	        
			'europe' => array(
	            'db_name' => 'europe',
	            'type' => 'text',
	            'display_as' => 'within EU',
	            'col_info' => 'in EUR',
	        ),
	        
			'worldwide' => array(
	            'db_name' => 'worldwide',
	            'type' => 'text',
	            'display_as' => 'rest of world',
	            'col_info' => 'in EUR',
	        ),
	        
	        
        ));
	
	    $data['crud_data'] = $bc->execute();
	    $this->page('backend/crud', $data);		
	}
	
}
