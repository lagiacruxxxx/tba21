<?php defined('BASEPATH') OR exit('No direct script access allowed');

require(APPPATH.'controllers/Backend.php');

class Menu extends Backend 
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('entities/Menu_model', 'em');
    }  

	public function tags($continentId)
	{
		$bc = new besc_crud();
		$bc->table('continent_metatag');
		$bc->primary_key('id');
		$bc->title('Menu metatag - ' . strtoupper($this->continentNames[LANG_EN][$continentId]));
		$bc->where('continent_id = ' . $continentId);
		
		$metatags = array();
		foreach($this->em->getMetatags($continentId)->result() as $tag)
		{
		    $metatags[] = array(
		        'key' => $tag->id,
		        'value' => $tag->name,
		    );
		}
		
		$bc->list_columns(array(
		    'metatag_id'
		));
		
		$bc->ordering(array(
            'ordering' => 'ordering',
		    'value' => 'metatag_id',
		));
		
		$bc->order_by_field('ordering');
		$bc->order_by_direction('asc');
		
		
		$bc->columns(array
	    (
	        'metatag_id' => array(
                'db_name' => 'metatag_id',
	            'type' => 'combobox',
	            'display_as' => 'Metatag',
	            'options' => $metatags,
	            'validation' => 'required',
	        ),
	        
	        'continent_id' => array(
	            'db_name' => 'continent_id',
	            'type' => 'hidden',
	            'value' => $continentId,
	        ),
		));
		
		$data['crud_data'] = $bc->execute();
		$this->page('backend/crud', $data);		
	}
	
}
