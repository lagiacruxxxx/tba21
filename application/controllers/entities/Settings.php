<?php defined('BASEPATH') OR exit('No direct script access allowed');

require(APPPATH.'controllers/Backend.php');

class Settings extends Backend 
{
    function __construct()
    {
        parent::__construct();
    }  

	public function settings()
	{
	    if($this->uri->segment_array()[4] == 'list' || !isset($this->uri->segment_array()[4]))
	        redirect('backend/index');
	    
		$bc = new besc_crud();
		$bc->table('settings');
		$bc->primary_key('id');
		$bc->title('Settings');
		
		$bc->unset_add();
		$bc->unset_delete();
		
		$categories = array(
		    array(
		        'key' => 0,
		        'value' => 'None', 		        
		));
		foreach($this->sm->getMetatagCategories()->result() as $cat)
		{
		    $categories[] = array(
		        'key' => $cat->id,
		        'value' => $cat->name
		    );
		}
		
		$items = array(
		    array(
		        'key' => 0,
		        'value' => 'None', 		        
		));
		foreach($this->sm->getItems()->result() as $item)
		{
		    $items[] = array(
		        'key' => $item->id,
		        'value' => $item->name
		    );
		}
		
		$bc->columns(array
	    (
	        'artist_metatag_category_id' => array
	        (  
	            'db_name' => 'artist_metatag_category_id',
				'type' => 'select',
				'display_as' => 'Artist metatag category',
	            'options' => $categories,
	        ),

	        'foundation_item_id' => array
	        (
	            'db_name' => 'foundation_item_id',
	            'type' => 'select',
	            'display_as' => 'Foundation item',
	            'options' => $items,
	            'col_info' => 'Select the english version',
	        ),
	         
	        'contact_item_id' => array
	        (
	            'db_name' => 'contact_item_id',
	            'type' => 'select',
	            'display_as' => 'Contact item',
	            'col_info' => 'Select the english version',
	            'options' => $items,
	        ),
	        
	        'partners_item_id' => array
	        (
	            'db_name' => 'partners_item_id',
	            'type' => 'select',
	            'display_as' => 'Partners & Sponsors item',
	            'col_info' => 'Select the english version',
	            'options' => $items,
	        ),

	        'newsletter_item_id' => array
	        (
	            'db_name' => 'newsletter_item_id',
	            'type' => 'select',
	            'display_as' => 'Newsletter item',
	            'col_info' => 'Select the english version',
	            'options' => $items,
	        ),	        
	         
		));
		
		$data['crud_data'] = $bc->execute();
		$this->page('backend/crud', $data);		
	}
	
	
	public function splash()
	{
	    $bc = new besc_crud();
	    $bc->table('splashpage');
	    $bc->primary_key('id');
	    $bc->title('Splashpages');
	

	    $active_options = array(
	        array(
	            'key' => 0,
	            'value' => 'No',
	        ),
	        array(
	            'key' => 1,
	            'value' => 'Yes',
	        )
	    );
	    
	    $items = array();
	    $items[] = array(
	        'key' => 0,
            'value' => 'None',
        );
	    $items[] = array(
	        'key' => SPLASH_PROGRAM_LIST,
	        'value' => 'Program List',
	    );
	    $items[] = array(
	        'key' => SPLASH_COLLECTION_LIST,
	        'value' => 'Collection List',
	    );
	    $items[] = array(
	        'key' => SPLASH_PRODUCTION_LIST,
	        'value' => 'Media List',
	    );
	    $items[] = array(
	        'key' => SPLASH_ACADMEY_LIST,
	        'value' => 'Ocean List',
	    );
	    foreach($this->sm->getBaseItems()->result() as $item)
	    {
	        $items[] = array(
	            'key' => $item->id,
	            'value' => $item->name
	        );
	    }
	
	    $bc->columns(array
	        (
	            'name' => array
	            (
	                'db_name' => 'name',
	                'type' => 'text',
	                'display_as' => 'Name',
	            ),
	
	            'item_id' => array
	            (
	                'db_name' => 'item_id',
	                'type' => 'select',
	                'display_as' => 'Splashpage item',
	                'options' => $items,
	            ),
	
	            'active' => array
	            (
	                'db_name' => 'active',
	                'type' => 'select',
	                'display_as' => 'Active',
	                'options' => $active_options,
	            ),
	            
	            'startdate' => array
	            (
	                'db_name' => 'startdate',
	                'type' => 'date',
	                'display_as' => 'Startdate',
	                'edit_format' => 'dd.mm.yy',
	                'list_format' => 'd.m.Y'
	            ),
	            
	            'enddate' => array
	            (
	                'db_name' => 'enddate',
	                'type' => 'date',
	                'display_as' => 'Enddate',
	                'edit_format' => 'dd.mm.yy',
	                'list_format' => 'd.m.Y'
	            ),
	
	        ));
	
	    $data['crud_data'] = $bc->execute();
	    $this->page('backend/crud', $data);
	}
	
	
	public function newsletter()
	{
	    $bc = new besc_crud();
	    $bc->table('newsletter');
	    $bc->primary_key('id');
	    $bc->title('Newsletter registrations');
	    
	    $bc->custom_actions(array(
	        array(
	            'name' => 'Export newsletter registrations',
	            'url' => site_url('entities/Settings/newsletter_export'),
	            'add_pk' => false,
	            'icon' => site_url('items/backend/img/export.png'),
	        ),
	    ));
	    
	    $bc->columns(array
	        (
	            'firstname' => array
	            (
	                'db_name' => 'firstname',
	                'type' => 'text',
	                'display_as' => 'Firstname',
	                'validation' => 'max_length[255]',
	            ),
	    
	            'lastname' => array
	            (
	                'db_name' => 'lastname',
	                'type' => 'text',
	                'display_as' => 'Lastname',
	                'validation' => 'max_length[255]',
	            ),
	    
	            'email' => array
	            (
	                'db_name' => 'email',
	                'type' => 'text',
	                'display_as' => 'E-mail',
	                'validation' => 'required|is_unique[newsletter.email]|valid_email',
	            ),
	    
	        ));
	    
	    $data['crud_data'] = $bc->execute();
	    $this->page('backend/crud', $data);
		    
	}
	
	public function newsletter_export()
	{
	    $this->load->helper('file');
	    $this->load->dbutil();
	    $this->load->helper('download');

	    header('Content-type: text/csv');
	    header('Content-disposition: attachment;filename=TBA21_newsletter_registrations_' . date('Y-m-d') . '.csv');
	    echo "sep=,\n";
	    echo $this->dbutil->csv_from_result($this->sm->getNewsletterRegistrations());
	}
	
}
