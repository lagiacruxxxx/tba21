<?php defined('BASEPATH') OR exit('No direct script access allowed');

require(APPPATH.'controllers/Backend.php');

class Item extends Backend 
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('entities/Item_model', 'em');
    }  

	public function items()
	{
		$bc = new besc_crud();
		$bc->table('item');
		$bc->primary_key('id');
		$bc->title('Items');
		
		$bc->custom_buttons(array(
		    array(
		        'name' => 'Edit item',
                'icon' => site_url('items/backend/img/icon_edit_article.png'),
                'add_pk' => true,
                'url' => 'edit_item'),
		    array(
		        'name' => 'Clone item',
		        'icon' => site_url('items/backend/img/icon_clone.png'),
		        'add_pk' => true,
		        'url' => 'clone_item'),
		    array(
		        'name' => 'Edit tetrahaedron',
		        'icon' => site_url('items/backend/img/icon_tetrahaedron.png'),
		        'add_pk' => true,
		        'url' => 'tetrahaedron'),		    
		));
		
		$bc->custom_buttons(array());
		$bc->list_columns(array(
		    'name', 'continent_id', 'lang','metatag_item_relation', 'show', 'teaser'
		));
		
		$bc->filter_columns(array('continent_id', 'name', 'metatag_item_relation', 'lang', 'teaser'));
		
		$metatag_category = array();
		
		$bc->order_by_field('id');
		$bc->order_by_direction('desc');
		
		$continents = array();
		foreach($this->continentNames[LANG_EN] as $key => $value)
		{
		    $continents[] = array('key' => $key, 'value' => $value);
		}
		$continents[] = array(
	        'key' => 5,
	        'value' => 'None',
	    );
		
		$itemoptions = array();
		$itemoptions[] = array(
		    'key' => 0,
		    'value' => 'No Parent'
		);
		foreach($this->em->getItems()->result() as $item)
		{
            $itemoptions[] = array(
                'key' => $item->id,
	            'value' => $item->name . ' - ' . ($item->lang == LANG_DE ? 'DE' : 'EN')
    	    );
		}
		
		$specialoptions = array();
		$specialoptions[] = array(
            'key' => 0,
	        'value' => 'no special',  
	    );
		    
		$specialoptions[] = array(
	        'key' => SPECIAL_UTE_META_BAUER,
	        'value' => 'UTE META BAUER',
		);
		
		$specialoptions[] = array(
		    'key' => SPECIAL_EPHEMEROPTERAE,
		    'value' => 'EPHEMEROPTERAE',
		);
		
		
		$langoptions = array(
		    array(
		        'key' => LANG_EN,
		        'value' => 'EN',
		    ),
		    array(
		        'key' => LANG_DE,
		        'value' => 'DE',
		    )
		);
		
		$bc->columns(array
	    (
	        'name' => array
	        (  
	            'db_name' => 'name',
				'type' => 'text',
				'display_as' => 'Name',
	            'validation' => 'required',
	        ),
	        
	        'prettyurl' => array
	        (
	            'db_name' => 'prettyurl',
	            'type' => 'text',
	            'display_as' => 'PrettyURL',
	            'validation' => 'required|is_unique[item.prettyurl]',
	        ),
	        
	        'continent_id' => array
	        (  
	            'db_name' => 'continent_id',
				'type' => 'select',
				'display_as' => 'Continent',
	            'options' => $continents,
	            'validation' => 'required',
	        ),
	        
	        'lang' => array
	        (
	            'db_name' => 'lang',
	            'type' => 'select',
	            'display_as' => 'Language',
	            'options' => $langoptions
	        ),
	        
	        'ordering_date' => array
	        (
	            'db_name' => 'ordering_date',
	            'type' => 'date',
	            'display_as' => 'Ordering date',
	            'edit_format' => 'dd.mm.yy',
	            'list_format' => 'd.m.Y',
	            'defaultvalue' => '01.01.2016',
	        ),
	        
	        'parent_item_id' => array
	        (
	            'db_name' => 'parent_item_id',
	            'type' => 'combobox',
	            'display_as' => 'English parent',
	            'options' => $itemoptions,
	        ),
	        
	        'metatag_item_relation' => array
	        (
	            'relation_id' => 'metatag_item_relation',
	            'type' => 'm_n_relation',
	            'table_mn' => 'metatag_item',
	            'table_mn_pk' => 'id',
	            'table_mn_col_m' => 'item_id',
	            'table_mn_col_n' => 'metatag_id',
	            'table_m' => 'item',
	            'table_n' => 'metatag',
	            'table_n_pk' => 'id',
	            'table_n_value' => 'name',
	            'display_as' => 'Metatags',
	            'box_width' => 400,
	            'box_height' => 300,
	            'filter' => true,
	        ),
	        
	        'show' => array
	        (
	            'db_name' => 'show',
	            'type' => 'select',
	            'display_as' => 'Show in frontend',
	            'options' => array(
	                array(
                        'key' => 1,
	                    'value' => 'Show'   
	                ),
	                array(
	                    'key' => 0,
	                    'value' => 'Hide',
	                )
	            ),
	        ),
	        
	        'teaser' => array
	        (
	            'db_name' => 'teaser',
	            'type' => 'select',
	            'display_as' => 'Show as teaser',
	            'options' => array(
	                array(
	                    'key' => 0,
	                    'value' => 'No',
	                ),
	                array(
	                    'key' => 1,
	                    'value' => 'Yes'
	                ),
	            ),
	        ),
	        
	        'special' => array
	        (
	            'db_name' => 'special',
	            'type' => 'select',
	            'display_as' => 'Special page',
	            'options' => $specialoptions,
	        ),
	        
	    ));
		$stateInfo = $bc->get_state_info_from_url();
		if($stateInfo->operation == 'edit' && $this->em->getItem($stateInfo->first_parameter)->row()->continent_id == CONTINENT_COLLECTION)
		    $this->checkCollectionAdmin();
		
		$data['crud_data'] = $bc->execute();
		$this->page('backend/crud', $data);
	}
	
	
	public function edit_item($itemId)
	{
	    $data = array();
	    
	    $data['item'] = $this->em->getItem($itemId)->row();
	    
	    if($data['item']->continent_id == CONTINENT_COLLECTION)
	        $this->checkCollectionAdmin();
	    
        
        $data['continentNames'] = $this->continentNames;
        
	    $data['metatagCategories'] = $this->bm->getMetatagCategories();
	    foreach($data['metatagCategories']->result() as $category)
	    {
	        $data['metatags'][$category->id] = $this->bm->getMetatagByCategory($category->id);
	    }
	    
	    $data['relatedMetatags'] = array();
	    foreach($this->em->getRelatedMetatags($itemId)->result() as $relatedTag)
	    {
	        if($relatedTag->relatedmetatag_id != NULL)
	        {
	            $metatag = $this->em->getMetatagById($relatedTag->relatedmetatag_id);
	            if($metatag->num_rows() == 1)
	            {
	                $data['relatedMetatags'][] = array('metatag_id' => $metatag->row()->id, 'url' => 'null', 'text' => $metatag->row()->name);
	            }
	        }
	        else
	        {
	            $data['relatedMetatags'][] = array('metatag_id' => 'null', 'url' => $relatedTag->external_url, 'text' => $relatedTag->external_text);
	        }
	    }
	    
	    $data['relatedItems'] = $this->em->getRelatedItems($itemId);
	    $data['modulesText'] = $this->em->getModulesText($itemId);
	    $data['modulesImage'] = $this->em->getModulesImage($itemId);
	    $data['modulesBulletpoint'] = $this->em->getModulesBulletpoint($itemId);
	    $data['galleryItems'] = $this->em->getGalleryItems($itemId);
	    $data['modulesVideo'] = $this->em->getModulesVideo($itemId);
	    $data['modulesHTML'] = $this->em->getModulesHTML($itemId);
	    $data['modulesHeadline'] = $this->em->getModulesHeadline($itemId);
	    $data['modulesDownload'] = $this->em->getModulesDownload($itemId);
	    $data['modules2ColImage'] = $this->em->getModules2ColImage($itemId);
	    $data['modulesStore'] = $this->em->getModulesStore($itemId);
	     
	    // GET MAGENTO PRODUCTS
	    require_once (APPPATH . 'libraries/MyShop.php');
	    $shop = new MyShop();
	    $data['store_products'] = $shop->getAllProductsForBackendSelect();
	     
	    $data['crud_data'] = $this->load->view('backend/edit_item', $data, true);
	    $this->page('backend/crud', $data);
	}
	
	public function upload_image()
	{
	    $this->load->helper('besc_helper');
	    
	    $filename = $_POST['filename'];
	    $upload_path = $_POST['uploadpath'];
        if(substr($upload_path, -1) != '/')
            $upload_path .= '/';
         
        $rnd = rand_string(12);
        $ext = pathinfo($filename, PATHINFO_EXTENSION);
        $serverFile = time() . "_" . $rnd . "." . $ext;
    
        $error = move_uploaded_file($_FILES['data']['tmp_name'], getcwd() . "/$upload_path/$serverFile");
    
	    echo json_encode(array('success' => true,
	        'path' => getcwd() . "/$upload_path/$serverFile",
	        'filename' => $serverFile));
	}
	
	public function uploadFile()
	{
	    $filename = $this->input->post('filename');
	    $upload_path = $this->input->post('uploadpath');
	    
	    $error = move_uploaded_file($_FILES['data']['tmp_name'], getcwd() . "/$upload_path/$filename");
	    
	    echo json_encode
	    (
	        array
	        (
	            'error' => $error,
	            'success' => true,
	            'filename' => $filename
	        )
	    );
	}
	
	public function getItemsPerMetatags()
	{
        
	    $items = array();
	    if(isset($_POST['metatags']))
	    {
    	    foreach($_POST['metatags'] as $metatag)
    	    {
    	        foreach($this->em->getOrigItemsByMetatag($metatag)->result() as $item)
    	        {
    	            $items[] = array(
    	                'id' => $item->id,
    	                'name' => $item->name,
    	                'detailimg' => $item->detail_img,   
    	            );
    	        }
    	    }
	    }
	    echo json_encode(
	        array(
	            'success' => true,
	            'items' => $items
                )
        );
	}
	
	
	public function save_item()
	{
	    $data = array(
	        'name' => $_POST['name'],
	        'detail_img' => $_POST['detail_img'],
	        'header' => $_POST['header'],
	        'detail_img_credits' => $_POST['detail_img_credits'],
	        'detail_type' => $_POST['detail_type'],
	        'detail_html' => $_POST['detail_html'],
	        'detail_html_mobile' => $_POST['detail_html_mobile']
	    );
	    $this->em->updateItemData($_POST['id'], $data);
	    
	    
	    $this->em->deleteRelatedItems($_POST['id']);
	    if(isset($_POST['related_items']))
	    {
	        $batch = array();
	        foreach($_POST['related_items'] as $related)
	        {
	            $batch[] = array(
	                'item_id' => $_POST['id'],
	                'relateditem_id' => $related
	            );
	        }
	        $this->em->insertRelatedItems($batch);
	    }
	    
	    
	    $this->em->deleteRelatedMetatags($_POST['id']);
	    if(isset($_POST['related_metatags']))
	    {
	        $batch = array();
	        foreach($_POST['related_metatags'] as $related)
	        {
	            $batch[] = array(
	                'item_id' => $_POST['id'],
	                'relatedmetatag_id' => $related[0] == 'null' ? NULL : $related[0],
	                'external_url' => $related[1] == 'null' ? NULL : $related[1],
	                'external_text' => $related[2] == 'null' ? NULL : $related[2],
	            );
	        }
	        $this->em->insertRelatedMetatags($batch);
	    }
	    
	    
	    $this->em->deleteModulesText($_POST['id']);
	    $this->em->deleteModulesImage($_POST['id']);
	    $this->em->deleteModulesBulletpoint($_POST['id']);
	    $this->em->deleteModulesVideo($_POST['id']);
	    $this->em->deleteModulesHTML($_POST['id']);
	    $this->em->deleteModulesHeadline($_POST['id']);
	    $this->em->deleteModulesDownload($_POST['id']);
	    $this->em->deleteModules2ColImage($_POST['id']);
	    $this->em->deleteModulesStore($_POST['id']);
	    if(isset($_POST['modules']))
	    {
	        foreach($_POST['modules'] as $module)
	        {
	            switch($module['type'])
	            {
	                case 'text':
	                    $this->em->insertModuleText(array(
                            'item_id' => $_POST['id'],
                            'column_id' => ($module['column'] == 'col_left') ? 0 : 1,
                            'top' => $module['top'],
                            'content' => $module['content'],
	                    ));
	                    break;
	                    
	                case 'image':
	                    $this->em->insertModuleImage(array(
	                        'item_id' => $_POST['id'],
	                        'column_id' => ($module['column'] == 'col_left') ? 0 : 1,
	                        'top' => $module['top'],
	                        'fname' => $module['fname'],
	                    ));
	                    break;
	                    
                    case 'bulletpoint':
                        $this->em->insertModuleBulletpoint(array(
                        'item_id' => $_POST['id'],
                        'column_id' => ($module['column'] == 'col_left') ? 0 : 1,
                        'top' => $module['top'],
                        'content' => $module['content'],
                        'header' => $module['header'],
                        ));
                        break;
                    case 'video':
                        $this->em->insertModuleVideo(array(
                        'item_id' => $_POST['id'],
                        'column_id' => ($module['column'] == 'col_left') ? 0 : 1,
                        'top' => $module['top'],
                        'code' => $module['code'],
                        'start' => $module['start'],
                        ));
                        break;
                        
                    case 'html':
                        $this->em->insertModuleHTML(array(
                        'item_id' => $_POST['id'],
                        'column_id' => ($module['column'] == 'col_left') ? 0 : 1,
                        'top' => $module['top'],
                        'html' => $module['html'],
                        ));
                        break;
                        
                    case 'headline':
                        $this->em->insertModuleHeadline(array(
                        'item_id' => $_POST['id'],
                        'column_id' => ($module['column'] == 'col_left') ? 0 : 1,
                        'top' => $module['top'],
                        'headline' => $module['headline'],
                        ));
                        break;
                        
                    case 'download':
                        $array = array(
                        'item_id' => $_POST['id'],
                        'column_id' => ($module['column'] == 'col_left') ? 0 : 1,
                        'top' => $module['top'],
                        'fname' => $module['fname'],
                        'description' => $module['description'],
                        );
                        if($array['fname'] == '')
                            unset($array['fname']);
                        $this->em->insertModuleDownload($array);
                        break;    

                    case '2col_image':
                        $this->em->insertModule2ColImage(array(
                            'item_id' => $_POST['id'],
                            'column_id' => ($module['column'] == 'col_left') ? 0 : 1,
                            'top' => $module['top'],
                            'fname' => $module['fname'],
                        ));
                        break;
                    case 'store':
                        $this->em->insertModuleStore(array(
                        'item_id' => $_POST['id'],
                        'column_id' => ($module['column'] == 'col_left') ? 0 : 1,
                        'top' => $module['top'],
                        'content' => $module['content'],
                        'magento_id' => $module['magento_id'],
                        ));
                        break;                        
	            }
	        }
	    }
	    
	    
        $this->em->deleteGalleryItems($_POST['id']);
	    if(isset($_POST['gallery_items']))
	    {
	        $batch = array();
	        foreach($_POST['gallery_items'] as $galleryItem)
	        {
	            $batch[] = array(
	                'item_id' => $_POST['id'],
	                'fname' => $galleryItem['filename'],
	                'credits' => $galleryItem['credits'], 
	            );
	        }
	        $this->em->insertGalleryItems($batch);
	    }
	    
	    echo json_encode(
	        array(
	            'success' => true,
	        )
	    );
	}
	
	
	public function clone_item($itemId)
	{
	    //clone item
	    $item = $this->em->getItem($itemId)->row_array();
	    $item['name'] = $item['name'] . ' (CLONE)';
	    unset($item['id']);
	    $newItemId = $this->em->cloneItem($item);
	    
	    
	    //galleryitems
	    $batch = array();
	    foreach($this->em->getGalleryItems($itemId)->result() as $galleryItem)
	    {
	        $batch[] = array(
	            'item_id' => $newItemId,
	            'fname' => $galleryItem->fname,
	        );
	    }
	    if(count($batch) > 0)
            $this->em->insertGalleryItems($batch);
	    
	    
	    //relatedMetatag
	    $batch = array();
	    foreach($this->em->getRelatedMetatagsById($itemId)->result() as $data)
	    {
	        $batch[] = array(
	            'item_id' => $newItemId,
	            'relatedmetatag_id' => $data->relatedmetatag_id,
	        );
	    }
	    if(count($batch) > 0)
	        $this->em->insertRelatedMetatags($batch);
	    
	    
	    //relatedItem
	    $batch = array();
	    foreach($this->em->getRelatedItemsById($itemId)->result() as $data)
	    {
	        $batch[] = array(
	            'item_id' => $newItemId,
	            'relateditem_id' => $data->relateditem_id,
	        );
	    }
	    if(count($batch) > 0)
	        $this->em->insertRelatedItems($batch);	    
	    
	    
	    //metatags
	    $batch = array();
	    foreach($this->em->getMetatagsByItemId($itemId)->result() as $data)
	    {
	        $batch[] = array(
	            'item_id' => $newItemId,
	            'metatag_id' => $data->metatag_id,
	        );
	    }
	    if(count($batch) > 0)
	        $this->em->insertMetatags($batch);	    
	    
	    
	    //bulletpoint module
	    foreach($this->em->getModulesBulletpoint($itemId)->result() as $data)
	    {
	        $batch = array(
	            'item_id' => $newItemId,
	            'column_id' => $data->column_id,
	            'top' => $data->top,
	            'header' => $data->header,
	            'content' => $data->content,
	        );
	        $this->em->insertModuleBulletpoint($batch);
	    }
	    
	    
	    //html module
	    foreach($this->em->getModulesHTML($itemId)->result() as $data)
	    {
	        $batch = array(
	            'item_id' => $newItemId,
	            'column_id' => $data->column_id,
	            'top' => $data->top,
	            'html' => $data->html,
	        );
	        $this->em->insertModuleHTML($batch);
	    }	    
	    
	    //image module
	    foreach($this->em->getModulesImage($itemId)->result() as $data)
	    {
	        $batch = array(
	            'item_id' => $newItemId,
	            'column_id' => $data->column_id,
	            'top' => $data->top,
	            'fname' => $data->fname,
	        );
	        $this->em->insertModuleImage($batch);
	    }
	    
	    //text module
	    foreach($this->em->getModulesText($itemId)->result() as $data)
	    {
	        $batch = array(
	            'item_id' => $newItemId,
	            'column_id' => $data->column_id,
	            'top' => $data->top,
	            'content' => $data->content,
	        );
	        $this->em->insertModuleText($batch);
	    }
	    
	    //video module
	    foreach($this->em->getModulesVideo($itemId)->result() as $data)
	    {
	        $batch = array(
	            'item_id' => $newItemId,
	            'column_id' => $data->column_id,
	            'top' => $data->top,
	            'start' => $data->start,
	            'code' => $data->code,
	        );
	        $this->em->insertModuleVideo($batch);
	    }
	    
	    //headline module
	    foreach($this->em->getModulesHeadline($itemId)->result() as $data)
	    {
	        $batch = array(
	            'item_id' => $newItemId,
	            'column_id' => $data->column_id,
	            'top' => $data->top,
	            'headline' => $data->headline,
	        );
	        $this->em->insertModuleHeadline($batch);
	    }	    
	    
	    redirect('entities/item/items', 'refresh');
	}
	
	
	public function tetrahaedron($itemId, $fname = "")
	{
	    if($this->em->getItem($itemId)->row()->continent_id == CONTINENT_COLLECTION)
	        $this->checkCollectionAdmin();
	    
	    $data = array();
	    
	    $data['item'] = $this->em->getItem($itemId)->row();
	    $data['fname'] = "";
	    
	    $this->page('backend/tetrahaedron', $data);
	}

    public function saveBlurMask()
    {
        $fname = $_POST['filename'];
        $itemId = $_POST['itemId'];
        $mask = $_POST['mask'];
        
        $img = file_get_contents('https://banner.manufakturfuerneuemedien.com/tba21/mask/' . $fname);
        file_put_contents(getcwd() . '/items/uploads/items/' . $fname, $img);

        $img = file_get_contents('https://banner.manufakturfuerneuemedien.com/tba21/blur/blur_' . $fname);
        file_put_contents(getcwd() . '/items/uploads/items/blur_' . $fname, $img);
        
        $img = file_get_contents('https://banner.manufakturfuerneuemedien.com/tba21/mask/mirror_' . $fname);
        file_put_contents(getcwd() . '/items/uploads/items/mirror_' . $fname, $img);
        
        $img = file_get_contents('https://banner.manufakturfuerneuemedien.com/tba21/blur/blur_mirror_' . $fname);
        file_put_contents(getcwd() . '/items/uploads/items/blur_mirror_' . $fname, $img);
        
        $data = array(
            'type' => $mask,
            'image' => $fname,
            'image_blurred' => 'blur_' . $fname,
            'mirror_image' => 'mirror_' . $fname,
            'mirror_image_blurred' => 'blur_mirror_' . $fname,
        );
        
        $this->em->updateItemImage($itemId, $data);
        
        echo json_encode('success');
    }
}

