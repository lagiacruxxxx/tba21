<?php defined('BASEPATH') OR exit('No direct script access allowed');

require(APPPATH.'controllers/Backend.php');

class Metatag extends Backend 
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('entities/Metatag_model', 'em');
    }  

	public function metatag()
	{
		$bc = new besc_crud();
		$bc->table('metatag');
		$bc->primary_key('id');
		$bc->title('Metatag');
		
		$metatag_category = array();
		foreach($this->em->getMetatagCategories()->result() as $category)
		{
		    $metatag_category[] = array(
		        'key' => $category->id,
		        'value' => $category->name,
		    );
		}
		
		$bc->filter_columns(array('name', 'metatag_category_id'));
		
		
		$bc->columns(array
	    (
	        'name' => array
	        (  
	            'db_name' => 'name',
				'type' => 'text',
				'display_as' => 'Name EN',
	            'validation' => 'required|is_unique[metatag.name]',
	        ),
	        
	        'name_de' => array
	        (
	            'db_name' => 'name_de',
	            'type' => 'text',
	            'display_as' => 'Name DE',
	            'validation' => 'required|is_unique[metatag.name_de]',
	        ),
	        
	        'metatag_category_id' => array(
                'db_name' => 'metatag_category_id',
	            'type' => 'select',
	            'display_as' => 'Category',
	            'options' => $metatag_category,
	        ),
	        
	        'ordering' => array
	        (
	            'db_name' => 'ordering',
	            'type' => 'text',
	            'display_as' => 'Ordering letter',
	            'validation' => 'max_length[50]|alpha',
	        ),
	        
		));
		
		$data['crud_data'] = $bc->execute();
		$this->page('backend/crud', $data);		
	}
	
	
	public function metatag_category()
	{
	    $bc = new besc_crud();
	    $bc->table('metatag_category');
	    $bc->primary_key('id');
	    $bc->title('Metatag category');
	    
	    $bc->columns(array
	        (
	            'name' => array
	            (
	                'db_name' => 'name',
	                'type' => 'text',
	                'display_as' => 'Name',
	                'validation' => 'required|is_unique[metatag_category.name]'
	            ),
	             
	        ));
	    
	    $data['crud_data'] = $bc->execute();
	    $this->page('backend/crud', $data);
	}
	
	
	public function regex_test()
	{
	    echo preg_match('/is_unique/', 'is_unique[metatag.name]');
	}

}
