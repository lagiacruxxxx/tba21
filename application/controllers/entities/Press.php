<?php defined('BASEPATH') OR exit('No direct script access allowed');

require(APPPATH.'controllers/Backend.php');

class Press extends Backend 
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('entities/Press_model', 'em');
    }  

	public function pressusers()
	{
		$bc = new besc_crud();
		$bc->table('pressuser');
		$bc->primary_key('id');
		$bc->title('Press users');
		
		$bc->unset_edit();
		
		$bc->custom_actions(array(
		    array(
		        'name' => 'Export press registrations',
		        'url' => site_url('entities/Press/registration_export'),
		        'add_pk' => false,
		        'icon' => site_url('items/backend/img/export.png'),
		    ),
		));
		
		$bc->columns(array
	    (
	        'firstname' => array
	        (  
	            'db_name' => 'firstname',
				'type' => 'text',
				'display_as' => 'Firstname',
	        ),

	        'lastname' => array
	        (
	            'db_name' => 'lastname',
	            'type' => 'text',
	            'display_as' => 'Lastname',
	        ),
	         
	        'publication' => array
	        (
	            'db_name' => 'publication',
	            'type' => 'text',
	            'display_as' => 'Publication',
	        ),
	         
	        'email' => array
	        (
	            'db_name' => 'email',
	            'type' => 'text',
	            'display_as' => 'E-Mail',
	        ),
	         
		));
		
		$data['crud_data'] = $bc->execute();
		$this->page('backend/crud', $data);		
	}
	
	
	public function presskits()
	{
	    $bc = new besc_crud();
	    $bc->table('presskit');
	    $bc->primary_key('id');
	    $bc->title('Press kits');
	    
	    foreach($this->sm->getItems()->result() as $item)
	    {
	        $items[] = array(
	            'key' => $item->id,
	            'value' => $item->name
	        );
	    }
	    
	    $active = array(
	        array(
	            'key' => 1,
	            'value' => 'Show',
	        ),
	        array(
	            'key' => 0,
	            'value' => 'Hide',
	        )
	    );
	    
	    $bc->columns(array
	        (
	            'name' => array
	            (
	                'db_name' => 'name',
	                'type' => 'text',
	                'display_as' => 'Name',
	                'validation' => 'required',
	            ),
	            
	            'name_de' => array
	            (
	                'db_name' => 'name_de',
	                'type' => 'text',
	                'display_as' => 'Name DE',
	                'validation' => 'required',
	            ),
	    
	            'year' => array
	            (
	                'db_name' => 'year',
	                'type' => 'date',
	                'display_as' => 'Date',
	                'validation' => 'required',
	                'edit_format' => 'dd.mm.yy',
                    'list_format' => 'd.m.Y',
                    'defaultvalue' => '01.01.2016',	                
	            ),
	           
	            'item_id' => array
	            (
	                'db_name' => 'item_id',
	                'type' => 'select',
	                'display_as' => 'Presskit item',
	                'col_info' => 'Select the english version',
	                'validation' => 'required',
	                'options' => $items,
	            ),
	            
	            'active' => array
	            (
	                'db_name' => 'active',
	                'type' => 'select',
	                'display_as' => 'Show',
	                'validation' => 'required',
	                'options' => $active,
	            ),	            
	    
	        ));
	    
	    $data['crud_data'] = $bc->execute();
	    $this->page('backend/crud', $data);
	}
	
	
	public function registration_export()
	{
	    $this->load->helper('file');
	    $this->load->dbutil();
	    $this->load->helper('download');
	    
	    header('Content-type: text/csv');
	    header('Content-disposition: attachment;filename=TBA21_press_registrations_' . date('Y-m-d') . '.csv');
	    echo "sep=,\n";
	    echo $this->dbutil->csv_from_result($this->em->getPressUsers());
	}
	
}
