<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Frontend extends MY_Controller 
{
    protected $canvasWidth = 40;
    protected $canvasHeight = 40;
    protected $itemWidth = 300;
    protected $itemHeight = 348;
    protected $items = array();
    protected $continents = array();
    protected $item_density = 1000;
    protected $smallItemOccurence = 75;
    protected $preselect = "";
    protected $shop = null;
    
    function __construct()
    {
        parent::__construct();
    
        $this->load->model('Frontend_model', 'fm');
        $this->_continentConfig();
        $this->load->helper('besc_helper');
        
        require_once (APPPATH . 'libraries/MyShop.php');
        $this->shop = new MyShop();
    }    
    
	public function index()
	{
        $data = array();
        
        $data['is_mobile'] = $this->is_mobile;
        $data['is_ipad'] = $this->is_ipad;
	     
	    $data['canvasWidth'] = $this->canvasWidth * $this->itemWidth;
	    $data['canvasHeight'] = ($this->canvasHeight * $this->itemHeight) /2;
	    $data['itemWidth'] = $this->itemWidth;
	    $data['itemHeight'] = $this->itemHeight;
	    $data['canvasCols'] = $this->canvasWidth;
	    $data['canvasRows'] = $this->canvasHeight;
        $data['settings'] = $this->settings;
	    
	    $data['menus'] = $this->_getContinentMenus();
	    $data['items'] = $this->_createWorld();
	    
	    $data['pressItemId'] = $this->config->item('pressItemId');
	    $data['contactItemId'] = $this->config->item('contactItemId');
	    $data['foundationItemId'] = $this->config->item('foundationItemId');
	    
	    $data['continentName'] = $this->continentNames;
	    $data['staticName'] = $this->staticNames;
	    $data['preselect'] = $this->preselect;
	    $data['language'] = $this->language;
	    $data['splashpage'] = $this->_getSplashPage();
	    
	    $data['show_cookie_warning'] = $this->input->cookie('tba21_cookie_accepted');
	     
	    $this->load->view('frontend/desktop', $data);
	}
	

	public function item($prettyurl)
	{
	    if(substr($prettyurl, 0, 1) != '#')
	    {
	        if(is_numeric($prettyurl))
	        {
	            $item = $this->fm->getItemById($prettyurl);
	            
	            if($item->num_rows() == 1)
	            {
	                $this->preselect = 'item--' . $item->row()->prettyurl . '--' . $item->row()->id;
                    if(!$this->_isCrawler())
                        redirect(site_url() . '#' . $this->preselect, 'auto');
                    else 
                        $this->load->view('frontend/share', array('item' => $item->row()));
	            }
	            else
	                $this->index();
	        }
	        else
	        {
    	        $item = $this->fm->getItemByPrettyUrl($prettyurl);
    	        //var_dump($item->result());
    	        if($item->num_rows() == 1)
                {
                    $this->preselect = 'item--' . $item->row()->prettyurl . '--' . $item->row()->id;
                    if(!$this->_isCrawler())
                        redirect(site_url() . '#' . $this->preselect, 'auto');
                    else 
                        $this->load->view('frontend/share', array('item' => $item->row()));
                }
                else
                    $this->index();
	        }
	    }
	    else
	        $this->index();
	}
	
	
	public function continent($continentId)
	{
	    switch($continentId)
	    {
	        case CONTINENT_ACADEMY:
	            $hash = '#ocean';
	            break;
	        case CONTINENT_COLLECTION:
	            $hash = '#collection';
	            break;
            case CONTINENT_PROGRAM:
                $hash = '#program';
                break;
            case CONTINENT_PRODUCTION:
                $hash = '#media';
                break;
            default:
                $hash = '';
                break;
	    }
        redirect(site_url() . $hash, 'auto');
	}
	
	
	private function _continentConfig()
	{
	    $this->continents[CONTINENT_COLLECTION] = array
	    (
	        'start_col' => 9,
	        'start_row' => 10,
	        'name' => strtoupper($this->continentNames[$this->language][CONTINENT_COLLECTION]),
	    );
	    $this->continents[CONTINENT_PROGRAM] = array
	    (
	        'start_col' => 24,
	        'start_row' => 12,
	        'name' => strtoupper($this->continentNames[$this->language][CONTINENT_PROGRAM])
	    );
	    $this->continents[CONTINENT_PRODUCTION] = array
	    (
	        'start_col' => 12,
	        'start_row' => 24,
	        'name' => strtoupper($this->continentNames[$this->language][CONTINENT_PRODUCTION])
	    );
	    $this->continents[CONTINENT_ACADEMY] = array
	    (
            'start_col' => 24,
	        'start_row' => 24,
	        'name' => strtoupper($this->continentNames[$this->language][CONTINENT_ACADEMY])
	    );
	}
	
	private function _createWorld()
	{
	    $this->_createRaster();
	    
	    $this->_createContinent(CONTINENT_COLLECTION);
	    $this->_createContinent(CONTINENT_PROGRAM);
	    $this->_createContinent(CONTINENT_ACADEMY);
	    $this->_createContinent(CONTINENT_PRODUCTION);
	    
	    $this->_createTeaser(CONTINENT_PROGRAM);
	    $this->_createTeaser(CONTINENT_ACADEMY);
	    $this->_createTeaser(CONTINENT_COLLECTION);
	    $this->_createTeaser(CONTINENT_PRODUCTION);
	     
	    return $this->_renderWorld();
	}
	
	private function _createRaster()
	{
	    $id = 0;
	    for($i = 0 ; $i < $this->canvasWidth ; $i++)
	    {
	        $this->items[$i] = array();
	        for($j = 0 ; $j < $this->canvasHeight ; $j++)
	        {
	            $this->items[$i][$j] = array
	            (
                    'id' => $id++,
                    'col' => $i,
	                'row' => $j,
	                'tile' => TILE_TYPE_EMPTY, 
	                'left' => $i * $this->itemWidth,
	                'top' => $j * ($this->itemHeight / 2),
	                'isQuad' => mt_rand(1,100) <= $this->smallItemOccurence ? true : false,
	            );
	        }
	    }
	}
	
	private function _getContinentMenus()
	{
	    $html = "";
	    
	    foreach($this->continents as $continentId => $continent)
	    {
	        foreach($this->fm->getMetatagsByContinent($continentId)->result() as $metatag)
	        {
	            $data['continentMenus'][$continentId]['menuItems'][] = $this->fm->getMetatagById($metatag->metatag_id)->row();
	        }
	    }
	    
	    $data['continents'] = $this->continents;
	    $data['continentName'] = $this->continentNames;
	    $data['staticName'] = $this->staticNames;
	    $data['language'] = $this->language;
	    $data['settings'] = $this->settings;
	    
	    
	    $data['cartCount'] = $this->shop->getCartItemCount();
	    
	    if(!$this->is_mobile)
	        return $this->load->view('frontend/continent_menu', $data, true);
	    else
	        return $this->load->view('frontend/continent_menu_mobile', $data, true);
	}
	
	private function _createTeaser($continentId)
	{
	    $items = $this->fm->getTeaserItemByContinentId($continentId);
	    
	    if($items->num_rows() > 0)
	    {
    	    $this->items[$continentId][100] = array
    	    (
    	        'id' => 1000 + $continentId,
    	        'col' => $continentId,
    	        'row' => 100,
    	        'tile' => TILE_TYPE_ITEM,
    	        'left' => $this->itemWidth * -10,
    	        'top' => $this->itemHeight * -10,
    	        'isQuad' => true,
            );
    	    
    	    foreach($items->result() as $item)
    	    {
    	        $metatags = $this->fm->getMetatagsPerItem(-1); // -1 because i dont want to find any metatags for the teaser
    	        $this->_setItemStats($continentId, 100, $item, $continentId, $metatags);
    	    }
	    }
	}
	
	private function _createContinent($continentId)
	{
	    $start_col = $this->continents[$continentId]['start_col'];
	    $start_row = $this->continents[$continentId]['start_row'];
	    
	    $this->items[$start_col][$start_row]['tile'] = TILE_TYPE_CONTINENT_CENTER;
	    $this->items[$start_col][$start_row]['continentId'] = $continentId;
	    
	    $items = $this->fm->getItemsPerContinent($continentId);
	    
    
	    foreach($items->result() as $item)
	    {
	        $positioned = false;
	        $radius = 1;
	        
	        $metatags = $this->fm->getMetatagsPerItem($item->id);
	            
	        while(!$positioned)
	        {
	            $x = $radius*-1 + 1;
	            $y = $radius;
	            while($x < $radius)
	            {
	                if($this->items[$start_col + $x][$start_row + $y]['tile'] == TILE_TYPE_EMPTY && mt_rand(1,100) < $this->item_density)
	                {
	                    $this->_setItemStats($start_col + $x, $start_row + $y, $item, $continentId, $metatags);
	                    $positioned = true;
                	    break;
	                }
	                
	                $x++;
	            }
	            if($positioned)
	                break;
	            
	            while($y > $radius*-1)
	            {
	                if($this->items[$start_col + $x][$start_row + $y]['tile'] == TILE_TYPE_EMPTY && mt_rand(1,100) < $this->item_density)
	                {
	                    $this->_setItemStats($start_col + $x, $start_row + $y, $item, $continentId, $metatags);
	                    $positioned = true;
	                    break;
	                }
	                 
	                $y--;
	            }
	            if($positioned)
	                break;
	            
	            while($x > $radius*-1)
	            {
	                if($this->items[$start_col + $x][$start_row + $y]['tile'] == TILE_TYPE_EMPTY && mt_rand(1,100) < $this->item_density)
	                {
	                    $this->_setItemStats($start_col + $x, $start_row + $y, $item, $continentId, $metatags);
	                    $positioned = true;
	                    break;
	                }
	            
	                $x--;
	            }	
	            if($positioned)
	                break;

	            while($y < $radius + 1)
	            {
	                if($this->items[$start_col + $x][$start_row + $y]['tile'] == TILE_TYPE_EMPTY && mt_rand(1,100) < $this->item_density)
	                {
	                    $this->_setItemStats($start_col + $x, $start_row + $y, $item, $continentId, $metatags);
	                    $positioned = true;
	                    break;
	                }
	            
	                $y++;
	            }	    
	            if($positioned)
	                break;
	            
	            $radius++;
	        }
	    }
	}
	
	private function _setItemStats($x, $y, $item, $continentId, $metatags)
	{
	    $germanItem = $this->getLanguageItem($item->id, LANG_DE);
	    if(!$this->items[$x][$y]['isQuad'])
	    {
	        $this->items[$x][$y]['tile'] = TILE_TYPE_ITEM;
    	    $this->items[$x][$y]['itemId'] = $item->id;
    	    $this->items[$x][$y]['img'] = $item->image;
    	    $this->items[$x][$y]['img_blurred'] = $item->image_blurred;
    	    $this->items[$x][$y]['mirror_img'] = $item->mirror_image;
    	    $this->items[$x][$y]['mirror_img_blurred'] = $item->mirror_image_blurred;
    	    $this->items[$x][$y]['imgType'] = $item->type;
    	    $this->items[$x][$y]['continentId'] = $continentId;
    	    $this->items[$x][$y]['metatags'] = $metatags;
    	    $this->items[$x][$y]['name_en'] = $this->_getItemTooltip($item->name, $item->id, LANG_EN, $continentId);
    	    if($germanItem != NULL)
                $this->items[$x][$y]['name_de'] = $this->_getItemTooltip($germanItem->name, $germanItem->id, LANG_DE, $continentId);
    	    else
    	        $this->items[$x][$y]['name_de'] = $this->_getItemTooltip($item->name, $item->id, LANG_EN, $continentId);
    	    $this->items[$x][$y]['header'] = $item->header;
	    }
	    else
	    {
	        if(isset($this->items[$x][$y]['quadCount']))
	            $this->items[$x][$y]['quadCount']++;
	        else
	            $this->items[$x][$y]['quadCount'] = 0;
	        
	        $count = $this->items[$x][$y]['quadCount'];
	        
	        $this->items[$x][$y]['itemId'][$count] = $item->id;
	        $this->items[$x][$y]['quadId'][$count] = $count;
	        $this->items[$x][$y]['img'][$count] = $item->image;
	        $this->items[$x][$y]['img_blurred'][$count] = $item->image_blurred;
	        $this->items[$x][$y]['mirror_img'][$count] = $item->mirror_image;
	        $this->items[$x][$y]['mirror_img_blurred'][$count] = $item->mirror_image_blurred;
	        $this->items[$x][$y]['imgType'][$count] = $item->type;
	        $this->items[$x][$y]['continentId'][$count] = $continentId;
	        $this->items[$x][$y]['metatags'][$count] = $metatags;
	        $this->items[$x][$y]['name_en'][$count] = $this->_getItemTooltip($item->name, $item->id, LANG_EN, $continentId);
	        if($germanItem != NULL)
	            $this->items[$x][$y]['name_de'][$count] = $this->_getItemTooltip($germanItem->name, $germanItem->id, LANG_DE, $continentId);
	        else 
	            $this->items[$x][$y]['name_de'][$count] = $this->_getItemTooltip($item->name, $item->id, LANG_EN, $continentId);
	        $this->items[$x][$y]['header'][$count] = $item->header;
	        
	        switch($count)
	        {
	            // LEFT TOP
	            case 0:
	                $this->items[$x][$y]['quadLeft'][$count] = 0;
	                $this->items[$x][$y]['quadTop'][$count] = 0;
	                break;
	                
	            // CENTER
	            case 1:
	                $this->items[$x][$y]['quadLeft'][$count] = 0;
	                $this->items[$x][$y]['quadTop'][$count] = intval($this->itemHeight / 4);
	                break;
	                
	            // RIGHT
	            case 2:
	                $this->items[$x][$y]['quadLeft'][$count] = intval($this->itemWidth / 2);
	                $this->items[$x][$y]['quadTop'][$count] = intval($this->itemHeight / 4);
	                break;
	                
                // BOTTOM LEFT
                case 3:
                    $this->items[$x][$y]['quadLeft'][$count] = 0;
                    $this->items[$x][$y]['quadTop'][$count] = intval($this->itemHeight / 2);
                    break;
                     
	        }
	        
	        if($this->items[$x][$y]['quadCount'] == 3)
	            $this->items[$x][$y]['tile'] = TILE_TYPE_ITEM;
	    }
	}
	
	private function _getItemTooltip($name, $itemId, $language, $continentId)
	{
	    $metatag = $this->fm->getArtistMetatagByItemId($itemId, $this->settings->artist_metatag_category_id);
	    $tooltip = $name . '<br/>';
	    $artists = "";
	    
	    if($continentId == CONTINENT_COLLECTION)
	    {
    	    foreach($metatag->result() as $artist)
    	    {
    	        if($artists != "")
    	            $artists .= ', ';
    	        if($language == LANG_EN)
    	            $artists .= $artist->name;
    	        else
    	            $artists .= $artist->name_de;
    	    }
	    }
	    
	    $tooltip .= $artists;

	    return $tooltip;
	}
	
	
	private function _renderWorld()
	{
	    $html = "";
	    
	    foreach($this->items as $row)
	    {
	        foreach($row as $item)
	        {
    	        switch($item['tile'])
    	        {
    	            
    	            case TILE_TYPE_CONTINENT_CENTER:
    	                $data['item'] = $item;
    	                $data['continentName'] = $this->continentNames; 
    	                $data['language'] = $this->language;
    	                $html .= $this->load->view('frontend/continent_center', $data, true);
    	                break;
    	                
    	            case TILE_TYPE_ITEM:
    	                $html .= $this->load->view('frontend/item', array('item' => $item), true);
    	                break;
    	                
                    case TILE_TYPE_EMPTY:
                        if(isset($item['quadCount']))
                            $html .= $this->load->view('frontend/item', array('item' => $item), true);
                        else
                            $html .= $this->load->view('frontend/empty', array('item' => $item), true);

                        break;
                    default:
                        break;	                
    	        }
	        }
	    }
	    
	    return $html;
	}
	
	
	public function detail($itemId)
	{
        if(($this->session->userdata('tba21_press_logged_in') != null && $this->_isItemPresskit($itemId)) || !$this->_isItemPresskit($itemId))
	    {
    	    $data['isSplash'] = $this->input->post('isSplash');
    	    if($itemId != $this->settings->foundation_item_id && $itemId != $this->settings->contact_item_id && !$data['isSplash'])
                $nav = $this->_getNavigation('item', $itemId);
    	    elseif($this->_isItemPresskit($itemId))
                $nav = $this->_getNavigation('presskit', null);
    	    else
    	        $nav = "";
    	    
    	    if($this->language != LANG_EN)
    	    {
                $langItem = $this->getLanguageItem($itemId, $this->language); 
                if($langItem != null)
                {
                    $data['item'] = $langItem;
                    $itemId = $langItem->id;
                }
                else
                    $data['item'] = $this->fm->getItemById($itemId)->row();
                
                $data['relatedItems'] = array();
                foreach($this->fm->getRelatedItems($itemId)->result() as $relatedItem)
                {
                    $langItem = $this->getLanguageItem($relatedItem->id, $this->language);
                    if($langItem != null)
                        $data['relatedItems'][] = $langItem;
                    else
                        $data['relatedItems'][] = $relatedItem;
                }
    	    }
    	    else
    	    {
    	        $data['item'] = $this->fm->getItemById($itemId)->row();
    	        if($data['item']->lang != LANG_EN)
    	        {
    	            $dummy2 = $this->fm->getItemById($data['item']->parent_item_id);
    	            $data['item'] = $dummy2->num_rows() == 1 ? $dummy2->row() : $data['item'];
    	            $itemId = $dummy2->num_rows() == 1 ? $dummy2->row()->id : $itemId; 
    	        }
    	        $data['relatedItems'] = array();
    	        foreach($this->fm->getRelatedItems($itemId)->result() as $relatedItem)
    	        {
    	            $data['relatedItems'][] = $relatedItem;
    	        }
    	    }
    	    
    	    $data['relatedMetatags'] = array();
    	    foreach($this->fm->getRelatedMetatags($itemId)->result() as $relatedTag)
    	    {
    	        if($relatedTag->relatedmetatag_id != NULL)
    	        {
    	            $metatag = $this->fm->getMetatagById($relatedTag->relatedmetatag_id);
    	            if($metatag->num_rows() == 1)
    	            {
    	                $data['relatedMetatags'][] = array('type' => 'metatag', 'data' => $metatag->row());
    	            }
    	        }
    	        else
    	        {
    	            $data['relatedMetatags'][] = array('type' => 'external', 'data' => $relatedTag);
    	        }
    	    }
    	    
    	    $data['module_text'] = $this->fm->getModuleTextByItemId($itemId);
            $data['module_image'] = $this->fm->getModuleImageByItemId($itemId);
            $data['module_bulletpoint'] = $this->fm->getModuleBulletpointByItemId($itemId);
            $data['module_video'] = $this->fm->getModuleVideoByItemId($itemId);
            $data['module_html'] = $this->fm->getModuleHTMLByItemId($itemId);
            $data['module_headline'] = $this->fm->getModuleHeadlineByItemId($itemId);
            $data['module_download'] = $this->fm->getModuleDownloadByItemId($itemId);
            $data['module_2col_image'] = $this->fm->getModule2ColImageByItemId($itemId);
            $data['module_store'] = $this->fm->getModuleStoreByItemId($itemId);
    	    
            $data['galleryItems'] = $this->fm->getGalleryItems($itemId);
            //$data['relatedMetatags'] = $this->fm->getRelatedMetatags($itemId);
            $data['language'] = $this->language;
            $data['is_mobile'] = $this->is_mobile;
            $data['cartcount'] = $this->shop->getCartItemCount();
            
            $data['modules'] = array_merge(array(), $data['module_text']->result_array());
            $data['modules'] = array_merge($data['modules'], $data['module_image']->result_array());
            $data['modules'] = array_merge($data['modules'], $data['module_bulletpoint']->result_array());
            $data['modules'] = array_merge($data['modules'], $data['module_video']->result_array());
            $data['modules'] = array_merge($data['modules'], $data['module_html']->result_array());
            $data['modules'] = array_merge($data['modules'], $data['module_headline']->result_array());
            $data['modules'] = array_merge($data['modules'], $data['module_download']->result_array());
            $data['modules'] = array_merge($data['modules'], $data['module_2col_image']->result_array());
            $data['modules'] = array_merge($data['modules'], $data['module_store']->result_array());
            
            usort($data['modules'], 'module_cmp');
            
            $html = $this->load->view('frontend/detailview', $data, true);
            
            echo json_encode(array(
                'success' => true,
                'html' => $html,
                'nav' => $nav,
                'prettyurl' => $data['item']->prettyurl,
                'trackerID' => $data['item']->name,
                'isSplash' => $data['isSplash'],
                'special' => $data['item']->special,
                'specialData' => $data['item']->special == 0 ? '' : $this->_special($data['item']->id),
            ));
	    }
	    else
	    {
	        echo json_encode(array(
	            'success' => true,
	            'html' => $this->load->view('frontend/press_nologin', array(), true),
	            'nav' => '',
	            'prettyurl' => 'press-nologin',
	            'trackerID' => '',
	            'isSplash' => true,
	            'special' => $data['item']->special,
                'specialData' => $data['item']->special == 0 ? '' : $this->_special($data['item']->id),
	        ));
	    }

	}
	
	
	public function listview()
	{
	    $listType = $this->input->post('listType');
	    $listId = $this->input->post('listId');
	    $params = $this->input->post('params');
	    
	    switch($listType)
	    {
	        case 'home':
	            $data['offset'] = $this->input->post('offset') == null ? 0 : $this->input->post('offset') + LISTVIEW_LIMIT;
	            $data['items'] = $this->fm->getListHome(LISTVIEW_LIMIT, $data['offset'])->result_array();
	            $data['headline'] = 'Home';
	            $data['prettyurl'] = 'home';
	            $html = $this->_createListView($data);
	            $data['nav'] = '';
	            $data['trackerData'] = '';
	            break;
	            
	        case 'cart':
	            $data['offset'] = 0;
                $html = $this->shop->cartView();
	            $data['prettyurl'] = 'cart';
	            $data['nav'] = '';
	            $data['trackerData'] = '';
	            break;
	            
            case 'checkout':
                $data['offset'] = 0;
                $html = $this->shop->checkoutView();
                $data['prettyurl'] = 'checkout';
                $data['nav'] = '';
                $data['trackerData'] = '';
                break;
                
            case 'shopConfirm':
                $data['offset'] = 0;
                $html = $this->shop->confirmView();
                $data['prettyurl'] = 'confirm';
                $data['nav'] = '';
                $data['trackerData'] = '';
                break;            
				
            case 'shopFinished':
                $data['offset'] = 0;
                $html = $this->shop->finishedView();
                $data['prettyurl'] = 'finished';
                $data['nav'] = '';
                $data['trackerData'] = '';
                break;              				    
	            
	        case 'metatag':
	            $data['offset'] = $this->input->post('offset') == null ? 0 : $this->input->post('offset') + LISTVIEW_LIMIT;
	            
	            $mtag = $this->fm->getMetatagById($listId)->row();
	            if($this->language == LANG_EN)
                    $data['headline'] = $mtag->name;
	            else
	                $data['headline'] = $mtag->name_de;
	            
	            $data['trackerData'] = $mtag->name;
	            
	            if(!$this->_isMetatagArtist($listId))
	            {
	                $data['items'] = $this->fm->getItemsByMetatagIdWithLimit($listId, LISTVIEW_LIMIT, $data['offset'])->result_array();
                    $html = $this->_createListView($data);
                    $data['prettyurl'] = 'tag--' . $this->fm->getMetatagById($listId)->row()->name . '--' . $listId;
                    $data['nav'] = '';
	            }
	            else
	            {
	                $skipToArtwork = false;
	                $all_items = array();
	                $dummy = $this->fm->getItemsByMetatagIdByContinentId($listId, CONTINENT_COLLECTION)->result_array();
	                $skipToArtwork = count($dummy) == 1 ? true : false;
	                if(count($dummy) > 0)
	                {
                        $all_items[] = array('type' => 'continent_headline', 'continentId' => CONTINENT_COLLECTION, 'continentName' => $this->continentNames[$this->language][CONTINENT_COLLECTION]);
	                    $all_items = array_merge($all_items, $dummy);
	                }
	                
	                
	                $dummy = $this->fm->getItemsByMetatagIdByContinentId($listId, CONTINENT_PROGRAM)->result_array();
	                $skipToArtwork = count($dummy) == 0 && $skipToArtwork ? true : false;
	                if(count($dummy) > 0)
	                {
	                    $all_items[] = array('type' => 'continent_headline', 'continentId' => CONTINENT_PROGRAM, 'continentName' => $this->continentNames[$this->language][CONTINENT_PROGRAM]);
	                    $all_items = array_merge($all_items, $dummy);
	                }
                        
	                
	                $dummy = $this->fm->getItemsByMetatagIdByContinentId($listId, CONTINENT_ACADEMY)->result_array();
	                $skipToArtwork = count($dummy) == 0 && $skipToArtwork ? true : false;
	                if(count($dummy) > 0)
	                {
                        $all_items[] = array('type' => 'continent_headline', 'continentId' => CONTINENT_ACADEMY, 'continentName' => $this->continentNames[$this->language][CONTINENT_ACADEMY]);
	                    $all_items = array_merge($all_items, $dummy);
	                }
	                
	                $dummy = $this->fm->getItemsByMetatagIdByContinentId($listId, CONTINENT_PRODUCTION)->result_array();
	                $skipToArtwork = count($dummy) == 0 && $skipToArtwork ? true : false;
	                if(count($dummy) > 0)
	                {
                        $all_items[] = array('type' => 'continent_headline', 'continentId' => CONTINENT_PRODUCTION, 'continentName' => $this->continentNames[$this->language][CONTINENT_PRODUCTION]);
	                    $all_items = array_merge($all_items, $dummy);
	                }

	                if(!$skipToArtwork)
	                {
	                    $results = array();
    	                for($i = $data['offset'] ; $i < $data['offset'] + LISTVIEW_LIMIT ; $i++)
    	                {
        	                if(isset($all_items[$i]))
    	                       $results[] = $all_items[$i];
    	                }
    	                
    	                $data['items'] = $results;
    	                $data['group_by_continents'] = true;
    	                
    	                $html = $this->_createListView($data);
    	                $data['nav'] = $this->_getNavigation('artist', null);
    	                $data['prettyurl'] = 'tag--' . $this->fm->getMetatagById($listId)->row()->name . '--' . $listId;
	                }
	                else
	                {
	                    $data['offset'] = $all_items[1]['id'];
	                    $data['nav'] = $all_items[1]['id'];
	                    $data['prettyurl'] = $all_items[1]['id'];
	                    $html = $all_items[1]['id'];
	                }
	                
	            }
	            break;
	            
	        case 'artist_list':
	            $data['artists'] = $this->fm->getArtists($this->settings->artist_metatag_category_id, 'ordering');
	            $data['break'] = ceil($data['artists']->num_rows() / 2);
	            $data['language'] = $this->language;
	            $data['headline'] = $this->staticNames[$this->language]['artists'];
	            $data['offset'] = 0;
	            $html = $this->load->view('frontend/artistsview', $data, true);
	            $data['prettyurl'] = 'artists';
	            $data['nav'] = '';
	            $data['trackerData'] = '';
	            break;    
	            
	        case 'continent':
	            $data['offset'] = $this->input->post('offset') == null ? 0 : $this->input->post('offset') + LISTVIEW_LIMIT;
	            $data['items'] = $this->fm->getItemsPerContinentWithLimit($listId, LISTVIEW_LIMIT, $data['offset'])->result_array();
                $data['headline'] = $this->continentNames[$this->language][$listId];
                $html = $this->_createListView($data);
                $data['prettyurl'] = 'continent--' . $this->continentNames[$this->language][$listId] . '--' . $listId;
                $data['nav'] = '';
                $data['trackerData'] = $this->continentNames[LANG_EN][$listId];
                break;
	        
	        case 'press':
	            $data['offset'] = 0;
	            if($this->session->userdata('tba21_press_logged_in') != null)
	                $html = $this->_presskits();
	            else
	                $html = $this->_press();
	            $data['prettyurl'] = 'press';
	            $data['nav'] = '';
	            $data['trackerData'] = '';
	            break;
	            
	        case 'search':
	            $data['offset'] = $this->input->post('offset') == null ? 0 : $this->input->post('offset') + LISTVIEW_LIMIT;
	            $data['items'] = $this->_getSearchResults($listId, $params, $data['offset']);
	            $data['headline'] = $this->lang->line('search_button') . ': ' . $listId;
	            $html = $this->_createListView($data);
	            $data['prettyurl'] = 'search--' . $listId;
	            $data['nav'] = '';
	            $data['trackerData'] = '';
	            break;    
	            
	        case 'search_empty':
	            $data['items'] = array();
	            $data['offset'] = 0;
	            $data['headline'] = $this->staticNames[$this->language]['search_button'];
	            $html = $this->_createListView($data);
	            $data['prettyurl'] = 'search';
	            $data['nav'] = '';
	            $data['trackerData'] = '';
	            break;
	    }
	    
        echo json_encode(array(
            'success' => true,
            'html' => $html,
            'append' => $data['offset'],
            'nav' => $data['nav'],
            'prettyurl' => $data['prettyurl'],
            'trackerData' => array('cat' => $listType, 'value' => $data['trackerData']), 
        ));
	}
	
	private function _getArtistName($itemId)
	{
	    $item = $this->fm->getItemById($itemId)->row();
	    $artist = $this->fm->getArtistMetatagByItemId($itemId, $this->settings->artist_metatag_category_id);
	    if($item->continent_id == CONTINENT_COLLECTION && $artist->num_rows() == 1)
	    {
	        if($this->language == LANG_EN)
	            $artistName = $artist->row()->name;
	        else
	            $artistName = $artist->row()->name_de;
	    }
	    else 
	        $artistName = '';
	    
	    return $artistName;
	}
	
	private function _isMetatagArtist($metatagId)
	{
	    return $this->fm->getMetatagById($metatagId)->row()->metatag_category_id == $this->settings->artist_metatag_category_id;
	}
	
	private function _isItemPresskit($itemId)
	{
	    $this->load->model('entities/Press_model', 'pm');
	    return $this->pm->isPresskit($itemId)->num_rows() >= 1 ? true : false;
	}
	
	private function _isSpecial($itemId)
	{
	    return $this->fm->getItemById($itemId)->row()->special != 0;
	}
	
	private function _getNavigation($type, $id)
	{
	    $nav = array();
	    switch($type)
	    {
	        case 'item':
	            $item = $this->fm->getItemById($id)->row();
	            if($item->continent_id != CONTINENT_NOCONTINENT)
	            {
    	            $nav[] = array(
    	                'key' => $item->continent_id,
    	                'value' => $this->continentNames[$this->language][$item->continent_id],
    	                'type' => 'continent',
    	            );
    	            if($item->continent_id == CONTINENT_COLLECTION && $this->fm->getArtistMetatagByItemId($id, $this->settings->artist_metatag_category_id)->num_rows() == 1)
    	            {
    	                $tag = $this->fm->getArtistMetatagByItemId($id, $this->settings->artist_metatag_category_id)->row();
    	                $nav[] = array(
    	                    'key' => $tag->id,
    	                    'value' => $this->language == LANG_EN ? $tag->name : $tag->name_de,
    	                    'type' => 'metatag',
    	                );
    	            }
	            }
	            break;
	            
	        case 'artist':
	            $nav[] = array(
	                'key' => CONTINENT_COLLECTION,
	                'value' => $this->continentNames[$this->language][CONTINENT_COLLECTION],
	                'type' => 'continent',
	            );
	            $nav[] = array(
	                'key' => '',
	                'value' => $this->staticNames[$this->language]['artists'],
	                'type' => 'artistlist',
	            );
	            break;
	            
	        case 'presskit':
	            $nav[] = array(
	                'key' => 0,
	                'value' => $this->lang->line('presskit_header'),
	                'type' => 'press',
	            );
	            break;
	    }
	    
	    $data['nav'] = $nav;
	    
	    return $this->load->view('frontend/navigation', $data, true);
	}
	
	private function _createListView($data, $returnItemArray = false)
	{
	    $this->load->helper('besc_helper');
	    $listItems = array();
	    foreach($data['items'] as $item)
	    {
	        // prevent continent separators
	        if(!isset($item['type']) || $item['type'] != 'continent_headline')
	        {
    	        if($this->language != LANG_EN)
    	        {
    	            $langItem = $this->getLanguageItem($item['id'], $this->language, true);
    	            if($langItem != null)
    	                $item = $langItem;
    	        }
    	        $text = $this->fm->getFirstTextModuleByItemId($item['id']);
    	        if($text->num_rows() != 1)
    	            $data['text'][$item['id']] = "";
    	        else
    	            $data['text'][$item['id']] = besc_trim($text->row()->content, 300);
    	        
    	        $data['artist'][$item['id']] = $this->_getArtistName($item['id']);
	        }
	        
	        $listItems[] = $item;
	    }
	    
	    $data['listitems'] = $listItems;
	    $data['dummy'] = date('H:m:s');
	    if(!isset($data['group_by_continents']))
	        $data['group_by_continents'] = false;

	    if(!$returnItemArray)
    	    return $this->load->view('frontend/listview', $data, true);
	    else 
	        return $data;    
	}
	
	private function _getSearchResults($string ,$params, $offset)
	{
	    $continents = explode(';', $params);
	    $all_items = array();
	    $result_ids = array();

	    $all_items = array_merge($all_items, $this->fm->searchItemByNameByContinents($continents, $string, $result_ids)->result_array());
	    
	    $result_ids = $this->_getSearchResultIds($all_items);
	    
	    foreach($this->fm->searchMetatagByName($string)->result() as $metatag)
	    {
	        $all_items = array_merge($all_items, $this->fm->searchItemsByMetatagIdByContinents($metatag->id, $continents, $result_ids)->result_array());
	    }
	    
	    $results = array();
	    
	    for($i = $offset ; $i < $offset + LISTVIEW_LIMIT ; $i++)
	    {
	        if(isset($all_items[$i]))
	            $results[] = $all_items[$i];
	    }
	    
	    return $results;
	}
	
	private function _getSearchResultIds($all_items)
	{
	    $all_ids = array();
	    
	    foreach($all_items as $item)
	    {
            $all_ids[] = $item['id'];   
	    }
	    
	    return $all_ids;
	}

	public function getLanguageItem($itemId, $language, $as_array = false)
	{
	    $langItem = $this->fm->getLanguageItem($itemId, $language);
	    if($langItem->num_rows() == 1)
	        if($as_array)
                return $langItem->row_array();
	        else
                return $langItem->row();
	    else
	        return null;
	}	
	
	
	
	/*************************************************************************************************************************************************
	 * PRESS KITS
	 ************************************************************************************************************************************************/
	
	private function _press()
	{
	    return $this->load->view('frontend/press', array(), true);
	}
	
	private function _presskits()
	{
	    $this->load->model('entities/Press_model', 'pm');
	    $data['pressuser'] = $this->pm->getPressuserById($this->session->userdata('tba21_press_logged_in'))->row();
	    $presskits = $this->pm->getPresskits()->result_array();
	    foreach($presskits as $key => $presskit)
	    {
	        $presskits[$key]['group'] = date('Y', strtotime($presskit['year']));
	    }
	    $data['breakpoint'] = ceil(count($presskits) / 2);
	    $data['language'] = $this->language;
	    
	    
	    
	    $data['presskits'] = $presskits;
	    return $this->load->view('frontend/presskits', $data, true);
	}
	
	
	/*************************************************
	 *  FIXING
	 *  **************************************************/
	
	public function fix_headers()
	{
        foreach($this->fm->getItemsPerContinent(CONTINENT_COLLECTION)->result() as $item)
        {
            $header = $item->header;
            
            $header = strip_tags($header, '<br>');
            
            $s = explode('<br/>', $header);
            $header = "";
            foreach($s as $str)
            {
                $header .= '<span style="padding-right: ' . strlen($str) . 'px;">' . $str . '</span><br>';
            }
            var_dump($s);
            
            $this->fm->updateItemHeaders(array(
                'header' => $header,
            ), $item->id);
        }
	}
	
	public function fix_text1()
	{
	    foreach($this->fm->getTextModuleForFix()->result() as $module)
	    {
	        /*var_dump($module->content);
	        echo '<br>';*/
	        
	        $data = array(
	            'content' => str_replace('â€', '"', $module->content)
	        );
	        
	        $this->fm->updateTextModuleFix($data, $module->id);
	    }
	    echo "finished";
	}
	
	public function fix_text2()
	{
	    foreach($this->fm->getTextModuleForFix2()->result() as $module)
	    {
	        $data = array(
	            'content' => str_replace('"œ', '"', $module->content)
	        );
	         
	        $this->fm->updateTextModuleFix($data, $module->id);
	    }
	    echo "finished";
	}
	 
	
	private function _getSplashPage()
	{

	    $date = strtotime(date('d.m.Y'));
	    
	    $activeSplash = array(
	        'type' => 'infinite',
	        'itemId' => null,
	    );
	    
	    foreach($this->fm->getActiveSplashpages()->result() as $sp)
	    {
	        if($sp->startdate != null && $sp->enddate != null)
	        {
	            if( $date >= strtotime($sp->startdate) && $date <= strtotime($sp->enddate))
	            {
	                $activeSplash['type'] = 'timed';
	                $activeSplash['itemId'] = $sp->item_id;
	            }
	        }
	        else
	        {
	            if($activeSplash['type'] != 'timed')
	            {
	                $activeSplash['type'] = 'infinite';
	                $activeSplash['itemId'] = $sp->item_id;
	            }
	        }
	    }
	    
	    if($activeSplash['itemId'] == null)
            $show = false;
	    else
	        $show = true;
	    
	    return array(
	        'itemId' => $activeSplash['itemId'],
	        'show' => $show,
	    );
	}
	
	/*************************************************************************************************************************************************
	 * SHARING
	 ************************************************************************************************************************************************/
	public function getItemForSharing($itemId)
	{
	    $item = $this->fm->getItemById($itemId);
	    if($item->num_rows() == 1)
	    {
	        $item = $item->row_array();
	        if($item['parent_item_id'] != 0 && $item['parent_item_id'] != null)
	        {
	            $item_orig = $this->fm->getItemById($item['parent_item_id']);
	            if($item_orig->num_rows() == 1)
	            {
	                $item = $item_orig->row_array();
	            }
	            else
	            {
	                $success = false;
	                $item = null;
	            }
	        }
	        $success = true;
	        
	        if($item['prettyurl'] == '')
	            $shareurl = site_url($item['id']);
	        else
	            $shareurl = site_url($item['prettyurl']);
	    }
	    else
	    {
	        $success = false;
	        $shareurl = '';
	    }
	    
	    echo json_encode(array(
            'success' => $success,
	        'url' => $shareurl,
	    ));
	        
	}
	
	private function _isCrawler()
	{
	    $ua = $_SERVER['HTTP_USER_AGENT'];
	    
	    if (in_array($_SERVER['HTTP_USER_AGENT'], array(
	        'facebookexternalhit/1.1 (+https://www.facebook.com/externalhit_uatext.php)',
	        'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)',
	        'facebookexternalhit/1.0 (+http://www.facebook.com/externalhit_uatext.php)',
	        'facebookexternalhit/1.0 (+https://www.facebook.com/externalhit_uatext.php)',
	        'facebookexternalhit/1.1',
	        'Facebot',
	    )))
	        return true;	        
	    else
	        return false;
	}
	
	
	
	public function cop21()
	{
	    $currentDate = date('m/d/Y H:i:s', time());
	    $triggerDate = date('12/07/2015 15:00:00');
	    
        $this->load->view('cop21/cop21');
	}
	
	private function _special($itemId)
	{
	    $item = $this->fm->getItemById($itemId)->row();
	    switch($item->special)
	    {
	        case SPECIAL_UTE_META_BAUER:
                $data = $this->load->view('special/ute_meta_bauer', array(), true);   
	            break;
	            
	        case SPECIAL_EPHEMEROPTERAE:
	            $data = 'https://www.tba21.org/#tag--Ephemeropteræ--23';
	            break;
	            
	        default:
	            $data = '';
	            break;
	    }
	    
	    return $data;
	}
}

