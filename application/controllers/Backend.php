<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Backend extends MY_Controller 
{
	protected $user;
	protected $base_url;
	protected $module_tables = array();
	

    function __construct()
    {
        parent::__construct();
        
        date_default_timezone_set('Europe/Vienna');
        
		if(!$this->logged_in())
			redirect('authentication/showLogin');
		
		$this->load->model('Authentication_model');
		$this->user = $this->Authentication_model->getUserdataByID($this->session->userdata('user_id'))->row();
		$this->load->library('Besc_crud');
		$this->load->model('Backend_model', 'bm');
    }  

	public function index()
	{
		$this->page('backend/home', array());
	}

	/***********************************************************************************
	 * DISPLAY FUNCTIONS
	 **********************************************************************************/	
	
    public function page($content_view, $content_data = array())
    {
    	$data = array();
		$data['username'] = $this->user->username;
		$data['additional_css'] = isset($content_data['additional_css']) ? $content_data['additional_css'] : array();
		$data['additional_js'] = isset($content_data['additional_js']) ? $content_data['additional_js'] : array();
		
		$this->load->view('backend/head', $data);
		$this->load->view('backend/menu', $data);
        $this->load->view($content_view, $content_data);
		$this->load->view('backend/footer', $data);
    }	
    
    /***********************************************************************************
     * COLLECTION ADMIN CHECKS
     **********************************************************************************/
    protected function checkCollectionAdmin()
    {
        if($this->user->collection_admin == 0)
        {
            redirect(site_url('backend/not_allowed'));
        }
    }
    
    public function not_allowed()
    {
        $this->page('backend/not_allowed', array());
    }
    
}
