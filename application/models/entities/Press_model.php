<?php

class Press_model extends CI_Model
{
    function getSettings($id)
    {
        $this->db->where('id', $id);
        return $this->db->get('settings');
    }
    
    function getItems()
    {
        $this->db->order_by('name', 'asc');
        $this->db->where('continent_id', 5);
        return $this->db->get('item');
    }
    
    function getPressuserById($id)
    {
        $this->db->where('id', $id);
        return $this->db->get('pressuser');
    }
    
    function getPresskits()
    {
        $this->db->from('presskit');
        $this->db->where('active', 1);
        $this->db->order_by('year', 'desc');
        return $this->db->get();
    }
    
    function isPresskit($itemId)
    {
        $this->db->where('item_id', $itemId);
        return $this->db->get('presskit');
    }
    
    function getPressUsers()
    {
        return $this->db->get('pressuser');
    }
}

?>