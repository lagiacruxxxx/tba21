<?php

class Settings_model extends CI_Model
{
    function getSettings($id)
    {
        $this->db->where('id', $id);
        return $this->db->get('settings');
    }
    
    function getMetatagCategories()
    {
        $this->db->order_by('name', 'asc');
        return $this->db->get('metatag_category');
    }
    
    function getItems()
    {
        $this->db->order_by('name', 'asc');
        $this->db->where('continent_id', 5);
        return $this->db->get('item');
    }
    
    function getBaseItems()
    {
        $this->db->where('(parent_item_id IS NULL OR parent_item_id = 0)');
        $this->db->order_by('name', 'asc');
        return $this->db->get('item');
    }
    
    
    function getNewsletterRegistrations()
    {
        return $this->db->get('newsletter');
    }
    
}

?>