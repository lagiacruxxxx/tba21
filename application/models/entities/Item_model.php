<?php

class Item_model extends CI_Model
{
    function getMetatagsByContinent($continentId)
    {
        $this->db->where('continent_id', $continentId);
        return $this->db->get('metatag');
    }
    
    
    
    function updateItemData($id, $data)
    {
        $this->db->where('id', $id);
        $this->db->update('item', $data);
    }
    
    function getItem($itemId)
    {
        $this->db->where('id', $itemId);
        return $this->db->get('item');
    }
    
    function getItems()
    {
        $this->db->where('parent_item_id IS NULL');
        $this->db->or_where('parent_item_id', 0);
        $this->db->order_by('name', 'asc');
        return $this->db->get('item');
    }
    
    function getMetatagById($metatagId)
    {
        $this->db->where('id', $metatagId);
        return $this->db->get('metatag');
    }
    
    
    function deleteRelatedItems($itemId)
    {
        $this->db->where('item_id', $itemId);
        $this->db->delete('item_relateditem');
    }
    
    function insertRelatedItems($batch)
    {
        $this->db->insert_batch('item_relateditem', $batch);
    }
    
    function getRelatedItems($itemId)
    {
        $this->db->select('item.id, item.detail_img, item.name');
        $this->db->from('item_relateditem');
        $this->db->join('item', 'item.id = item_relateditem.relateditem_id');
        $this->db->where('item_id', $itemId);
        return $this->db->get();
    }
    
    function getRelatedItemsById($itemId)
    {
        $this->db->where('item_id', $itemId);
        return $this->db->get('item_relateditem');
    }
    
    function deleteRelatedMetatags($itemId)
    {
        $this->db->where('item_id', $itemId);
        $this->db->delete('item_relatedmetatag');
    }
    
    function insertRelatedMetatags($batch)
    {
        $this->db->insert_batch('item_relatedmetatag', $batch);
    }
    
    function getRelatedMetatags($itemId)
    {
        //$this->db->select('metatag.id, metatag.name');
        $this->db->where('item_id', $itemId);
        //$this->db->join('metatag', 'metatag.id = item_relatedmetatag.relatedmetatag_id');
        return $this->db->get('item_relatedmetatag');
    }    
    
    function getRelatedMetatagsById($itemId)
    {
        $this->db->where('item_id', $itemId);
        return $this->db->get('item_relatedmetatag');
    }
    
    
    
    
    
    function deleteModulesText($itemId)
    {
        $this->db->where('item_id', $itemId);
        $this->db->delete('module_text');
    }
    
    function insertModuleText($data)
    {
        $this->db->insert('module_text', $data);
    }
    
    function getModulesText($itemId)
    {
        $this->db->where('item_id', $itemId);
        return $this->db->get('module_text');
    }
    
    function deleteModulesImage($itemId)
    {
        $this->db->where('item_id', $itemId);
        $this->db->delete('module_image');
    }
    
    function insertModuleImage($data)
    {
        $this->db->insert('module_image', $data);
    }
    
    function getModulesImage($itemId)
    {
        $this->db->where('item_id', $itemId);
        return $this->db->get('module_image');
    }
    
    function deleteModulesBulletpoint($itemId)
    {
        $this->db->where('item_id', $itemId);
        $this->db->delete('module_bulletpoint');
    }
    
    function insertModuleBulletpoint($data)
    {
        $this->db->insert('module_bulletpoint', $data);
    }
    
    function getModulesBulletpoint($itemId)
    {
        $this->db->where('item_id', $itemId);
        return $this->db->get('module_bulletpoint');
    }    
    
    function deleteModulesVideo($itemId)
    {
        $this->db->where('item_id', $itemId);
        $this->db->delete('module_video');
    }
    
    function insertModuleVideo($data)
    {
        $this->db->insert('module_video', $data);
    }
    
    function getModulesVideo($itemId)
    {
        $this->db->where('item_id', $itemId);
        return $this->db->get('module_video');
    }
    
    function deleteModulesHTML($itemId)
    {
        $this->db->where('item_id', $itemId);
        $this->db->delete('module_html');
    }
    
    function insertModuleHTML($data)
    {
        $this->db->insert('module_html', $data);
    }
    
    function getModulesHTML($itemId)
    {
        $this->db->where('item_id', $itemId);
        return $this->db->get('module_html');
    }
    
    function deleteModulesHeadline($itemId)
    {
        $this->db->where('item_id', $itemId);
        $this->db->delete('module_headline');
    }
    
    function insertModuleHeadline($data)
    {
        $this->db->insert('module_headline', $data);
    }
    
    function getModulesHeadline($itemId)
    {
        $this->db->where('item_id', $itemId);
        return $this->db->get('module_headline');
    }
        
    
    function deleteModulesDownload($itemId)
    {
        $this->db->where('item_id', $itemId);
        $this->db->delete('module_download');
    }
    
    function insertModuleDownload($data)
    {
        $this->db->insert('module_download', $data);
    }
    
    function getModulesDownload($itemId)
    {
        $this->db->where('item_id', $itemId);
        return $this->db->get('module_download');
    }    
    
    
    function deleteModules2ColImage($itemId)
    {
        $this->db->where('item_id', $itemId);
        $this->db->delete('module_2col_image');
    }
    
    function insertModule2ColImage($data)
    {
        $this->db->insert('module_2col_image', $data);
    }
    
    function getModules2ColImage($itemId)
    {
        $this->db->where('item_id', $itemId);
        return $this->db->get('module_2col_image');
    }
    
    function deleteModulesStore($itemId)
    {
        $this->db->where('item_id', $itemId);
        $this->db->delete('module_store');
    }
    
    function insertModuleStore($data)
    {
        $this->db->insert('module_store', $data);
    }
    
    function getModulesStore($itemId)
    {
        $this->db->where('item_id', $itemId);
        return $this->db->get('module_store');
    }
    
    
    
    
    
    function deleteGalleryItems($itemId)
    {
        $this->db->where('item_id', $itemId);
        $this->db->delete('item_galleryitem');
    }
    
    function insertGalleryItems($data)
    {
        $this->db->insert_batch('item_galleryitem', $data);
    }
    
    function getGalleryItems($itemId)
    {
        $this->db->where('item_id', $itemId);
        return $this->db->get('item_galleryitem');
    }  
    
    
    function cloneItem($data)
    {
        $this->db->insert('item', $data);
        return $this->db->insert_id();
    }
    
    function getMetatagsByItemId($itemId)
    {
        $this->db->where('item_id', $itemId);
        return $this->db->get('metatag_item');
    }
    
    function insertMetatags($data)
    {
        $this->db->insert_batch('metatag_item', $data);
    }
    
    
    function updateItemImage($itemId, $data)
    {
        $this->db->where('id', $itemId);
        $this->db->update('item', $data);
    }
    
    
    function getOrigItemsByMetatag($metatagId)
    {
        $this->db->select('item.id, item.name, item.detail_img');
        $this->db->from('metatag_item');
        $this->db->where('metatag_id', $metatagId);
        $this->db->where('(parent_item_id IS NULL OR parent_item_id = 0)');
        $this->db->join('item', 'item.id = metatag_item.item_id');
        $this->db->order_by('item.name', 'asc');
        return $this->db->get();        
    }
}

?>