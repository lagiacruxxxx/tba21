<?php

class Import_model extends CI_Model
{
    protected $db2 = null;
    
    public function setDB($db)
    {
        $this->db2 = $db;
    }
    
    function getArtworksByName($name)
    {
        $this->db2->select('artworks.id, artworks.title, artworks.production_date, artworks.artist_id, CONVERT(CAST(CONVERT(artists.name USING latin1) AS BINARY) USING utf8) as name, CONVERT(CAST(CONVERT(artworks.title USING latin1) AS BINARY) USING utf8) as cleartitle');
        $this->db2->like('title', $name);
        $this->db2->from('artworks');
        $this->db2->join('artists', 'artworks.artist_id = artists.id');
        return $this->db2->get();
    }
    
    function getArtistsByName($name)
    {
        //$this->db2->like('name', $name);
        $sql = 'select id from artists where CONVERT(CAST(CONVERT(name USING latin1) AS BINARY) USING utf8) like ?';
        return $this->db2->query($sql, array('%' . $name . '%'));
        //return $this->db2->last_query();
        //return $this->db2->get('artists');
    }
    
    function getArtistById($artistId)
    {
        $this->db2->select('*, CONVERT(CAST(CONVERT(name USING latin1) AS BINARY) USING utf8) as clearname');
        $this->db2->where('id', $artistId);
        return $this->db2->get('artists');
    }
    
    function getArtworksByArtists($artists)
    {
        $this->db2->select('artworks.id, artworks.title, artworks.production_date, artworks.artist_id, CONVERT(CAST(CONVERT(artists.name USING latin1) AS BINARY) USING utf8) as name, CONVERT(CAST(CONVERT(artworks.title USING latin1) AS BINARY) USING utf8) as cleartitle');
        $this->db2->from('artworks');
        $this->db2->join('artists', 'artworks.artist_id = artists.id');
        $this->db2->where_in('artists.id', $artists);
        return $this->db2->get();
    }
    
    function alreadyImported($id)
    {
        $this->db->where('artwork_id', $id);
        return $this->db->get('item');
    }
    
    function getArtworkById($artwork_id)
    {
        $this->db2->select('*, CONVERT(CAST(CONVERT(title USING latin1) AS BINARY) USING utf8) as cleartitle, CONVERT(CAST(CONVERT(media_material_description USING latin1) AS BINARY) USING utf8) as clearmaterial, CONVERT(CAST(CONVERT(dimensions_notes USING latin1) AS BINARY) USING utf8) as cleardimensions');
        $this->db2->where('id', $artwork_id);
        return $this->db2->get('artworks');
    }
    
    function insertArtwork($item)
    {
        $this->db->insert('item', $item);
        return $this->db->insert_id();
    }
    
    function getTextsByArtworkId($artworkId)
    {
        $this->db2->select('*, CONVERT(CAST(CONVERT(content USING latin1) AS BINARY) USING utf8) as clearcontent');
        $this->db2->where('artwork_id', $artworkId);
        $this->db2->where('category', 'artwork_literature');
        $this->db2->limit(1);
        $this->db2->order_by('id', 'asc');
        return $this->db2->get('artwork_texts');
    }
    
    function insertTextModule($data)
    {
        $this->db->insert('module_text', $data);
    }
    
    function getDetailImgFromArtwork($artworkId)
    {
        $this->db2->select('*, CONVERT(CAST(CONVERT(photographer_name USING latin1) AS BINARY) USING utf8) as clearphotographer, CONVERT(CAST(CONVERT(credits USING latin1) AS BINARY) USING utf8) as clearcredits');
        $this->db2->where('collection_big_top', 1);
        $this->db2->where('artwork_id', $artworkId);
        $this->db2->limit(1);
        return $this->db2->get('artwork_photos');
    }
    
    function getGalleryImgFromArtwork($artworkId)
    {
        $this->db2->select('*, CONVERT(CAST(CONVERT(photographer_name USING latin1) AS BINARY) USING utf8) as clearphotographer, CONVERT(CAST(CONVERT(credits USING latin1) AS BINARY) USING utf8) as clearcredits');
        $this->db2->where('collection_big_top', 0);
        $this->db2->where('collection_big', 1);
        $this->db2->where('artwork_id', $artworkId);
        $this->db2->order_by('id', 'asc');
        return $this->db2->get('artwork_photos');        
    }
    
    function insertGalleryItems($data)
    {
        $this->db->insert_batch('item_galleryitem', $data);
    }
    
    function updateArtwork($item, $artworkId)
    {
        $this->db->where('artwork_id', $artworkId);
        $this->db->update('item', $item);
    }
    
    
    function getItemByArtworkId($artworkId)
    {
        $this->db->where('artwork_id', $artworkId);
        return $this->db->get('item');
    }
    
    function deleteModulesText($itemId)
    {
        $this->db->where('item_id', $itemId);
        $this->db->delete('module_text');
    }
    
    function deleteGalleryItems($itemId)
    {
        $this->db->where('item_id', $itemId);
        $this->db->delete('item_galleryitem');
    }
}

?>