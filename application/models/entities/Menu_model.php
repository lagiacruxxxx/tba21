<?php

class Menu_model extends CI_Model
{
    function getMetatagsByContinent($continentId)
    {
        $this->db->where('continent_id', $continentId);
        return $this->db->get('metatag');
    }
    
    function getMetatags()
    {
        return $this->db->get('metatag');
    }
    
    function getMetatagsByCategory($categoryId)
    {
        $this->db->where('metatag_category_id', $categoryId);
        return $this->db->get('metatag');
    }
}

?>