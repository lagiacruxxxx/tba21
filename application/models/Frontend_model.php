<?php

class Frontend_model extends CI_Model  
{

    function getItems()
    {
        return $this->db->get('item');
    }
    
    function getMetatagsPerItem($itemId)
    {
        $this->db->where('item_id', $itemId);
        return $this->db->get('metatag_item');
    }
    
    function getItemsPerContinent($continentId)
    {
        $this->db->where('continent_id', $continentId);
        $this->db->where('(parent_item_id IS NULL OR parent_item_id = 0)');
        $this->db->where('show', 1);
        $this->db->order_by('ordering_date', 'desc');
        $this->db->order_by('name', 'asc');
        return $this->db->get('item');
    }

    
    function getMetatagsByContinent($continentId)
    {
        $this->db->where('continent_id', $continentId);
        $this->db->order_by('ordering', 'asc');
        return $this->db->get('continent_metatag');
    }
    
    function getMetatagById($metatagId)
    {
        $this->db->where('id', $metatagId);
        return $this->db->get('metatag');
    }
    
    function getItemById($itemId)
    {
        $this->db->where('id', $itemId);
        return $this->db->get('item');
    }
    
    function getModuleTextByItemId($itemId)
    {
        $this->db->select('*, "text" as "mod"');
        $this->db->where('item_id', $itemId);
        return $this->db->get('module_text');
    }
    
    function getModuleImageByItemId($itemId)
    {
        $this->db->select('*, "image" as "mod"');
        $this->db->where('item_id', $itemId);
        return $this->db->get('module_image');
    }    
    
    function getModuleBulletpointByItemId($itemId)
    {
        $this->db->select('*, "bulletpoint" as "mod"');
        $this->db->where('item_id', $itemId);
        return $this->db->get('module_bulletpoint');
    }    
    
    function getModuleVideoByItemId($itemId)
    {
        $this->db->select('*, "video" as "mod"');
        $this->db->where('item_id', $itemId);
        return $this->db->get('module_video');
    }
    
    function getModuleHTMLByItemId($itemId)
    {
        $this->db->select('*, "html" as "mod"');
        $this->db->where('item_id', $itemId);
        return $this->db->get('module_html');
    }
    
    function getModuleDownloadByItemId($itemId)
    {
        $this->db->select('*, "download" as "mod"');
        $this->db->where('item_id', $itemId);
        return $this->db->get('module_download');
    }
    
    function getModuleHeadlineByItemId($itemId)
    {
        $this->db->select('*, "headline" as "mod"');
        $this->db->where('item_id', $itemId);
        return $this->db->get('module_headline');
    }
    
    function getModule2ColImageByItemId($itemId)
    {
        $this->db->select('*, "2col_image" as "mod"');
        $this->db->where('item_id', $itemId);
        return $this->db->get('module_2col_image');
    }
    
    function getModuleStoreByItemId($itemId)
    {
        $this->db->select('module_store.*, "store" as "mod", shop_item.in_stock');
        $this->db->where('item_id', $itemId);
		$this->db->join('shop_item', 'shop_item.id = module_store.magento_id');
        return $this->db->get('module_store');
    }    
    
    function getGalleryItems($itemId)
    {
        $this->db->where('item_id', $itemId);
        return $this->db->get('item_galleryitem');
    }
    
    function getRelatedMetatags($itemId)
    {
        //$this->db->select('metatag.id, metatag.name, metatag.name_de');
        $this->db->where('item_id', $itemId);
        //$this->db->join('metatag', 'metatag.id = item_relatedmetatag.relatedmetatag_id');
        return $this->db->get('item_relatedmetatag');
    }
    
    function getRelatedItems($itemId)
    {
        $this->db->select('item.id, item.detail_img, item.name');
        $this->db->from('item_relateditem');
        $this->db->join('item', 'item.id = item_relateditem.relateditem_id AND item.show = 1');
        $this->db->where('item_id', $itemId);
        return $this->db->get();
    }
    
    function getListHome($limit, $offset)
    {
        $this->db->where('teaser', 1);
        $this->db->where('show', 1);
        $this->db->limit($limit, $offset);
        $this->db->order_by('ordering_date', 'desc');
        $this->db->order_by('name', 'asc');
        return $this->db->get('item');
    }
    
    function getFirstTextModuleByItemId($itemId)
    {
        $this->db->order_by('top', 'asc');
        $this->db->where('column_id', 0);
        $this->db->where('item_id', $itemId);
        $this->db->limit(1);
        return $this->db->get('module_text');
    }
    
    function getItemsByMetatagId($metatagId)
    {
        $this->db->select('item.*');
        $this->db->from('metatag_item');
        $this->db->where('metatag_item.metatag_id', $metatagId);
        $this->db->where('(parent_item_id IS NULL OR parent_item_id = 0)');
        $this->db->join('item', 'item.id = metatag_item.item_id');
        return $this->db->get();
    }
    
    function getItemsByMetatagIdByContinentId($metatagId, $continentId)
    {
        $this->db->select('item.*');
        $this->db->from('metatag_item');
        $this->db->where('metatag_item.metatag_id', $metatagId);
        $this->db->where('item.continent_id', $continentId);
        $this->db->where('item.show', 1);
        $this->db->where('(parent_item_id IS NULL OR parent_item_id = 0)');
        $this->db->join('item', 'item.id = metatag_item.item_id');
        $this->db->order_by('item.ordering_date', 'desc');
        $this->db->order_by('item.name', 'asc');
        return $this->db->get();
    }
    
    
    function getItemByPrettyUrl($prettyurl)
    {
        $this->db->where('prettyurl', $prettyurl);
        $this->db->where('(parent_item_id IS NULL OR parent_item_id = 0)');
        return $this->db->get('item');
    }
    
    
    function getLanguageItem($itemId, $language)
    {
        $this->db->where('parent_item_id', $itemId);
        $this->db->where('lang', $language);
        return $this->db->get('item');
    }
    
    
    function getArtists($metatagCategoryId, $orderBy)
    {
        $this->db->where('metatag_category_id', $metatagCategoryId);
        $this->db->order_by($orderBy, 'asc');
        return $this->db->get('metatag');
    }
    
    
    
    
    
    function getTeaserItemByContinentId($continentId)
    {
        $this->db->where('continent_id', $continentId);
        $this->db->where('teaser', 1);
        $this->db->where('(parent_item_id IS NULL OR parent_item_id = 0)');
        $this->db->limit(4);
        return $this->db->get('item');
    }
    
    
    function updateItemHeaders($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('item', $data);
    }
    
    
    
    function getItemsPerContinentWithLimit($continentId, $limit, $offset)
    {
        $this->db->where('continent_id', $continentId);
        $this->db->limit($limit, $offset);
        $this->db->where('(parent_item_id IS NULL OR parent_item_id = 0)');
        $this->db->where('show', 1);
        $this->db->order_by('ordering_date', 'desc');
        $this->db->order_by('name', 'asc');
        return $this->db->get('item');
    }
    
    function getItemsByMetatagIdWithLimit($metatagId, $limit, $offset)
    {
        $this->db->select('item.*');
        $this->db->from('metatag_item');
        $this->db->limit($limit, $offset);
        $this->db->where('metatag_item.metatag_id', $metatagId);
        $this->db->where('(parent_item_id IS NULL OR parent_item_id = 0)');
        $this->db->where('item.show', 1);
        $this->db->join('item', 'item.id = metatag_item.item_id');
        $this->db->order_by('item.ordering_date', 'desc');
        $this->db->order_by('item.name', 'asc');
        return $this->db->get();
    }
    
    
    /*********************************************************
     * SEARCH
     *********************************************************/
    
    function searchItemByNameByContinents($continents, $string, $result_ids)
    {
        $this->db->like('name', $string);
        $this->db->where('show', 1);
        $this->db->where('lang', LANG_EN);
        $this->db->where_in('continent_id', $continents);
        $this->db->where_not_in('id', $result_ids);
        $this->db->order_by('ordering_date', 'desc');
        $this->db->order_by('name', 'asc');
        return $this->db->get('item');
    }
    
    function searchMetatagByName($string)
    {
        $this->db->like('name', $string);
        $this->db->or_like('name_de', $string);
        return $this->db->get('metatag');
    }
    
    function searchItemsByMetatagIdByContinents($metatagId, $continents, $result_ids)
    {
        $this->db->select('item.*');
        $this->db->from('metatag_item');
        $this->db->where('metatag_item.metatag_id', $metatagId);
        $this->db->where_in('continent_id', $continents);
        $this->db->where_not_in('item.id', $result_ids);
        $this->db->where('(parent_item_id IS NULL OR parent_item_id = 0)');
        $this->db->where('item.lang', LANG_EN);
        $this->db->where('item.show', 1);
        $this->db->join('item', 'item.id = metatag_item.item_id');
        $this->db->order_by('item.ordering_date', 'desc');
        $this->db->order_by('item.name', 'asc');
        return $this->db->get();
    }
    
    
    function getArtistMetatagByItemId($itemId, $artistMetatagCategory)
    {
        $this->db->select('metatag.*');
        $this->db->from('metatag_item');
        $this->db->join('metatag', 'metatag.id = metatag_item.metatag_id');
        $this->db->where('metatag.metatag_category_id', $artistMetatagCategory);
        $this->db->where('metatag_item.item_id', $itemId);
        return $this->db->get();
    }
    
    
    
    function getTextModuleForFix()
    {
        $this->db->like('content', 'â€');
        return $this->db->get('module_text');
    }
    
    function getTextModuleForFix2()
    {
        $this->db->like('content', '"œ');
        return $this->db->get('module_text');
    }
    
    function updateTextModuleFix($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('module_text', $data);
    }
    
    
    function getActiveSplashpages()
    {
        $this->db->where('active', 1);
        return $this->db->get('splashpage');
        
    }
    
}
