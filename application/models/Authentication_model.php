<?php

class Authentication_model extends CI_Model
{
    function getPW($username)
    {
        $this->db->where('username', $username);
        $this->db->select('pword');
        return $this->db->get('user');
    }

    function getUserdataByUsername($username)
    {
        $this->db->where('username', $username);
        return $this->db->get('user');
    }

    function getUserdataByID($id)
    {
        $this->db->where('id', $id);
        return $this->db->get('user');
    }
    
    function getPressUserByEmail($email)
    {
        $this->db->where('email', $email);
        return $this->db->get('pressuser');
    }
    
    function insertPressUser($data)
    {
        $this->db->insert('pressuser', $data);
    }
    
    function updateUser($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('user', $data);
    }
    
    
    public function subscribeNewsletter($data)
    {
        $this->db->insert('newsletter', $data);
    }
    
    public function unsubscribeNewsletter($email)
    {
        $this->db->where('email', $email);
        $this->db->delete('newsletter');
    }
    
    public function newsletterEmailExists($email)
    {
        $this->db->where('email', $email);
        return $this->db->get('newsletter');
    }
}

?>