<?php

class Shop_model extends CI_Model
{
    function getCountries()
    {
        return $this->db->get('country');
    }
    
    function getShopItems()
    {
        return $this->db->get('shop_item');
    }
    
    function getShopItemById($id)
    {
        $this->db->where('id', $id);
        return $this->db->get('shop_item');
    }
    
    function getShopSettings($id)
    {
        $this->db->where('id', $id);
        return $this->db->get('shop_settings');
    }
    
    function insertShopOrder($data)
    {
        $this->db->insert('shop_order', $data);
        return $this->db->insert_id();
    }
    
    function insertShopOrderItems($items)
    {
        $this->db->insert_batch('shop_order_item', $items);
    }
    
    function getShopOrderById($id)
    {
        $this->db->where('id', $id);
        return $this->db->get('shop_order');
    }
    
    function getCountryById($id)
    {
        $this->db->where('id', $id);
        return $this->db->get('country');
    }
    
    function getOrderItemsByOrder($id)
    {
        $this->db->select('shop_order_item.*, shop_item.name as name, shop_item.price as price, shop_item.weight as weight');
        $this->db->where('order_id', $id);
        $this->db->join('shop_item', 'shop_item.id = shop_order_item.shop_item_id');
        return $this->db->get('shop_order_item');
    }

	function getShopOrderByIdent($orderIdent)
	{
		$this->db->select('shop_order.*, country.name_de, country.name_en');
		$this->db->where('orderIdent', $orderIdent);
		$this->db->join('country', 'country.id = shop_order.country_id');
		return $this->db->get('shop_order');
	}
    
	function updateOrderStatus($orderId, $orderStatus)
	{
		
		$this->db->where('id', $orderId);
		$this->db->update('shop_order', array('orderstatus' => $orderStatus, 'orderstatus_ts' => date('Y-m-d H:s:i')));
	}
	
	function insertPaymentResult($data)
	{
		$this->db->insert('shop_paymentresult', $data);
	}
	
	function getLatestInvoicenumber()
	{
		$this->db->select_max('invoice');
		return $this->db->get('shop_order');
	}
	
	function setInvoiceNumber($orderId, $invoice)
	{
		$this->db->where('id', $orderId);
		$this->db->update('shop_order', array('invoice' => $invoice));
	}
	
	
	function getShippingCosts($shipmentClass, $shipmentType)
	{
		$this->db->where('shipment_class', $shipmentClass);
		$this->db->where('shipment_type', $shipmentType);
		return $this->db->get('shop_shipment');
	}
	
   
}

?>