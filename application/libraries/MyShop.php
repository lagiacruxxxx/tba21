<?php defined('BASEPATH') OR exit('No direct script access allowed');

class MyShop 
{
	protected $ci = null;
	protected $cart = null;
	protected $soap = null;
	protected $session_id = null;
	
	function __construct()
	{
	    $this->ci = & get_instance();
        $this->ci->load->library('cart');
        $this->ci->load->model('Shop_model', 'shopm');
    }	
	
	
	public function getCartItemCount()
	{
	    return $this->ci->cart->total_items();
	}
	
	public function clearCart()
	{
	    $this->ci->cart->destroy();
	}
	
	public function getAllProductsForBackendSelect()
	{
	    return $this->ci->shopm->getShopItems();
	}
	
	public function termsView()
	{
	    $settings = $this->ci->shopm->getShopSettings($this->ci->config->item('shopSettingsId'))->row();
	    
	    if($this->ci->language == LANG_EN)
	        return $settings->tos_en;
	    else    
	        return $settings->tos_de;
	}
	
	public function cartView()
	{
	    $data['cartitems'] = $this->ci->cart->contents();
	    $data['total'] = $this->ci->cart->total();
	    
	    return $this->ci->load->view('frontend/cart', $data, true);
	}
	
	public function addToCart($productId)
	{
	    $product = $this->ci->shopm->getShopItemById($productId)->row();
	    
        $data = array(
            'id' => $productId,
            'qty' => 1,
            'price' => $product->price,
            'name' => $product->name,
            'options' => array(
                'image' => site_url('items/uploads/shop/' . $product->image),
                'desc' => $product->description,
            ),
        );
        
        $this->ci->cart->insert($data);
		
		
		$this->ci->session->set_userdata('orderIdent', '');
		$this->ci->session->set_userdata('storageId', '');
	}
	
	public function removeFromCart($rowId)
	{
	    $this->ci->cart->remove($rowId);
		
		$this->ci->session->set_userdata('orderIdent', '');
		$this->ci->session->set_userdata('storageId', '');
	}
	
	public function updateItemCartQty($rowId, $change)
	{
		$cartrow = $this->ci->cart->get_item($rowId);
		
	    $this->ci->cart->update(array(
            'rowid' => $rowId,
	        'qty' => $cartrow['qty'] + $change,
	    ));
		
		$this->ci->session->set_userdata('orderIdent', '');
		$this->ci->session->set_userdata('storageId', '');
	    
	    return array(
	    	'rowId' => $rowId,
	        'count' => $cartrow['qty'] + $change,
	        'price' => number_format((float)($cartrow['qty'] + $change) * $cartrow['price'], 2, '.', ''),
        );
	}
	
	public function getCartTotal()
	{
	    return number_format((float)$this->ci->cart->total(), 2, '.', '');
	}
	
	public function getCartWeight()
	{
		$weight = 0;
		foreach($this->ci->cart->contents() as $cartitem)
		{
			$shopitem = $this->ci->shopm->getShopItemById($cartitem['id'])->row();
			$weight += $shopitem->weight * $cartitem['qty'];
		}

		return $weight;
	}
	
	public function checkoutView()
	{
	    
	    $data = array();
	    $data['storageInfo'] = $this->__initStorage();
		
		$this->ci->session->set_userdata('orderIdent', $data['storageInfo']['orderIdent']);
		$this->ci->session->set_userdata('storageId', $data['storageInfo']['storageId']);
	    
	    $data['countries'] = $this->ci->shopm->getCountries();
	    $data['language'] = $this->ci->language;
		
		$weight = $this->getCartWeight();
		if($weight > 2000)
			$shipmentClass = SHIPMENT_CLASS_HEAVY;
		else
			$shipmentClass = SHIPMENT_CLASS_LIGHT;
		
		$standard = $this->ci->shopm->getShippingCosts($shipmentClass, SHIPMENT_TYPE_STANDARD)->row();
		$express = $this->ci->shopm->getShippingCosts($shipmentClass, SHIPMENT_TYPE_EXPRESS)->row();
			
	    $data['shipment']['austria'] = array(
	        SHIPMENT_TYPE_STANDARD => array(
                'name' => 'Standard',
	            'price' => $standard->austria . ' €',	            
	        ),
	        SHIPMENT_TYPE_EXPRESS => array(
                'name' => 'Express',
	            'price' => $express->austria . ' €',	            
	        ),
	    );
		
	    $data['shipment']['europe'] = array(
	        SHIPMENT_TYPE_STANDARD => array(
                'name' => 'Standard',
	            'price' => $standard->europe . ' €',	            
	        ),
	        SHIPMENT_TYPE_EXPRESS => array(
                'name' => 'Express',
	            'price' => $express->europe . ' €',	            
	        ),
	    );
		
	    $data['shipment']['worldwide'] = array(
	        SHIPMENT_TYPE_STANDARD => array(
                'name' => 'Standard',
	            'price' => $standard->worldwide . ' €',	            
	        ),
	        SHIPMENT_TYPE_EXPRESS => array(
                'name' => 'Express',
	            'price' => $express->worldwide . ' €',	            
	        ),
	    );
	    
	    
	    return !$this->ci->is_mobile ? $this->ci->load->view('frontend/checkout', $data, true) : $this->ci->load->view('frontend/checkout_mobile', $data, true);
	}


	public function finishedView()
	{
		$this->ci->cart->destroy();
		$data['expired'] = false;
		return $this->ci->load->view('frontend/shop_finished', $data, true);
	}
	
	public function verifyCheckoutData()
	{
	    $this->ci->load->library('form_validation');
	    
	    $this->ci->form_validation->set_rules('firstname', 'lang:shop_firstname', 'trim|required|max_length[255]');
	    $this->ci->form_validation->set_rules('lastname', 'lang:shop_lastname', 'trim|required|max_length[255]');
	    $this->ci->form_validation->set_rules('email', 'lang:shop_email', 'trim|required|max_length[255]|valid_email');
	    $this->ci->form_validation->set_rules('phone', 'lang:shop_phone', 'trim|required|max_length[50]');	    
	    $this->ci->form_validation->set_rules('street', 'lang:shop_street', 'trim|required|max_length[255]');
	    $this->ci->form_validation->set_rules('zip', 'lang:shop_zipcode', 'trim|required|max_length[10]|numeric');
	    $this->ci->form_validation->set_rules('city', 'lang:shop_city', 'trim|required|max_length[255]');
	    $this->ci->form_validation->set_rules('country', 'lang:shop_city', 'trim|required');
	    $this->ci->form_validation->set_rules('accept_tos', '', 'in_list[1]',
	        array('in_list' => $this->ci->lang->line('shop_tos_error')));
	    $this->ci->form_validation->set_rules('shipmentType', '', 'in_list[0,1]');
	         
	     
	    
	    if($this->ci->form_validation->run() == FALSE)
	    {
	        $success = false;
	        $this->ci->form_validation->set_error_delimiters('', '');
	        $message = nl2br($this->ci->form_validation->error_string());
	    }
	    else
	    {
	        $message = '';
	        $success = true;
	    }
	    
	    echo json_encode(array(
	        'success' => $success,
	        'message' => $message,
	    ));
	}
	
	
	public function createOrder()
	{
		$storageResponse = $this->ci->input->post('storageResponse');
		$data = array(
	        'orderstatus' => ORDER_STATUS_CREATED,
	        'orderstatus_ts' => date('Y-m-d H:s:i'),
	        'firstname' => $this->ci->input->post('firstname'),
	        'lastname' => $this->ci->input->post('lastname'),
	        'email' => $this->ci->input->post('email'),
	        'phone' => $this->ci->input->post('phone'),
	        'street' => $this->ci->input->post('street'),
	        'zip' => $this->ci->input->post('zip'),
	        'city' => $this->ci->input->post('city'),
	        'country_id' => $this->ci->input->post('country'),
	        'paymentType' => $this->ci->input->post('paymentType'),
	        'shipmentType' => $this->ci->input->post('shipmentType'),
	        'cc_pan' => $storageResponse['maskedPan'],
	        'cc_cardholder' => $storageResponse['cardholdername'],
	        'cc_exp_date' => $storageResponse['expiry'],
	        'orderIdent' => $this->ci->session->userdata('orderIdent'),
	        'storageId' => $this->ci->session->userdata('storageId'),
	    );
	    $amount = 0;
		$weight_total = 0;
		foreach($this->ci->cart->contents() as $item)
	    {
			$amount += $item['price'] * $item['qty'];
			$weight_total += $this->ci->shopm->getShopItemById($item['id'])->row()->weight * $item['qty'];
	    }
		$data['total'] = $amount + $this->getShipmentCosts($data['country_id'], $weight_total, $data['shipmentType']);		
	    $orderId = $this->ci->shopm->insertShopOrder($data);
	    
	    $order_items = array();
	    
		
		
	    foreach($this->ci->cart->contents() as $item)
	    {
	        $order_items[] = array(
                'order_id' => $orderId,
	            'shop_item_id' => $item['id'],
	            'amount' => $item['qty'],    
	        );
	    }
		
	    $this->ci->shopm->insertShopOrderItems($order_items);

	    //$this->ci->session->set_userdata('shop_orderId', $orderId);
	    
	    echo json_encode(array(
            'success' => true,
	        'msg' => '',    
	    ));
	}


	private function getShipmentCosts($country_id, $weight, $shipmentType)
	{
		if($weight > 2000)
			$shipmentClass = SHIPMENT_CLASS_HEAVY;
		else
			$shipmentClass = SHIPMENT_CLASS_LIGHT;
		
		$shipment = $this->ci->shopm->getShippingCosts($shipmentClass, $shipmentType)->row();
		$country = $this->ci->shopm->getCountryById($country_id)->row();
	
		
	
		switch($country->shipping)
		{
			case 'austria':
				$costs = $shipment->austria;
				break;
			case 'europe':
				$costs = $shipment->europe;
				break;
			case 'worldwide':
				$costs = $shipment->worldwide;
				break;

		}
	
		return $costs;
	}

	
	public function confirmView()
	{
	    $data = array();
	    
	    if(!$this->__sessionValid())
			$data['expired'] = true;
		else 
		{
			$data['expired'] = false;
			$data['order'] = $this->ci->shopm->getShopOrderByIdent($this->ci->session->userdata('orderIdent'))->row();
			$data['country'] = $this->ci->shopm->getCountryById($data['order']->country_id)->row();
		    $data['country'] = $this->ci->language == LANG_DE ? $data['country']->name_de : $data['country']->name_en;
		    $data['shipment'] = $data['order']->shipmentType == SHIPMENT_TYPE_STANDARD ? 'Standard' : 'Express';
		    $data['items'] = $this->ci->shopm->getOrderItemsByOrder($data['order']->id);
		    
	        $total = 0;
			$weight = 0;	     
		    foreach($data['items']->result() as $item)
		    {
		        $total += $item->price * $item->amount;
				$weight += $item->weight * $item->amount;
		    }
			$deliverycost = $this->getShipmentCosts($data['order']->country_id, $weight, $data['order']->shipmentType);
			$data['delivery'] = number_format((float)$deliverycost, 2, '.', '');
	        
		    $data['item_total'] = number_format((float)$total, 2, '.', '');
		    $data['total'] = number_format((float)$total + (float)$deliverycost, 2, '.', '');
		}
			
		return $this->ci->load->view('frontend/shop_confirm', $data, true);
	}

	private function __sessionValid()
	{
		$order = $this->ci->shopm->getShopOrderByIdent($this->ci->session->userdata('orderIdent'));
		
		return $order->num_rows() == 1 && $order->row()->orderstatus == ORDER_STATUS_CREATED;
	}
	
	
	public function confirmPayment()
	{
		$paymentState = isset($_POST["paymentState"]) ? $_POST["paymentState"] : "undefined";

		$params = "";
		foreach ($_POST as $key => $value) 
		{
		    $params .= ";$key = $value;";
		}
		
		$this->ci->shopm->insertPaymentResult(array(
			'paymentresult' => $paymentState,
			'params' => $params
		));
		
		switch($paymentState)
		{
			case 'CANCEL':
				$this->ci->shopm->updateOrderStatus($_POST["orderNumber"], ORDER_STATUS_CANCEL);
				break;
			case 'SUCCESS':
				$this->ci->shopm->updateOrderStatus($_POST["orderNumber"], ORDER_STATUS_SUCCESS);
				$invoiceId = $this->__createInvoice($_POST["orderNumber"]);
				$this->sendOrderConfirmation($_POST["orderNumber"]);
				$this->sendInvoice($_POST["orderNumber"], $invoiceId);
				break;
			case 'FAILURE':
				$this->ci->shopm->updateOrderStatus($_POST["orderNumber"], ORDER_STATUS_FAILURE);
				break;
			case 'PENDING':
				$this->ci->shopm->updateOrderStatus($_POST["orderNumber"], ORDER_STATUS_PENDING);
				break; 
		}
	}
	
	public function successView()
	{
		$data['resulttext'] = $this->ci->lang->line('paymentresult_success');
		$this->ci->load->view('frontend/paymentresult', $data);
	}
	
	public function cancelView()
	{
		$data['resulttext'] = $this->ci->lang->line('paymentresult_cancel');
		$this->ci->load->view('frontend/paymentresult', $data);
	}
	
	public function pendingView()
	{
		$data['resulttext'] = 'PENDING';//$this->ci->lang->line('paymentresult_cancel');
		$this->ci->load->view('frontend/paymentresult', $data);
	}

	public function failureView()
	{
		$data['resulttext'] = 'FAILURE';//$this->ci->lang->line('paymentresult_cancel');
		$this->ci->load->view('frontend/paymentresult', $data);
	}
	
	
	
	public function sendOrderConfirmation($orderId = 1)
	{
		$data['order'] = $this->ci->shopm->getShopOrderById($orderId)->row();
		$data['settings'] = $this->ci->shopm->getShopSettings($this->ci->config->item('shopSettingsId'))->row();
		$data['country'] = $this->ci->shopm->getCountryById($data['order']->country_id)->row();
		$data['country'] = $this->ci->language == LANG_EN ? $data['country']->name_en : $data['country']->name_de; 
		
		$data['items'] = $this->ci->shopm->getOrderItemsByOrder($data['order']->id);
	    $shipment = $data['order']->shipmentType == SHIPMENT_TYPE_STANDARD ? 'Standard' : 'Express';
		$data['shipment'] = $this->ci->lang->line('shipment_costs') . '(' . $shipment . ')';
        
        $total = 0;
		$weight = 0;	     
	    foreach($data['items']->result() as $item)
	    {
	        $total += $item->price * $item->amount;
			$weight += $item->weight * $item->amount;
	    }
	    $deliverycost = $this->getShipmentCosts($data['order']->country_id, $weight, $data['order']->shipmentType);
		$data['delivery'] = number_format((float)$deliverycost, 2, '.', '');
		
	    $data['item_total'] = number_format((float)$total, 2, '.', '');
	    $data['total'] = number_format((float)$total + (float)$deliverycost, 2, '.', '');
		
		require_once(APPPATH . "libraries/phpmailer/PHPMailerAutoload.php");	
		$mail = new PHPMailer();
		$mail->CharSet = 'UTF-8';
        $mail->IsSendmail(); // we are going to use SMTP
        //$mail->SMTPSecure = "ssl";  // prefix for secure protocol to connect to the server 
        //$mail->SMTPDebug = 0;  // prefix for secure protocol to connect to the server 
        $mail->Host       = "mail.tba21.org";      // setting SMTP server
        $mail->Port       = 25;                   // SMTP port to connect to 
        $mail->Username   = "greenlight@tba21.org";  // user email address
        $mail->Password   = "grLig2602$";            // password 
        $mail->SMTPAuth   = true; // enabled SMTP authentication
        $mail->SetFrom('shop@tba21.org');  //Who is sending the email
        $mail->AddReplyTo('shop@tba21.org');  //email address that receives the response
        $mail->IsSMTP();
		
		$subject = $this->ci->lang->line('orderconfirmation_subject');
		
        $body = $this->ci->language == LANG_END ? $data['settings']->email_order_confirmation_en : $data['settings']->email_order_confirmation_de;/*$this->load->view('mail/bill_template', $response_data, true);*/
        $body = str_replace(':::customer:::', $data['order']->firstname . ' ' . $data['order']->lastname, $body);
		$body = str_replace(':::supportmail:::', '<a href=mailto:' . $data['settings']->supportemail . '>' . $data['settings']->supportemail . '</a>', $body);
		$body = str_replace(':::orderinfo:::', $this->ci->load->view('frontend/shop_mailconfirmation', $data, true), $body);
		
		$mail->Subject = $subject;
		$mail->Body = $body;			
	    $mail->isHTML(true); 
        
        $mail->AddAddress($data['order']->email, '');
        $mail->AddBCC("fanny.hauser@tba21.org", '');
        $mail->Send();
    }


	public function sendInvoice($orderId, $invoiceId)
	{
		$data['order'] = $this->ci->shopm->getShopOrderById($orderId)->row();
		$data['settings'] = $this->ci->shopm->getShopSettings($this->ci->config->item('shopSettingsId'))->row();
		$data['country'] = $this->ci->shopm->getCountryById($data['order']->country_id)->row();
		$data['country'] = $this->ci->language == LANG_EN ? $data['country']->name_en : $data['country']->name_de; 
		
		$data['items'] = $this->ci->shopm->getOrderItemsByOrder($data['order']->id);
	    $shipment = $data['order']->shipmentType == SHIPMENT_TYPE_STANDARD ? 'Standard' : 'Express';
		$data['shipment'] = $this->ci->lang->line('shipment_costs') . '(' . $shipment . ')';
        
        $total = 0;	     
		$weight = 0;
	    foreach($data['items']->result() as $item)
	    {
	        $total += $item->price * $item->amount;
			$weight += $item->weight * $item->amount;
	    }
		
		$deliverycost = $this->getShipmentCosts($data['order']->country_id, $weight, $data['order']->shipmentType);
		$data['delivery'] = number_format((float)$deliverycost, 2, '.', '');
			
	    $data['item_total'] = number_format((float)$total, 2, '.', '');
	    $data['total'] = number_format((float)$total + (float)$deliverycost, 2, '.', '');
		
		require_once(APPPATH . "libraries/phpmailer/PHPMailerAutoload.php");	
		$mail = new PHPMailer();
		$mail->CharSet = 'UTF-8';
        $mail->IsSendmail(); // we are going to use SMTP
        //$mail->SMTPSecure = "ssl";  // prefix for secure protocol to connect to the server 
        //$mail->SMTPDebug = 0;  // prefix for secure protocol to connect to the server 
        $mail->Host       = "mail.tba21.org";      // setting SMTP server
        $mail->Port       = 25;                   // SMTP port to connect to 
        $mail->Username   = "greenlight@tba21.org";  // user email address
        $mail->Password   = "grLig2602$";            // password 
        $mail->SMTPAuth   = true; // enabled SMTP authentication
        $mail->SetFrom('shop@tba21.org');  //Who is sending the email
        $mail->AddReplyTo('shop@tba21.org');  //email address that receives the response
        $mail->IsSMTP();
		
		$data['subject'] = $this->ci->lang->line('invoice_subject') . $invoiceId;
		
		if($this->ci->language == LANG_EN)
        	$body = $this->ci->load->view('frontend/shop_mailinvoice_en', $data, true);
		else
        	$body = $this->ci->load->view('frontend/shop_mailinvoice_de', $data, true);
			
        
		$mail->Subject = $data['subject'];
		$mail->Body = $body;			
	    $mail->isHTML(true); 
        
        $mail->AddAddress($data['order']->email, '');		
        $mail->AddBCC("bookkeeping@tba21.org", '');
        $mail->Send();

	}
	
	
	
	
	private function __createInvoice($orderId)
	{
		$max_invoice = $this->ci->shopm->getLatestInvoicenumber($orderId)->row()->invoice;
		
		if($max_invoice == NULL)
			$new_invoice = 1;
		else
			$new_invoice = $max_invoice + 1;
		
		$this->ci->shopm->setInvoiceNumber($orderId, $new_invoice);
		return $new_invoice;
	}
	
	
	
	
	private function __initStorage()
	{
	    require_once(APPPATH . "third_party/config.inc.php");
	    
	    
	    // initializes the fingerprint seed
	    // please be aware that the correct order for the fingerprint seed has
	    // to be the following one:
	    // customerId, shopId, orderIdent, returnUrl, language, javascriptScriptVersion, secret
	    $requestFingerprintSeed = "";
	    
	    // adds the customer id to the fingerprint seed
	    $requestFingerprintSeed .= $customerId;
	    
	    // adds the shop id to the fingerprint seed
	    $requestFingerprintSeed .= $shopId;
	    
	    // adds the unique identification for the order (order identity) to the fingerprint seed
	    // (for demonstration purposes only a random string is generated)
	    $characters = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ-";
	    $randomString = "";
	    for ($i = 0; $i < 10; $i++) {
	        $randomString .= $characters[rand(0, strlen($characters) - 1)];
	    }
	    $orderIdent = $randomString;
	    $requestFingerprintSeed .= $orderIdent;
	    
	    $url = $WEBSITE_URL;
	    
	    // adds the return URL to the fingerprint seed
	    $returnURL = $url . "frontend/fallback_return.php";
	    $requestFingerprintSeed .= $returnURL;
	    
	    // adds the language to the fingerprint seed
	    $language = "en";
	    $requestFingerprintSeed .= $language;
	    
	    // adds the JavaScript version to the fingerprint seed
	    $javascriptScriptVersion = $PCI3_DSS_SAQ_A_ENABLE ? "pci3" : ''; // version can be an empty string
	    $requestFingerprintSeed .= $javascriptScriptVersion;
	    
	    // adds the merchant specific secret to the fingerprint seed
	    $requestFingerprintSeed .= $secret;
	    
	    // computes the fingerprint based on SHA512 and the fingerprint seed
	    $requestFingerprint = hash_hmac("sha512", $requestFingerprintSeed, $secret);
	    
	    //--------------------------------------------------------------------------------//
	    // Creates and sends a POST request (server-to-server request) to the
	    // Wirecard Checkout Platform for initiating the Wirecard data storage.
	    //--------------------------------------------------------------------------------//
	    
	    // initiates the string containing all POST parameters and
	    // adds them as key-value pairs to the post fields
	    $postFields = "";
	    $postFields .= "customerId=" . $customerId;
	    $postFields .= "&shopId=" . $shopId;
	    $postFields .= "&javascriptScriptVersion=" . $javascriptScriptVersion;
	    $postFields .= "&orderIdent=" . $orderIdent;
	    $postFields .= "&returnUrl=" . $returnURL;
	    $postFields .= "&language=" . $language;
	    $postFields .= "&requestFingerprint=" . $requestFingerprint;
	    
	    if ($PCI3_DSS_SAQ_A_ENABLE) {
	        if ($PCI3_DSS_SAQ_A_IFRAME_CSS_URL)
	            $postFields .= '&iframeCssUrl=' . $PCI3_DSS_SAQ_A_IFRAME_CSS_URL;
	    
	        if ($PCI3_DSS_SAQ_A_CCARD_SHOW_CVC !== null)
	            $postFields .= '&creditcardShowCvcField=' . ($PCI3_DSS_SAQ_A_CCARD_SHOW_CVC ? 'true' : 'false');
	    
	        if ($PCI3_DSS_SAQ_A_CCARD_SHOW_ISSUEDATE !== null)
	            $postFields .= '&creditcardShowIssueDateField=' . ($PCI3_DSS_SAQ_A_CCARD_SHOW_ISSUEDATE ? 'true' : 'false');
	    
	        if ($PCI3_DSS_SAQ_A_CCARD_SHOW_ISSUENUMBER !== null)
	            $postFields .= '&creditcardShowIssueNumberField=' . ($PCI3_DSS_SAQ_A_CCARD_SHOW_ISSUENUMBER ? 'true' : 'false');
	    
	        if ($PCI3_DSS_SAQ_A_CCARD_SHOW_CARDHOLDERNAME !== null)
	            $postFields .= '&creditcardShowCardholderNameField=' . ($PCI3_DSS_SAQ_A_CCARD_SHOW_CARDHOLDERNAME ? 'true' : 'false');
	    }
	     
	    
	    // initializes the libcurl of PHP used for sending a POST request
	    // to the Wirecard data storage as a server-to-server request
	    // (please be aware that you have to use a web server where a
	    // server-to-server request is enabled)
	    $curl = curl_init();
	    
	    // sets the required options for the POST request via curl
	    curl_setopt($curl, CURLOPT_URL, $URL_DATASTORAGE_INIT);
	    curl_setopt($curl, CURLOPT_PORT, $WIRECARD_CHECKOUT_PORT);
	    curl_setopt($curl, CURLOPT_PROTOCOLS, $WIRECARD_CHECKOUT_PROTOCOL);
	    curl_setopt($curl, CURLOPT_POST, true);
	    curl_setopt($curl, CURLOPT_POSTFIELDS, $postFields);
	    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, true);
	    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
	    curl_setopt($curl, CURLOPT_USERAGENT, $_SERVER['HTTP_USER_AGENT']);
	    
	    // sends a POST request to the Wirecard Checkout Platform and stores the
	    // result returned from the Wirecard data storage in a string for later use
	    $curlResult = curl_exec($curl);
	    if (!$curlResult) {
	        $error = curl_error($curl);
	        var_dump($error);
	    }
	    
	    // closes the connection to the Wirecard Checkout Platform
	    curl_close($curl);
	    
    	// extracts each key-value pair returned from the previous POST request
        foreach (explode('&', $curlResult) as $keyvalue) {
            // splits the key and the name of each key-value pair
            $param = explode('=', $keyvalue);
        	
            if (sizeof($param) == 2) {
                // decodes key and value
                $key = urldecode($param[0]);
                $value = urldecode($param[1]);
                if (strcmp($key, "storageId") == 0) {
                    $storageId = $value;
                    // saves the storage id in a session variable for later use
                    // when reading data from the data storage in file read.php
                    $_SESSION[$STORAGE_ID] = $storageId;
                }
                if (strcmp($key, "javascriptUrl") == 0) {
                    // saves the JavaScript URL in variable for later use within this file
                    $javascriptURL = $value;
                }
            }
        }

        return array(
            'javascriptUrl' => $javascriptURL,
            'storageId' => $storageId,
            'orderIdent' => $orderIdent,
        );
	}







	public function initPayment()
	{
		require_once(APPPATH . "third_party/config.inc.php");
		
		//--------------------------------------------------------------------------------//
		// Computes the protocol, servername, port and path for the various return URLs.
		//--------------------------------------------------------------------------------//
		
		//--------------------------------------------------------------------------------//
		// Sets the values of all required and optional parameters.
		//--------------------------------------------------------------------------------//
		
		$requestFingerprintOrder = "";
		$requestFingerprint = "";
		
		// sets values for parameters
		if($this->ci->language == LANG_EN)
			$language = "en";
		else
			$language = 'de';
		
		$this->ci->cart->destroy();
		
		$order = $this->ci->shopm->getShopOrderByIdent($this->ci->session->userdata('orderIdent'))->row();
		
		$amount = $order->total;
		$currency = 'EUR';
		$paymentType = $order->paymentType == 'CreditCard' ? 'CCARD' : $order->paymentType;
		$orderDescription = "$order->firstname $order->lastname ($order->name_en $order->zip): $order->orderIdent"; //"Jane Doe (33562), Order: 5343643-034";
		$successURL = site_url('Shop/return_success');
		$cancelURL = site_url('Shop/return_cancel');
		$failureURL = site_url('Shop/return_failure');
		$serviceURL = site_url('Shop/service');
		$pendingURL = site_url('Shop/return_pending');
		$confirmURL = site_url('Shop/confirm');
		$consumerUserAgent = $_SERVER['HTTP_USER_AGENT'];
		$consumerIpAddress = $_SERVER['REMOTE_ADDR'];
		$storageId = $order->storageId;
		$orderIdent = $order->orderIdent;
		$windowName = $CHECKOUT_WINDOW_NAME;
		
		//--------------------------------------------------------------------------------//
		// Computes the fingerprint and the fingerprint order.
		//--------------------------------------------------------------------------------//
		
		$requestFingerprintSeed = "";
		$requestFingerprintOrder .= "secret,";
		$requestFingerprintSeed .= $secret;
		$requestFingerprintOrder .= "customerId,";
		$requestFingerprintSeed .= $customerId;
		$requestFingerprintOrder .= "shopId,";
		$requestFingerprintSeed .= $shopId;
		$requestFingerprintOrder .= "language,";
		$requestFingerprintSeed .= $language;
		$requestFingerprintOrder .= "amount,";
		$requestFingerprintSeed .= $amount;
		$requestFingerprintOrder .= "currency,";
		$requestFingerprintSeed .= $currency;
		$requestFingerprintOrder .= "orderDescription,";
		$requestFingerprintSeed .= $orderDescription;
		$requestFingerprintOrder .= "successUrl,";
		$requestFingerprintSeed .= $successURL;
		$requestFingerprintOrder .= "pendingUrl,";
		$requestFingerprintSeed .= $pendingURL;
		$requestFingerprintOrder .= "confirmUrl,";
		$requestFingerprintSeed .= $confirmURL;
		$requestFingerprintOrder .= "consumerUserAgent,";
		$requestFingerprintSeed .= $consumerUserAgent;
		$requestFingerprintOrder .= "consumerIpAddress,";
		$requestFingerprintSeed .= $consumerIpAddress;
		$requestFingerprintOrder .= "storageId,";
		$requestFingerprintSeed .= $storageId;
		$requestFingerprintOrder .= "orderIdent,";
		$requestFingerprintSeed .= $orderIdent;
		$requestFingerprintOrder .= "orderNumber,";
		$requestFingerprintSeed .= $order->id;
		
		// adds fingerprint order to fingerprint
		$requestFingerprintOrder .= "requestFingerprintOrder";
		$requestFingerprintSeed .= $requestFingerprintOrder;
		
		// computes the request fingerprint
		$requestFingerprint = hash_hmac("sha512", $requestFingerprintSeed, $secret);
		
		//--------------------------------------------------------------------------------//
		// Creates and sends a POST request (server-to-server request) to the
		// Wirecard Checkout Seamless for initiating the checkout.
		//--------------------------------------------------------------------------------//
		
		// initiates the string containing all POST parameters and
		// adds them as key-value pairs to the post fields
		$postFields = "";
		
		$postFields .= "customerId=" . $customerId;
		$postFields .= "&shopId=" . $shopId;
		$postFields .= "&amount=" . $amount;
		$postFields .= "&currency=" . $currency;
		$postFields .= "&paymentType=" . $paymentType;
		$postFields .= "&language=" . $language;
		$postFields .= "&orderDescription=" . $orderDescription;
		$postFields .= "&successUrl=" . $successURL;
		$postFields .= "&cancelUrl=" . $cancelURL;
		$postFields .= "&failureUrl=" . $failureURL;
		$postFields .= "&serviceUrl=" . $serviceURL;
		$postFields .= "&pendingUrl=" . $pendingURL;
		$postFields .= "&confirmUrl=" . $confirmURL;
		$postFields .= "&requestFingerprintOrder=" . $requestFingerprintOrder;
		$postFields .= "&requestFingerprint=" . $requestFingerprint;
		$postFields .= "&consumerUserAgent=" . $consumerUserAgent;
		$postFields .= "&consumerIpAddress=" . $consumerIpAddress;
		$postFields .= "&storageId=" . $storageId;
		$postFields .= "&orderIdent=" . $orderIdent;
		$postFields .= "&windowName=" . $windowName;
		$postFields .= "&orderNumber=" . $order->id;
		
		// initializes the libcurl of PHP used for sending a POST request
		// to the Wirecard data storage as a server-to-server request
		// (please be aware that you have to use a web server where a
		// server-to-server request is enabled)
		$curl = curl_init();
		
		// sets the required options for the POST request via curl
		curl_setopt($curl, CURLOPT_URL, $URL_FRONTEND_INIT);
		curl_setopt($curl, CURLOPT_PORT, $WIRECARD_CHECKOUT_PORT);
		curl_setopt($curl, CURLOPT_PROTOCOLS, $WIRECARD_CHECKOUT_PROTOCOL);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $postFields);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, true);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_USERAGENT, $_SERVER['HTTP_USER_AGENT']);
		
		// sends a POST request to the Wirecard Checkout Platform and stores the
		// result returned from the Wirecard data storage in a string for later use
		$curlResult = curl_exec($curl);
		if (!$curlResult) {
			$error = curl_error($curl);
			var_dump($error);
		}
		
		// closes the connection to the Wirecard Checkout Platform
		curl_close($curl);
		
		//--------------------------------------------------------------------------------//
		// Retrieves the value for the redirect URL.
		//--------------------------------------------------------------------------------//
		
		$redirectURL = "";
		foreach (explode('&', $curlResult) as $keyvalue) {
		    $param = explode('=', $keyvalue);
		    if (sizeof($param) == 2) {
		        $key = urldecode($param[0]);
		        if ($key == "redirectUrl") {
		            $redirectURL = urldecode($param[1]);
		            break;
		        }
		    }
		}
		
		//--------------------------------------------------------------------------------//
		// Redirects consumer to payment page.
		//--------------------------------------------------------------------------------//
		
		if ($redirectURL == "") 
		{
		    echo "<pre>";
		    echo "Frontend Intitiation failed with errors:\n\n";
		    foreach (explode('&', $curlResult) as $keyvalue) {
		        $param = explode('=', $keyvalue);
		        if (sizeof($param) == 2) {
		            $key = urldecode($param[0]);
		            $value = urldecode($param[1]);
		            echo $key . " = " . $value . "\n";
		        }
		    }
		    echo "</pre>";
		} else {
			$this->ci->shopm->updateOrderStatus($order->id, ORDER_STATUS_INITIATED);
		    header("Location: " . $redirectURL);
		}		
	}


	
}


