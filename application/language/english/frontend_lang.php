<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');


/***********************************************************************************************
 * VARIABLES
 ***********************************************************************************************/


/***********************************************************************************************
 * STRINGS
 ***********************************************************************************************/
$lang['press_header'] = "Press";
$lang['press_welcome'] = "Welcome to the press section of TBA21!";
$lang['press_infoheader'] = "Here you can find all of the media-relevant information related to our exhibitions and projects. Press materials are available to download by registered users. Please note that the images are to be used only with the correct copyright credits and with reference to TBA21. If you wish to receive press information, please sign up for our press release distribution list. Please send us a short e-mail to <a href='mailto:press@tba21.org'>press@tba21.org</a>. ";
$lang['press_login_header'] = "Already have a TBA21 account?";
$lang['press_login_info'] = "If you have already registered with us then please sign in with your e-mail address.";
$lang['press_login_email'] = "E-Mail";
$lang['press_login_button'] = "Login";
$lang['press_reg_header'] = "Please sign in to access download section.";
$lang['press_reg_info'] = "To access TBA21's press section, you will need to create a journalist account. Here you can register for our press login area, where you can download all press informations and images. Please fill out all the fields in the form below.";
$lang['press_reg_firstname'] = "First name";
$lang['press_reg_lastname'] = "Last name";
$lang['press_reg_publication'] = "Company/Publication";
$lang['press_reg_email'] = "E-Mail";
$lang['press_reg_button'] = "Register";
$lang['press_error_nologin'] = "E-Mail not found";
$lang['press_error_reg_success'] = "Thank you for registering. You can now use your E-mail address to log in.";

$lang['presskit_header'] = "Press Archive";
$lang['presskit_logged_in_as'] = "Logged in as: ";
$lang['presskit_logout'] = "Logout";
$lang['presskit_denied'] = "Access denied";
$lang['presskit_nologin'] = "You are not logged in.<br/><br/>To gain access to the press archive you need to log in or register <span class='press_nologin'>here</span>.";

$lang['search_button'] = "Search";

$lang['intro'] = "realizes the philanthropic vision of Francesca Habsburg.";

$lang['cookieinfo'] = "We use the technology of cookies to display this site correctly. You accept the usage of cookies by using our website.";
$lang['cookieinfo_accept'] = "Accept";

$lang['newsletter_subscribe_success'] = "Thank you for subscribing.";
$lang['newsletter_unsubscribe_success'] = "You will no longer receive any Newsletters from us.";
$lang['newsletter_email_notunique'] = "This E-mail is already subscribed.";
$lang['newsletter_email_not_found'] = "E-mail not found.";

$lang['cart_headline'] = "Cart";
$lang['cart_item_remove'] = "remove";
$lang['cart_item_more'] = "more";
$lang['cart_item_less'] = "less";
$lang['cart_empty'] = "The cart is empty";
$lang['cart_checkout'] = "Checkout";

$lang['add_to_cart'] = "Add to cart";
$lang['go_to_cart'] = "Go to cart";

$lang['checkout_headline'] = "Checkout";

$lang['shop_firstname'] = "Firstname";
$lang['shop_lastname'] = "Lastname";
$lang['shop_email'] = "E-mail";
$lang['shop_phone'] = "Phone";
$lang['shop_street'] = "Street";
$lang['shop_zipcode'] = "Zip code";
$lang['shop_city'] = "City";
$lang['shop_phone'] = "Phone";
$lang['shop_email'] = "E-mail";
$lang['shop_country'] = "Country";
$lang['shop_terms'] = "TOS";
$lang['shop_terms_accept'] = "Accept"; 
$lang['shop_personal_data'] = "Personal information";
$lang['shop_payment_data'] = "Payment information";
$lang['shop_tos_error'] = "Please read and accept our terms and conditions.";
$lang['shop_cc_error'] = "Creditcard data invalid.";

$lang['shop_creditcard'] = "Credit card";
$lang['shop_paymentmethod'] = "Payment method";

$lang['shop_cc_owner'] = "Card holder";
$lang['shop_cc_number'] = "Credit card number";
$lang['shop_cc_exp_date'] = "Expiration date";
$lang['shop_cc_code'] = "Card security code";

$lang['shop_tos_header'] = "Terms of Service";
$lang['shop_summary'] = "Costs";

$lang['shop_shipment_header'] = "Shipment";
$lang['shop_shipment_method'] = "Select shipment method";

$lang['shipment_costs'] = "Shipment ";
$lang['total'] = "Total";

$lang['finish_checkout'] = "Confirm and pay now";
$lang['checkout_continue'] = "Confirm Informations";

$lang['paymentresult_cancel'] = "You have canceled the payment. You can now close this window.";
$lang['paymentresult_success'] = "Your payment was a success.<br><br>You will receive an E-mail confirming your purchase shortly.<br><br>You can now close this window.";

$lang['shop_session_problem'] = "A problem with your session occured. Please restart your shopping from the cart.";

$lang['shop_finished_headline'] = "Complete purchase";
$lang['shop_finished_text'] = "Your purchase will be comleted a new window. You can leave this page now and navigate through the site.";

$lang['invoice_subject'] = "TBA21 Invoice Nr. WEB-";
$lang['invoice_userheader'] = "Customer data";
$lang['invoice_summary'] = "Summary";
$lang['invoice_payment'] = "Payment data";
$lang['orderconfirmation_subject'] = "Green light order confirmation";

