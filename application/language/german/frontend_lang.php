<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');


/***********************************************************************************************
 * VARIABLES
 ***********************************************************************************************/


/***********************************************************************************************
 * STRINGS
 ***********************************************************************************************/
$lang['press_header'] = "Presse";
$lang['press_welcome'] = "Herzlich willkommen im Pressebereich von TBA21!";
$lang['press_infoheader'] = "Hier finden Sie aktuelle Pressemitteilungen und -bilder zu unseren Ausstellungen und Projekten. Die Verwendung des Materials ist im Rahmen der aktuellen Berichterstattung kostenfrei. Wir bitten Sie aber zu beachten, dass das Bildmaterial nur unter Angabe der angeführten Copyrights und mit Verweis auf TBA21 verwendet werden darf. Sollten Sie unsere Presseinformationen noch nicht bekommen, nehmen wir Sie gerne in den Presseverteiler auf. Bitte schicken Sie uns ein kurzes E-Mail an <a href='mailto:press@tba21.org'>press@tba21.org</a>. Die Pressematerialien stehen registrierten Benutzern zum Download Verfügung.";
$lang['press_login_header'] = "Login mit E-Mail-Adresse";
$lang['press_login_info'] = "Wenn Sie bereits registriert sind, loggen Sie sich bitte mit Ihrer E-Mail-Adresse ein.";
$lang['press_login_email'] = "E-Mail";
$lang['press_login_button'] = "Anmelden";
$lang['press_reg_header'] = "Registrieren";
$lang['press_reg_info'] = "An dieser Stelle können Sie sich einmalig registrieren, um einen Journalisten-Account zu generieren. Damit erhalten Sie Zugang zum Presse-Login-Bereich von TBA21, wo Presseinformationen und -bilder zum Download zur Verfügung stehen. Bitte füllen Sie das nachfolgende Formular vollständig aus.";
$lang['press_reg_firstname'] = "Vorname";
$lang['press_reg_lastname'] = "Nachname";
$lang['press_reg_publication'] = "Medium/Publikation";
$lang['press_reg_email'] = "E-Mail";
$lang['press_reg_button'] = "Registrieren";
$lang['press_error_nologin'] = "E-Mail nicht gefunden";
$lang['press_error_reg_success'] = "Danke für die Registrierung. Sie können sich nun mit der angegebenen E-Mail Adresse anmelden.";

$lang['presskit_header'] = "Presse Archiv";
$lang['presskit_logged_in_as'] = "Angemeldet als: ";
$lang['presskit_logout'] = "Abmelden";
$lang['presskit_denied'] = "Zugang verweigert";
$lang['presskit_nologin'] = "Sie sind nicht angemeldet.<br/><br/>Um Zugang zum Pressearchiv zu erhalten, müssen sie sich entweder <span class='press_nologin'>hier</span> einloggen oder registrieren.";

$lang['search_button'] = "Suchen";

$lang['intro'] = "realizes the philanthropic vision of Francesca Habsburg.";

$lang['cookieinfo'] = "Wir speichern Informationen über Ihren Besuch in sogenannten Cookies. Durch die Nutzung dieser Webseite erklären Sie sich mit der Verwendung von Cookies einverstanden.";
$lang['cookieinfo_accept'] = "Akzeptieren";

$lang['newsletter_subscribe_success'] = "Danke für die Registrierung.";
$lang['newsletter_unsubscribe_success'] = "Sie werden keine weiteren Newsletter von uns erhalten.";
$lang['newsletter_email_notunique'] = "Diese E-mail Adresse ist bereits registriert.";
$lang['newsletter_email_not_found'] = "E-mail Adresse nicht gefunden.";

$lang['cart_headline'] = "Warenkorb";
$lang['cart_item_remove'] = "entfernen";
$lang['cart_item_more'] = "mehr";
$lang['cart_item_less'] = "weniger";
$lang['cart_empty'] = "Der Warenkorb ist leer";
$lang['cart_checkout'] = "Zur Kasse";

$lang['add_to_cart'] = "Zum Warenkorb hinzufügen";
$lang['go_to_cart'] = "Zum Warenkorb wechseln";

$lang['checkout_headline'] = "Checkout";

$lang['shop_firstname'] = "Vorname";
$lang['shop_lastname'] = "Nachname";
$lang['shop_email'] = "E-Mail";
$lang['shop_phone'] = "Telefon";
$lang['shop_street'] = "Straße";
$lang['shop_zipcode'] = "PLZ";
$lang['shop_city'] = "Stadt";
$lang['shop_country'] = "Land";
$lang['shop_terms'] = "AGBs";
$lang['shop_terms_accept'] = "akzeptieren";
$lang['shop_personal_data'] = "Persönliche Informationen";
$lang['shop_payment_data'] = "Zahlungsinformationen";
$lang['shop_tos_error'] = "Bitte lesen die AGBs und erklären sie sich damit einverstanden";
$lang['shop_cc_error'] = "Kreditkarten informationen ungültig";

$lang['shop_creditcard'] = "Kreditkarte";
$lang['shop_paymentmethod'] = "Zahlungsoptionen";

$lang['shop_cc_owner'] = "Kreditkartenhalter";
$lang['shop_cc_number'] = "Kreditkartennummer";
$lang['shop_cc_exp_date'] = "Auslaufdatum";
$lang['shop_cc_code'] = "Sicherheitscode";

$lang['shop_tos_header'] = "Allgemeine Geschäftsbedingungen";
$lang['shop_summary'] = "Kosten";

$lang['shop_shipment_header'] = "Lieferung";
$lang['shop_shipment_method'] = "Liefermethode auswählen";

$lang['shipment_costs'] = "Lieferkosten";
$lang['total'] = "Gesamt";

$lang['finish_checkout'] = "Bestellung verbindlich abschliessen";
$lang['checkout_continue'] = "Informationen bestätigen";

$lang['paymentresult_cancel'] = "Sie haben die Zahlung abgebrochen. Sie können dieses Fenster nun schliessen.";
$lang['paymentresult_success'] = "Die Zahlung war erfolgreich.<br><br>Sie erhalten in Kürze eine Bestellbestätigung via E-Mail.<br><br>Sie können dieses Fenster nun schließen";

$lang['shop_session_problem'] = "Es gab Probleme mit ihrer Session. Bitte starten sie Ihren Einkauf erneut vom Warenkorb.";

$lang['shop_finished_headline'] = "Einkauf abschliessen";
$lang['shop_finished_text'] = "Ihr Einkauf wird in einem neuen Fenster abgeschlossen. Sie können nun jederzeit auf der Seite weiternavigieren.";

$lang['invoice_subject'] = "TBA21 Rechnung Nr. WEB-";
$lang['invoice_userheader'] = "Kundeninformation";
$lang['invoice_summary'] = "Leistung";
$lang['invoice_payment'] = "Zahlungsinformationen";

$lang['orderconfirmation_subject'] = "Green light Bestellbestätigung";