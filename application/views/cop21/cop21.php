<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html class="scroller" xmlns="http://www.w3.org/1999/xhtml" lang="en" xml:lang="en">
<head>

	<meta name="viewport" content="width=800, minimum-scale=0.2, maximum-scale=2.5, user-scalable=yes">

	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	
    <meta charset="UTF-8">
	
	<meta property="og:title" content="The Current"/>
	<meta property="og:type" content="website"/>
	<meta property="og:image" content=""/>
	<meta name="description" content="The Current is a three-year exploratory fellowship program in the Pacific that takes creative prac- tice out of the studio, science out of the lab, and the participants out of their comfort zones. Working collaboratively across disciplines in unpredictable environments, we challenge ourselves to imagine new processes of knowledge production and new solutions for the environment."/>
	<meta name="google" content="notranslate" />

	<title>The Current</title>
	<link rel="stylesheet" type="text/css" href="<?=site_url('items/cop21/' . 'desktop.css') ?>">
	<link rel="stylesheet" type="text/css" href="<?=site_url('items/cop21/' . 'lightbox.css') ?>">
	
	<link rel="icon" type="image/ico" href="<?=site_url('items/cop21/' . 'favicon.ico') ?>"/>
	
	<script type="text/javascript" src="<?=site_url('items/cop21/' . 'jquery-2.1.1.min.js') ?>"></script>
	<script type="text/javascript" src="<?=site_url('items/cop21/' . 'lightbox.js') ?>"></script>
	
	
		
	<!-- VARIABLES -->
	<script type="text/javascript">
                       
            
	</script>	
	
	<!-- YT -->
	
	<!-- GA -->
	<script>
	  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
	
	  ga('create', 'UA-5934189-69', 'auto');
	  ga('send', 'pageview');
	
	</script>
	
	
    <!-- FB -->
	
	
	<!-- THE MAGIC -->
	<script type="text/javascript">
        var wWidth;
		var wHeight;
		
		
		$(document).ready(function()
		{	
			resize();
			addListeners();
			setTimeout(function(){
				resize();
			},500);
			
			setTimeout(function(){
				resize();
			},2000);
		});
		
		$(window).resize(function()
		{
			resize();
		});
		
		
		function addListeners()
		{
			$('#impressum').click(function(){
				$('#impressum_text').slideToggle('slow');
			});
			
			
			$('.collapsable').click(function(){
			
				var elem = $(this).parent().find('.collapse_holder');
				elem.slideToggle('slow',function(){
					
				});
				
				$("html, body").animate({
		                scrollTop: elem.offset().top+"px"
		            }, "slow");
			});

		}
		
		
		
		function resize()
		{
			wWidth = $(window).width();
			wHeight = $(window).height();
			
			var ratio = wWidth / wHeight;
			
			var coverH = $('#cover_text').height()+220;
			if(wHeight<coverH)
			{
				$('#cover').css({height: coverH});
			}
			else
			{
				$('#cover').css({height: wHeight});
			}
			
			if(ratio <= 1.33)
			{
				var imageW = wHeight*1.33;
				var left = (wWidth - imageW)/2;
				
				//var newW = 800+parseInt(left);
				
				
				
				if(wWidth>800)
				{
					$('#cover_image').css({left: left, width: wWidth+(left*(-1))});
				}
				else
				{
					$('#cover_image').css({left: left, width: 800+(left*(-1))});
				}
				
			}
			else
			{
				$('#cover_image').css({left: 0, width: '100%'});
			}
		
		}       
            
	</script>	
</head>
<body>
<div id="cover">
 	<div id="cover_image"></div>	
	<div id="cover_op"></div>	
 	 <div id="cover_text" style="position:absolute;top:40px;width:100%;z-index:100">
 	 	  <img style="width:130px;" src="<?= site_url('items/cop21/' . 'logo_white.png')?>" /><br/><br/>
		 <div class="regular" style="margin-top:0px;text-transform:uppercase">presents:<br/><br/><span style="font-size:40px;"> The Current</span><br/><br/> <span style="font-size:18px">a new creative approach to knowledge production</span></div>
		 <div class="regular" style="font-size:16px;margin-top:40px;"><span class="">The Current is a three-year exploratory fellowship program in the Pacific that takes creative practice<br/> out of the studio, science out of the lab, and the participants out of their comfort zones.<br/> Working collaboratively across disciplines in unpredictable environments, we challenge ourselves <br/>to imagine new processes of knowledge production and new solutions for the environment.</span><br/>
	<!-- <br/><a target="_blank" href="http://www.solutionscop21.org/en/">SOLUTIONS COP21</a> at Grand Palais<br/> December 7. 2015, 3PM <br>
Paris, France<br/> -->
		 </div>
	</div>
	
	
 </div>
 <div id="container">
	 <div id="text_holder">
		 <div class="text_item ptreg" style="font-size:14px;text-align:left;">
		In 2015, TBA21 has intensified its focus on the urgent need to protect our natural resources and environment, particularly the oceans. The foundation is pursuing this agenda by creating The Current, a tailor-made tool for commissioning and disseminating ambitious and unconventional projects that defy traditional art categories, reflecting the vision and approach of its founder and chairwoman, Francesca Habsburg.<br/>
		<br/>
		<a style="text-decoration:underline;" target="_blank" href="<?=site_url('items/cop21/' . 'THE-CURRENT-ENGLISH-PARIS.pdf') ?>">ENGLISH press release</a><br/>
		<a style="text-decoration:underline;" target="_blank" href="<?=site_url('items/cop21/' . 'THE-CURRENT-GERMAN-PARIS.pdf') ?>">GERMAN press release</a><br/>
		<a style="text-decoration:underline;" target="_blank" href="<?=site_url('items/cop21/' . 'THE-CURRENT-FRANCAIS-PARIS.pdf') ?>">FRENCH press release</a><br/>
		<a style="text-decoration:underline;" target="_blank" href="<?=site_url('items/cop21/' . 'THE-CURRENT-CHINESE-PARIS.pdf') ?>">CHINESE press release</a>
		</div>
		<div class="text_item ptreg" style="font-size:18px;text-align:left;color:#000000">“In spite of the unprecedented wealth of scientific information available, global environ- mental woes are still largely underestimated and poorly communicated. Art can be a powerful weapon if used well, by challenging us to reconsider the way we think, feel, and live instead of just conforming to the rules of the growing art market. After all, the next 10 years are going to be the most important in the next 10,000. Therefore it’s natural for me to invest everything at my disposal in becoming an agent of change.” <br/><br/><span class="bold" style="font-size:18px">(Francesca Habsburg, Chairperson of TBA21)</span></div>
		
		<div class="text_item ptreg" style="margin-right:0px;font-size:18px;text-align:left;color:#000000">“This program employs creativity as a trigger for the production of knowledge: by directly engaging the world’s top artists and scientists of today with the most urgent topics of our time, and by trusting in the artists’ unique powers of communication, we aim to reach the broadest possible audience, inspiring and empowing them to become agents of change.” <br/><br/><span class="bold" style="font-size:18px">(Markus Reymann,<br> Director, The Current)</span></div>
		
	<!-- YT EMBED -->
		<iframe style="margin-top:30px;" width="800" height="450" src="https://www.youtube.com/embed/88mvSIKrC3A?rel=0" frameborder="0" allowfullscreen></iframe>
		
	
	</div>	
	
<div id="current_holder">
	<div class="bold collapsable" style="font-size:28px;letter-spacing:3px;margin-bottom:0px;color:#000000;text-stransform:uppercase;">The Current is the exploratory soul of TBA21<br/><span style="font-size:12px; font-weight:normal;">Click for more</span></div>
	<div id="olafur_content_holder" class="collapse_holder">
		<div class="olafur_text ptreg">	
			<div class="worlds_item">The ocean and its coastal communities provides a singular arena in which sociopolitical, economic, and environmental factors converge with the spirit of exploration. The Current seeks to redefine the culture of exploration, exchange of ideas and discovery in the 21st century.</div>
			<div class="worlds_item" style="float:right;"> Organized in three-year cycles, The Current is a multiphase fellowship program that gives artists, curators, scientists, marine biologists, anthropologists, and other cultural producers a platform to cultivate interdisciplinary thought and transfer of knowledge. Embracing the notion of the journey as a goal in itself, participants in The Current will join an annual expedition on the research vessel Dardanella. Avoiding the structures of conventional conferences, think tanks, and residencies, The Current reimagines knowledge production and the methods through which we present, understand, and exchange ideas.</div>	
		</div>	
	</div>
</div>

<div id="structure_holder">
	<div class="bold collapsable" style="font-size:28px;letter-spacing:3px;margin-bottom:0px;color:#000000;text-stransform:uppercase;">Structure and Timeline<br/><span style="font-size:12px; font-weight:normal;">Click for more</span></div>
	<div id="olafur_content_holder" class="collapse_holder">
		<div class="olafur_text ptreg">	
			<div class="worlds_item">In the spirit of TBA21’s aquatic heart, each three-year cycle is called The Current. Each Current will be led by three curators (Expedition Leaders) selected by TBA21. Each Expedition Leader guides one expedition annually over the course of three years and proposes five participants (Fellows), who can take part in one or several expeditions throughout The Current. Each Fellow will participate in a 10- to 14-day expedition aboard the Dardanella. A small documentary film team will accompany each expedition. Participants will be able to use footage produced on board as a resource in the future if they so choose. Fellows then contribute to one public presentation, ideally over two to three days, informed by their experiences at sea and the joint fieldwork. Moreover, TBA21 will release an online publication that embraces all media with the aim to share and enrichen the experience of The Current with a larger community.</div>
			<div class="worlds_item" style="float:right;font-size:12px;">Current 1, 2015–18:<br/>
Current 1: Papua New Guinea I, September 30–October 9, 2015<br/>
Current 1: Tuamotu, July 1- 15, 2016<br/>
The Current Convening Kingston, Jamaica, March 2016<br/>
Expedition Leader: Ute Meta Bauer, founding Director of the NTU Centre for Contemporary Art Singapore, a research center of Nanyang Technological University<br/>
<br/>
Current 1: Papua New Guinea II, October 11-18, 2015<br/>
Current 1: Marquesas Islands, October 3-14, 2015 (tbc)<br/>
The Current Convening Istanbul, April 2016<br/>
Expedition Leader: Cesar Garcia, Founder, Director and Chief curator of The Mistake Room, Los Angeles<br/>
<br/>
Current 1: Fiji April 25 - May 8, 2016 Expedition Leader: Damian Christinger<br/>
<br/>
TBA21<br/>
Köstlergasse 1<br/>
1060 Vienna, Austria<br/>
T. +43 (1) 513 98 56<br/>
F. +43 (1) 513 98 56 22 <br/>
<a href="mailto:thecurrent@tba21.org">thecurrent@tba21.org</a><br/> 
<a target="_blank" href="http://www.tba21.org/ocean">www.tba21.org/ocean</a></div>	
		</div>	
	</div>
</div>


<div id="tba_holder">
	<div class="bold collapsable" style="font-size:28px;letter-spacing:3px;margin-bottom:0px;color:#000000;text-stransform:uppercase;">Thyssen-Bornemisza Art Contemporary<br/><span style="font-size:12px; font-weight:normal;">Click for more</span></div>
	<div id="olafur_content_holder" class="collapse_holder">
		<div class="olafur_text ptreg">	
			<div class="worlds_item">Through its activities, Thyssen-Bornemisza Art Contemporary encourages private and public collec- tors, museums, and institutions to undertake cause related art projects to inspire us to engage with the major issues of our time. After more than 10 years of collecting, commissioning projects, and engaged exhibition practice, TBA21 has established a highly respected collection of more than 700 contemporary artworks in the field of new media, including film, video, light, sound and mixed-media installations, sculpture, painting, photography, and performance. TBA21 has also successfully co-commissioned about 60 projects, including multimedia installations, sound compositions, and contemporary architecture. The foundation sustains a far-reaching regional and international orientation, has an ongoing institutional commitment to the growing importance of art made in different parts of the world, and explores modes of presentation that are intended to pro- voke and broaden the way viewers perceive and experience art.</div>
			<div class="worlds_item" style="float:right;">
				<span class="bold">TBA21</span><br/>
TBA21 Founder & Chairwoman: Francesca Habsburg, Executive Director<br/>
<br/>
<span class="bold">The Current</span><br/>
Markus Reymann, Director Co-commissioner, Daniela Zyman, Ute Meta Bauer, Expedition Leader Cesar Garcia, Expedition Leader Damian Christinger, Expedition Leader<br/>
<br/>
<span class="bold">TBA21 The Current science advisory board:</span><br/>
Dr. David Gruber, CUNY<br/>
Patty Elkus, Board member of Scripps Institute of Oceanography Proff. Mark Wigley, Columbia University<br/>
Dr. Maria Spiropulu, Caltech and CERN<br/>
Dr. Dayne Buddo, UWI<br/>
Octavio Aburto, Scripps Institute of Oceanography<br/>
Dr. Josef Penninger, Institute of Molecular Biotechnology IMBA Prof. Neri Oxman, MIT Media Lab<br/>
Maria Wilhelm, Avatar Alliance Foundation<br/>
			</div>	
		</div>	
	</div>
</div>


<div id="gallery_holder">
	<div class="bold" style="font-size:28px;letter-spacing:3px;color:#000000;">Gallery</div>
	<div class="ptit" style="font-size:12px;margin-top:20px;margin-bottom:30px;">Click to enlarge</div>
	<div id="image_holder">
		
		
		<a href="<?= site_url('items/cop21/' . '11-Newell-Harry-copyright-Jegan-Vincent-de-Paul-Papua1.jpg')?>" data-lightbox="gallery" data-title="Artist Newell Harry being presented a traditional head piece in Boga Boga,  Milne Bay Papua New Guinea, October 2015. Photograph: Jegan Vincent de Paul. Copyright TBA21.">
		<div class="image_item_holder" style="background-image: url('<?= site_url('items/cop21/' . '11-Newell-Harry-copyright-Jegan-Vincent-de-Paul-Papua1.jpg')?>')"></div>
		</a>
		
		<a href="<?= site_url('items/cop21/' . '20-Ute-Meta-Bauer-and-Armin-Linke-copyright-Jegan-Vincent-de-Paul-Papua1.jpg')?>" data-lightbox="gallery" data-title="Expedition Leader Ute Meta Bauer and artist and filmmaker Armin Linke in conversation on the Dardanella, Papua New Guinea, October 2015. Photograph: Jegan Vincent de Paul. Copyright TBA21.">
		<div class="image_item_holder" style="background-image: url('<?= site_url('items/cop21/' . '20-Ute-Meta-Bauer-and-Armin-Linke-copyright-Jegan-Vincent-de-Paul-Papua1.jpg')?>')"></div>
		</a>
	
		<a href="<?= site_url('items/cop21/' . 'DJI_0010.jpg')?>" data-lightbox="gallery" data-title="The Dardanella anchored at Dina's beach, Milne Bay, Papua New Guinea, October 2015. Copyright TBA21.">
		<div class="image_item_holder" style="margin-right:0px;background-image: url('<?= site_url('items/cop21/' . 'DJI_0010.jpg')?>')"></div>
		</a>
		
		<a href="<?= site_url('items/cop21/' . 'DSC_6013.jpg')?>" data-lightbox="gallery" data-title="Artists Newell Harry and Armin Linke and Expedition Leader Ute Meta Bauer in an elementary school in Samarai, Milne Bay, Papua New Guinea, October 2015. Copyright TBA21.">
		<div class="image_item_holder" style="background-image: url('<?= site_url('items/cop21/' . 'DSC_6013.jpg')?>')"></div>
		</a>
		
		<a href="<?= site_url('items/cop21/' . 'DSC_6274.jpg')?>" data-lightbox="gallery" data-title="Artist Tue Greenfort launching a kite he constructed aboard the Dardanella in Boga Boga, Milne Bay, Papua New Guinea, October 2015. Copyright TBA21.">
		<div class="image_item_holder" style="background-image: url('<?= site_url('items/cop21/' . 'DSC_6274.jpg')?>')"></div>
		</a>
		
		<a href="<?= site_url('items/cop21/' . 'Filming-public-event-copyright-Tuan-Andrew-Nguyen-Papua2.jpg')?>" data-lightbox="gallery" data-title="The Propeller Group, participants of the second expedition to Papua New Guinea filming a local event. Papua New Guinea, October 2015. Photograph: Tuan Andrew Nguyen. Copyright TBA21.">
		<div class="image_item_holder" style="margin-right:0px;background-image: url('<?= site_url('items/cop21/' . 'Filming-public-event-copyright-Tuan-Andrew-Nguyen-Papua2.jpg')?>')"></div>
		</a>
		
		<a href="<?= site_url('items/cop21/' . 'IMG_0015.jpg')?>" data-lightbox="gallery" data-title="TBA21 The Current director Markus Reymann filming underwater on a dive site called “Black and Silver” near Tuboa Island, Milne Bay, Papua New Guinea, October 2015. Copyright TBA21. ">
		<div class="image_item_holder" style="background-image: url('<?= site_url('items/cop21/' . 'IMG_0015.jpg')?>')"></div>
		</a>
		
		<a href="<?= site_url('items/cop21/' . 'L1010012.jpg')?>" data-lightbox="gallery" data-title="Expedition Leader Cesar Garcia and the participants of the second expediton to Papua New Guinea, October 2015. Copyright TBA21.">
		<div class="image_item_holder" style="background-image: url('<?= site_url('items/cop21/' . 'L1010012.jpg')?>')"></div>
		</a>
		
		<a href="<?= site_url('items/cop21/' . 'L1010036.jpg')?>" data-lightbox="gallery" data-title="Expedition team and Expedition Leader in front of Papua New Guinea Airlines desk. Papua New Guinea, October 2015. Copyright TBA21.">
		<div class="image_item_holder" style="margin-right:0px;background-image: url('<?= site_url('items/cop21/' . 'L1010036.jpg')?>')"></div>
		</a>
		
		<a href="<?= site_url('items/cop21/' . 'Little-Suns-copyright-Phunam-propeller-group-Papua2.jpg')?>" data-lightbox="gallery" data-title="Inhabitants wearing Olafur Eliasson’s “Little Suns” (solar charged lamps). Papua New Guinea, October 2015. Copyright TBA21.">
		<div class="image_item_holder" style="background-image: url('<?= site_url('items/cop21/' . 'Little-Suns-copyright-Phunam-propeller-group-Papua2.jpg')?>')"></div>
		</a>
		
		<a href="<?= site_url('items/cop21/' . 'PA072163.jpg')?>" data-lightbox="gallery" data-title="TBA21 Founder and Chairwoman Francesca Habsburg diving a B-17 World War II bomber called Black Jack in 47 meters of depth at Boga Boga Milne Bay, Papua New Guinea, October 2015. Photograph: Craig de Wit. Copyright TBA21.">
		<div class="image_item_holder" style="background-image: url('<?= site_url('items/cop21/' . 'PA072163.jpg')?>')"></div>
		</a>
		
		<a href="<?= site_url('items/cop21/' . 'PA220074.jpg')?>" data-lightbox="gallery" data-title="Sunset with the Dardanella and dugouts, Papua New Guinea, October 2015. Photograph: Francesca Habsburg. Copyright TBA21.">
		<div class="image_item_holder" style="margin-right:0px;background-image: url('<?= site_url('items/cop21/' . 'PA220074.jpg')?>')"></div>
		</a>
	
	<!--	
		<a href="<?= site_url('items/cop21/' . 'portaits-by-propeller-group-copyright-Jamie-Shi-papua2.jpg')?>" data-lightbox="gallery" data-title="">
		<div class="image_item_holder" style="background-image: url('<?= site_url('items/cop21/' . 'portaits-by-propeller-group-copyright-Jamie-Shi-papua2.jpg')?>')"></div>
		</a>
	-->
		
		
		
		</div>
</div>


<div id="press_holder">
	<div class="bold collapsable" style="font-size:28px;letter-spacing:3px;margin-bottom:0px;color:#000000;text-stransform:uppercase;">Press Information<br/><span style="font-size:12px; font-weight:normal;">Contact us to receive more information and high resolution images</span></div>
	<div id="olafur_content_holder" class="collapse_holder">
		<a style="text-decoration:underline;color:#000" href="mailto:press@tba21.org">
				<div class="bold" style="font-size:24px;color:#000;margin-top:20px;margin-bottom:20px;">press@tba21.org</div>
			</a>
		<div class="olafur_text ptreg">	
			<div class="worlds_item" style="text-align:center;">International Press<br/> 
			Pickles PR<br/>
Juan Sánchez<br/> 
<a href="mailto:juan@picklespr.com">juan@picklespr.com</a><br/> 
T +44 788 223 7732</div>
			<div class="worlds_item" style="text-align:center;float:right;">
				International Communication Coordination<br/> 
				Imagine<br/>
Pierre Collet<br/>
<a href="mailto:collet@aec-imagine.com">collet@aec-imagine.com</a><br/>
T +33 680 84 87 71
			</div>	
		</div>	
	</div>
</div>

<div id="partner_bg">
	<div id="partners_holder">
		<div class="bold collapsable"  style="font-size:28px;letter-spacing:3px;margin-bottom:40px;color:#000000;">Contact<br/><span style="font-size:12px; font-weight:normal;">Click for more</span></div>
		
		
		<div class="collapse_holder">
			<a style="text-decoration:underline;color:#000" href="mailto:thecurrent@tba21.org">
				<div class="bold" style="font-size:24px;color:#000;margin-bottom:20px;">thecurrent@tba21.org</div>
			</a>
			<div class="patner_item" style="margin:0px auto">
				<img style="width:100px;" class="partner_image" src="<?= site_url('items/cop21/' . 'tba_logo.png')?>"/>
				<div class="bold" style="font-size:14px;text-transform:uppercase;margin-bottom:30px;color:#000000;">Thyssen-Bornemisza<br/> Art Contemporary</div>
				<div class="ptreg" style="font-size:12px;margin-bottom:40px;color:#202020">Scherzergasse 1A | 1020 Vienna</div>
				<a target="_blank" href="http://www.tba21.org/ocean/">
					<div class="partner_button" style="margin-top:60px">www.tba21.org/ocean</div>
				</a>
				
				<div class="social_holder">
					<a target="_blank" href="https://www.facebook.com/TBA21">
						<img class="social_img" src="<?= site_url('items/cop21/' . 'fb_logo.png')?>"/>
					</a>
					<a target="_blank" href="https://twitter.com/TBA21">
						<img class="social_img" src="<?= site_url('items/cop21/' . 'twitter_logo.png')?>"/>
					</a>
					<a target="_blank" href="https://instagram.com/tba_21/">
						<img class="social_img" style="margin-right:0px;" src="<?= site_url('items/cop21/' . 'instagram_logo.png')?>"/>
					</a>
				</div>
			</div>
			
		
			<div id="impressum_holder">
			<div id="impressum" class="bold" style="font-size:14px;letter-spacing:2px;margin-bottom:30px;">IMPRINT</div>
			<div id="impressum_text" class="ptreg" style="font-size:12px;">Copyright © 2015<br/>
				Thyssen Bornemisza Art Contemporary<br/>
				Scherzergasse 1A, 1020 Vienna
			</div>
		</div>
	</div>
	</div>
</div>





	 	</div> <!-- Container END -->
	</body>
</html>
