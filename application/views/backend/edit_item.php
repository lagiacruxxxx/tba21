
            
            <div class="content_h1">Edit item</div>
            
            <!-- CSS -->
        	<link rel="stylesheet" type="text/css" href="<?=site_url("items/backend/css/item.css"); ?>">
        
        	<!-- JS -->
        	<script type="text/javascript" src="<?=site_url("items/backend/ckeditor/ckeditor.js"); ?>"></script>
        	<script type="text/javascript" src="<?=site_url("items/backend/ckeditor/adapters/jquery.js"); ?>"></script>
        	<script type="text/javascript" src="<?=site_url("items/backend/js/item.js"); ?>"></script>
        	<script type="text/javascript" src="<?=site_url("items/backend/js/module_text.js"); ?>"></script>
        	<script type="text/javascript" src="<?=site_url("items/backend/js/module_image.js"); ?>"></script>
        	<script type="text/javascript" src="<?=site_url("items/backend/js/module_bulletpoint.js"); ?>"></script>
        	<script type="text/javascript" src="<?=site_url("items/backend/js/module_video.js"); ?>"></script>
        	<script type="text/javascript" src="<?=site_url("items/backend/js/module_html.js"); ?>"></script>
        	<script type="text/javascript" src="<?=site_url("items/backend/js/module_headline.js"); ?>"></script>
        	<script type="text/javascript" src="<?=site_url("items/backend/js/module_download.js"); ?>"></script>
        	<script type="text/javascript" src="<?=site_url("items/backend/js/module_2col_image.js"); ?>"></script>
        	<script type="text/javascript" src="<?=site_url("items/backend/js/module_store.js"); ?>"></script>
        	
        	<!-- moduledata -->
        	<script>
        	</script>
          
            <div id="item_container" class="unselectable" item_id="<?= $item->id?>">
                <div id="item_headline" class="has_placeholder" data-text="Enter text here"><?= $item->name?></div>
                <div id="item_detail">
                    <div id="item_detailimg" <?= $item->detail_type == DETAIL_TYPE_HTML ? 'style="display: none;"' : ''?>>
                        <?php if($item->detail_img != ""):?>
                            <img src="<?= site_url('items/uploads/detailimg/' . $item->detail_img)?>" filename="<?= $item->detail_img?>" />
                        <?php else:?>
                            <img src="<?= site_url('items/uploads/detailimg/image_upload_placeholder.png')?>" filename="image_upload_placeholder.png" />
                        <?php endif;?>
                        <input type="file" id="detailimg_upload" accept=".png,.jpg,.jpeg,.gif" uploadpath="items/uploads/detailimg" > 
                    </div>
                    <div id="item_detailimg_credits" class="has_placeholder" data-text="Enter photo credits here" <?= $item->detail_type == DETAIL_TYPE_HTML ? 'style="display: none;"' : ''?>><?= $item->detail_img_credits ?></div>
                    
                    <div id="item_detail_html" <?= $item->detail_type == DETAIL_TYPE_IMAGE ? 'style="display: none;"' : ''?>>
                        <div id="item_detail_html_overlay"></div>
                        <div id="item_detail_html_content" class="has_placeholder" data-text="Enter HTML code here"><?= $item->detail_html?></div>
                        <div id="item_detail_html_content_mobile"><?= $item->detail_html_mobile?></div>
                    </div>
                    <div id="item_detail_type_switch">
                        <div detail_type="<?= DETAIL_TYPE_IMAGE?>" class="item_detail_type_switch <?= $item->detail_type == DETAIL_TYPE_IMAGE ? 'item_detail_type_active' : ''?>">Image</div>
                        <div detail_type="<?= DETAIL_TYPE_HTML?>" class="item_detail_type_switch <?= $item->detail_type == DETAIL_TYPE_HTML ? 'item_detail_type_active' : ''?>">HTML</div>
                    </div>
                </div>
                
                
                <div id="item_header" class="has_placeholder" data-text="Enter text here"><?= $item->header?></div>
                
                <div id="col_container">
                    <?php $module_counter=0;?>
                    <div id="col_left" class="col has_placeholder" data-text="Drop modules here"><?php foreach($modulesText->result() as $module):?>
                        <?php if($module->column_id == 0):?>
                            <div module_id=<?= $module_counter++?> class="module module_text has_placeholder" data-text="Enter text here" style="top: <?= $module->top?>px;"><?= $module->content?></div>
                        <?php endif;?>
                        <?php endforeach;?><?php foreach($modulesImage->result() as $module):?>
                            <?php if($module->column_id == 0):?>
                                <div module_id=<?= $module_counter++?> class="module module_image" data-text="Enter text here" style="top: <?= $module->top?>px;"><img src="<?= site_url('items/uploads/module_image/' . $module->fname)?>" fname="<?= $module->fname?>"/><input type="file"  id="module_img_upload_<?= $module_counter -1?>" accept=".png,.jpg,.jpeg" uploadpath="items/uploads/module_image"></div>
                            <?php endif;?>
                        <?php endforeach;?><?php foreach($modulesBulletpoint->result() as $module):?>
                            <?php if($module->column_id == 0):?>
                                <div module_id=<?= $module_counter++?> class="module module_bulletpoint has_placeholder" data-text="Enter text here" style="top: <?= $module->top?>px;"><div class="enum_head"><span><?= $module->header?></span></div><div class="enum_body"><?= $module->content?></div></div>
                            <?php endif;?>
                        <?php endforeach;?><?php foreach($modulesVideo->result() as $module):?>
                            <?php if($module->column_id == 0):?>
                                <div module_id=<?= $module_counter++?> class="module module_video has_placeholder" data-text="Insert video here" style="top: <?= $module->top?>px;" code="<?= $module->code?>" start="<?= $module->start?>"><div class="module_video_overlay"></div><iframe class="module_video_iframe" src="<?= 'https://www.youtube.com/embed/' . $module->code . '?start=' . $module->start . '&amp;wmode=transparent'?>" frameborder="0" allowfullscreen="" wmode="Opaque"></iframe></div>
                            <?php endif;?>
                        <?php endforeach;?><?php foreach($modulesHTML->result() as $module):?>
                            <?php if($module->column_id == 0):?>
                                <div module_id=<?= $module_counter++?> class="module module_html has_placeholder" style="top: <?= $module->top?>px;" module_html_id="<?= $module->id?>"><div class="module_html_overlay"></div><div class="module_html_content" data-text="Insert HTML here"><?= $module->html?></div></div>
                            <?php endif;?>
                        <?php endforeach;?><?php foreach($modulesHeadline->result() as $module):?>
                            <?php if($module->column_id == 0):?>
                                <div module_id=<?= $module_counter++?> class="module module_headline has_placeholder" style="top: <?= $module->top?>px;"><span><?= $module->headline?></span></div>
                            <?php endif;?>
                        <?php endforeach;?><?php foreach($modulesDownload->result() as $module):?>
                            <?php if($module->column_id == 0):?>
                                <div module_id=<?= $module_counter++?> class="module module_download has_placeholder" style="top: <?= $module->top?>px;"><div class="module_download_field" fname="<?= $module->fname?>"><?= $module->description?></div></div>
                            <?php endif;?>
                        <?php endforeach;?><?php foreach($modules2ColImage->result() as $module):?>
                        <?php if($module->column_id == 0):?>
                            <div module_id=<?= $module_counter++?> class="module module_2col_image" data-text="Enter text here" style="top: <?= $module->top?>px;"><img src="<?= site_url('items/uploads/module_image/' . $module->fname)?>" fname="<?= $module->fname?>" /><input type="file"  id="module_img_upload_<?= $module_counter -1?>" accept=".png,.jpg,.jpeg" uploadpath="items/uploads/module_image"></div>
                        <?php endif;?>
                    <?php endforeach;?><?php foreach($modulesStore->result() as $module):?>
                            <?php if($module->column_id == 0):?>
                                <div module_id=<?= $module_counter++?> class="module module_store has_placeholder" style="top: <?= $module->top?>px;"><div class="module_store_text"><?= $module->content?></div><div class="module_store_button store_add" magento_id="<?= $module->magento_id?>">Add to cart</div><div class="module_store_button store_checkout">Checkout</div></div>
                            <?php endif;?>
                        <?php endforeach;?></div>
                    
                    <div id="col_right" class="col has_placeholder" data-text="Drop modules here"><?php foreach($modulesText->result() as $module):?>
                        <?php if($module->column_id == 1):?>
                            <div module_id=<?= $module_counter++?> class="module module_text has_placeholder" data-text="Enter text here" style="top: <?= $module->top?>px;"><?= $module->content?></div>
                        <?php endif;?>
                    <?php endforeach;?><?php foreach($modulesImage->result() as $module):?>
                        <?php if($module->column_id == 1):?>
                            <div module_id=<?= $module_counter++?> class="module module_image" data-text="Enter text here" style="top: <?= $module->top?>px;"><img src="<?= site_url('items/uploads/module_image/' . $module->fname)?>" fname="<?= $module->fname?>" /><input type="file"  id="module_img_upload_<?= $module_counter -1?>" accept=".png,.jpg,.jpeg" uploadpath="items/uploads/module_image"></div>
                        <?php endif;?>
                    <?php endforeach;?><?php foreach($modulesBulletpoint->result() as $module):?>
                        <?php if($module->column_id == 1):?>
                            <div module_id=<?= $module_counter++?> class="module module_bulletpoint has_placeholder" data-text="Enter text here" style="top: <?= $module->top?>px;"><div class="enum_head"><span><?= $module->header?></span></div><div class="enum_body"><?= $module->content?></div></div>
                        <?php endif;?>
                    <?php endforeach;?><?php foreach($modulesVideo->result() as $module):?>
                        <?php if($module->column_id == 1):?>
                            <div module_id=<?= $module_counter++?> class="module module_video has_placeholder" data-text="Insert video here" style="top: <?= $module->top?>px;" code="<?= $module->code?>" start="<?= $module->start?>"><div class="module_video_overlay"></div><iframe class="module_video_iframe" src="<?= 'https://www.youtube.com/embed/' . $module->code . '?start=' . $module->start . '&amp;wmode=transparent'?>" frameborder="0" allowfullscreen="" wmode="Opaque"></iframe></div>
                        <?php endif;?>
                    <?php endforeach;?><?php foreach($modulesHTML->result() as $module):?>
                        <?php if($module->column_id == 1):?>
                            <div module_id=<?= $module_counter++?> class="module module_html has_placeholder" style="top: <?= $module->top?>px;" module_html_id="<?= $module->id?>"><div class="module_html_overlay"></div><div class="module_html_content" data-text="Insert HTML here"><?= $module->html?></div></div>
                        <?php endif;?>
                    <?php endforeach;?><?php foreach($modulesHeadline->result() as $module):?>
                        <?php if($module->column_id == 1):?>
                            <div module_id=<?= $module_counter++?> class="module module_headline has_placeholder" style="top: <?= $module->top?>px;"><span><?= $module->headline?></span></div>
                        <?php endif;?>
                    <?php endforeach;?><?php foreach($modulesDownload->result() as $module):?>
                        <?php if($module->column_id == 1):?>
                            <div module_id=<?= $module_counter++?> class="module module_download has_placeholder" style="top: <?= $module->top?>px;"><div class="module_download_field" fname="<?= $module->fname?>"><?= $module->description?></div></div>
                        <?php endif;?>
                    <?php endforeach;?><?php foreach($modules2ColImage->result() as $module):?>
                        <?php if($module->column_id == 1):?>
                            <div module_id=<?= $module_counter++?> class="module module_2col_image" data-text="Enter text here" style="top: <?= $module->top?>px;"><img src="<?= site_url('items/uploads/module_image/' . $module->fname)?>" fname="<?= $module->fname?>" /><input type="file"  id="module_img_upload_<?= $module_counter -1?>" accept=".png,.jpg,.jpeg" uploadpath="items/uploads/module_image"></div>
                        <?php endif;?>
                    <?php endforeach;?><?php foreach($modulesStore->result() as $module):?>
                        <?php if($module->column_id == 1):?>
                            <div module_id=<?= $module_counter++?> class="module module_store has_placeholder" style="top: <?= $module->top?>px;"><div class="module_store_text"><?= $module->content?></div><div class="module_store_button store_add" magento_id="<?= $module->magento_id?>">Add to cart</div><div class="module_store_button store_checkout">Checkout</div></div>
                        <?php endif;?>
                    <?php endforeach;?></div>
                </div>
                
                <div id="gallery" class="has_placeholder" data-text="Add images here"><?php foreach($galleryItems->result() as $galleryItem):?>
                    <div class="gallery_item" filename="<?= $galleryItem->fname?>">
                        <img src="<?= site_url('items/uploads/gallery/' . $galleryItem->fname)?>" />
                        <div><?= nl2br($galleryItem->credits)?></div>
                    </div>
                <?php endforeach;?></div>
                
                <div id="item_rel">
                    <div id="item_rel_header">RELATED</div>
                    
                    <div id="item_rel_tags" class="has_placeholder" data-text="Add related tags here"><?php foreach($relatedMetatags as $relatedMetatag):?>
                        <span metatag_id="<?= $relatedMetatag['metatag_id']?>" url="<?= $relatedMetatag['url']?>"><?= $relatedMetatag['text']?></span>
                    <?php endforeach;?></div>
                    
                    <div id="item_rel_items" class="has_placeholder" data-text="Add related items here"><?php foreach($relatedItems->result() as $relatedItem):?>
                        <div class="item_rel_item" item_id="<?= $relatedItem->id?>">
                            <img src="<?=site_url('items/uploads/detailimg/' . $relatedItem->detail_img)?>" filename="<?= $relatedItem->detail_img?>"/>
                            <div class="item_rel_item_name unselectable"><?= $relatedItem->name?></div>
                        </div>
                    <?php endforeach;?></div>
                </div>
            </div>
            
            
            <div id="module_html_container">
            <?php foreach($modulesHTML->result() as $module):?>
     	        <textarea id="module_html_container_<?= $module->id?>"><?= $module->html?></textarea>
        	<?php endforeach;?>
        	</div>
            
            <div id="control_container">
                <div id="module_container">
                    <ul>
                        <li class="unselectable" type="text">Text module</li>
                        <li class="unselectable" type="image">Image module</li>
                        <li class="unselectable" type="2col_image">2 column image module</li>
                        <li class="unselectable" type="bulletpoint">Bulletpoint module</li>
                        <li class="unselectable" type="video">Video module</li>
                        <li class="unselectable" type="html">HTML embed module</li>
                        <li class="unselectable" type="headline">Headline module</li>
                        <li class="unselectable" type="download">Download module</li>
                        <li class="unselectable" type="store">Store module</li>
                    </ul>
                </div>
                
                <div id="button_container">
                    <ul>
                        <li class="unselectable item_save">Save</li>
                        <li class="unselectable item_cancel">Cancel</li>
                    </ul>
                </div>
            </div>
            

            
            
            <div id="popup_edit_text" class="popup_edit">
                <div class="header">Edit text</div>
                <div class="popup_edit_container">
                    <textarea></textarea>
                </div>
                <div class="popup_edit_controls">
                    <div class="popup_edit_button popup_save_button">Save</div>
                    <div class="popup_edit_button popup_cancel_button">Cancel</div>
                </div>
            </div>
            
            <div id="popup_edit_html" class="popup_edit">
                <div class="header">Edit html desktop</div>
                <div class="popup_edit_container desktop">
                    <textarea></textarea>
                </div>
                <div class="header">Edit html mobile</div>
                <div class="popup_edit_container mobile">
                    <textarea></textarea>
                </div>
                
                <div class="popup_edit_controls">
                    <div class="popup_edit_button popup_save_button">Save</div>
                    <div class="popup_edit_button popup_cancel_button">Cancel</div>
                </div>
            </div>
            
            <div id="popup_module_text" class="popup_edit">
                <div class="header">Edit text module</div>
                <div class="popup_edit_container">
                    <textarea id="module_text_editor"></textarea>
                </div>
                <div class="popup_edit_controls">
                    <div class="popup_edit_button popup_switch_button">Switch column</div>
                    <div class="popup_edit_button popup_delete_button">Delete</div>
                    <div class="popup_edit_button popup_save_button">Save</div>
                    <div class="popup_edit_button popup_cancel_button">Cancel</div>
                </div>
            </div>
            
            <div id="popup_module_image" class="popup_edit">
                <div class="header">Edit image module</div>
                <div class="popup_edit_container">
                    <img src="" />
                </div>
                <div class="popup_edit_controls">
                    <div class="popup_edit_button popup_switch_button">Switch column</div>
                    <div class="popup_edit_button popup_delete_button">Delete</div>
                    <div class="popup_edit_button popup_cancel_button">Close</div>
                </div>
            </div>
            
            <div id="popup_module_bulletpoint" class="popup_edit">
                <div class="header">Edit bulletpoint module</div>
                <div class="popup_edit_container">
                    <table>
                        <tr>
                            <td style="padding-right: 5px; padding-bottom: 5px;">Header</td>
                            <td><input type="text" value="" />
                        </tr>
                        <tr>
                            <td style="vertical-align: top;">Text</td>
                            <td><textarea id="module_bulletpoint_editor"></textarea></td>
                        </tr>
                    </table>
                </div>
                <div class="popup_edit_controls">
                    <div class="popup_edit_button popup_switch_button">Switch column</div>
                    <div class="popup_edit_button popup_delete_button">Delete</div>
                    <div class="popup_edit_button popup_save_button">Save</div>
                    <div class="popup_edit_button popup_cancel_button">Cancel</div>
                </div>
            </div>
            
            <div id="popup_module_download" class="popup_edit">
                <div class="header">Edit file</div>
                <div class="popup_edit_container">
                    <div id="file_upload_button">Upload</div>
                    <input id="file_upload_input" type="file" accept="" uploadpath="items/uploads/module_download"/>
                    <br/>
                    <img class="progress" src="<?= site_url('items/backend/img/ajax-loader.gif')?>" style="display: none;"/>
                    <br/>
                    <label for="file_display_name">Text: </label>
                    <input type="text" id="file_display_name" name="file_display_name" value="" ></input>
                               
                </div>
                <div class="popup_edit_controls">
                	<div class="popup_edit_button popup_delete_button">Delete</div>
                    <div class="popup_edit_button popup_save_button">Save</div>
                    <div class="popup_edit_button popup_cancel_button">Cancel</div>
                </div>
            </div>
            
            <div id="popup_related_tags" class="popup_edit">
                <div class="header">Edit related tags</div>
                <div class="popup_edit_container">
                    <div id="related_tags_external">
                        <div id="external_tags_text">
                            <div>Text:</div>
                            <input type="text" id="external_tag_text" />
                        </div>
                        <div id="external_tags_url">
                            <div>Url:</div>
                            <input type="text" id="external_tag_url" />
                        </div>
                        <div id="external_tag_add" class="popup_edit_button">Add</div>
                        
                    </div>
                    <select id="related_tags_category">
                        <option value="0">Select category ...</options>
                        <?php foreach($metatagCategories->result() as $category):?>
                            <option value="<?= $category->id?>"><?=$category->name?></option>
                        <?php endforeach;?>
                    </select>
                    <?php foreach($metatagCategories->result() as $category):?>
                        <div class="related_tags_metatags" category_id="<?= $category->id?>">
                            <?php foreach($metatags[$category->id]->result() as $tag):?>
                                <span metatag_id=<?= $tag->id?>><?= $tag->name?></span>
                            <?php endforeach;?>
                        </div>
                    <?php endforeach;?>
                    <div id="related_tags_selected"></div>
                </div>
                <div class="popup_edit_controls">
                    <div class="popup_edit_button popup_save_button">Save</div>
                    <div class="popup_edit_button popup_cancel_button">Cancel</div>
                </div>
            </div>
            
            <div id="popup_related_items" class="popup_edit">
                <div class="header">Edit related items</div>
                <div class="popup_edit_container">
                    <table style="width: 100%;">
                        <tr>
                            <td style="width: 100px;font-size: 13px;">Metatag category</td>
                            <td>
                                <select id="related_items_category">
                                    <option value="0">Select category ...</options>
                                    <?php foreach($metatagCategories->result() as $category):?>
                                        <option value="<?= $category->id?>"><?=$category->name?></option>
                                    <?php endforeach;?>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 100px;font-size: 13px;">Available metatags</td>
                            <td>
                                <?php foreach($metatagCategories->result() as $category):?>
                                    <div class="related_items_metatags" category_id="<?= $category->id?>">
                                        <?php foreach($metatags[$category->id]->result() as $tag):?>
                                            <span metatag_id=<?= $tag->id?>><?= $tag->name?></span>
                                        <?php endforeach;?>
                                    </div>
                                <?php endforeach;?>
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 100px;font-size: 13px;">Selected metatags</td>
                            <td><div id="related_items_tags_selected"></div></td>
                        </tr>
                        <tr>
                            <td style="width: 100px;font-size: 13px;">Available items</td>
                            <td><div id="related_items_itemlist"></div></td>
                        </tr>
                        <tr>
                            <td style="width: 100px;font-size: 13px;">Selected items</td>
                            <td><div id="related_items_selected"></div></td>
                        </tr>
                    </table>
                </div>
                <div class="popup_edit_controls">
                    <div class="popup_edit_button popup_save_button">Save</div>
                    <div class="popup_edit_button popup_cancel_button">Cancel</div>
                </div>
            </div>
            
            
            <div id="popup_gallery" class="popup_edit">
                <div class="header">Edit gallery</div>
                <div class="popup_edit_container">
                    <div id="gallery_upload_button">Upload</div>
                    <input id="gallery_upload_input" type="file" accept=".png,.jpg,.jpeg"/>
                    
                    <div id="gallery_items"></div>
                </div>
                <div class="popup_edit_controls">
                    <div class="popup_edit_button popup_save_button">Save</div>
                    <div class="popup_edit_button popup_cancel_button">Cancel</div>
                </div>
            </div>
            
            
            <div id="popup_module_video" class="popup_edit">
                <div class="header">Edit video</div>
                <div class="popup_edit_container">
                    <table style="width: 100%;">
                        <tr>
                            <td style="width: 100px;font-size: 13px;">Video code</td>
                            <td>
                                <input type="text" id="video_code_input" />
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 100px;font-size: 13px;">Starting position<br/>(in seconds)</td>
                            <td>
                                <input type="text" id="video_start_input" />
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="popup_edit_controls">
                    <div class="popup_edit_button popup_switch_button">Switch column</div>
                    <div class="popup_edit_button popup_delete_button">Delete</div>
                    <div class="popup_edit_button popup_save_button">Save</div>
                    <div class="popup_edit_button popup_cancel_button">Cancel</div>
                </div>
            </div>
            
            
            <div id="popup_module_html" class="popup_edit">
                <div class="header">Edit html embed</div>
                <div class="popup_edit_container">
                    <textarea style="height: 500px;"></textarea>
                </div>
                <div class="popup_edit_controls">
                    <div class="popup_edit_button popup_switch_button">Switch column</div>
                    <div class="popup_edit_button popup_delete_button">Delete</div>
                    <div class="popup_edit_button popup_save_button">Save</div>
                    <div class="popup_edit_button popup_cancel_button">Cancel</div>
                </div>
            </div>
            
            <div id="popup_module_store" class="popup_edit">
                <div class="header">Edit store module</div>
                <div class="popup_edit_container">
                    <table style="width: 100%;">
                        <tr>
                            <td style="width: 100px;font-size: 13px; vertical-align: top;">Store item<br/>description:</td>
                            <td>
                                <textarea id="module_store_editor"></textarea>
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 100px;font-size: 13px; vertical-align: top;">Store item:</td>
                            <td>
                                <select id="module_store_product">
                                    <?php foreach($store_products->result() as $sp):?>
                                        <option value="<?= $sp->id?>"><?= $sp->name?></option>
                                    <?php endforeach;?>
                                </select>
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="popup_edit_controls">
                    <div class="popup_edit_button popup_switch_button">Switch column</div>
                    <div class="popup_edit_button popup_delete_button">Delete</div>
                    <div class="popup_edit_button popup_save_button">Save</div>
                    <div class="popup_edit_button popup_cancel_button">Cancel</div>
                </div>
            </div>
            
            
            <div id="detail_img_dialog" title="What do you want to do?">
                <p>Do you want to upload a new image or do you want to delete the current one?
            </div>