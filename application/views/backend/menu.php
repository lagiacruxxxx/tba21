
<div id="messagecontainer"></div>

<div id="menu">
	<ul>
		<li><a href="<?= site_url('authentication/logout')?>">Logout</a></li>
		<li><a href="<?= site_url('authentication/usersettings')?>"><img src="<?= site_url('items/backend/img/settings.png')?>" /></a></li>
		<li>Logged in as <b><?= $username?></b></li>
	</ul>
</div>

<div id="sidebar">
    <div class="sidebar_logo">
        <img src="<?= site_url('items/backend/img/tba_logo_white.png')?>" />
    </div>

	<ul>
		<li class="sidebar_headline">METATAG</li>
		<li class="sidebar_menuitem">
            <a href="<?= site_url('entities/Metatag/metatag_category')?>">Tag categories</a>
        </li>
		<li class="sidebar_menuitem">
            <a href="<?= site_url('entities/Metatag/metatag')?>">Tags</a>
        </li>
        
		<li><div class="separator"></div></li>
		
		<li class="sidebar_headline">MENU TAGS</li>
		<li class="sidebar_menuitem">
            <a href="<?= site_url('entities/Menu/tags/' . CONTINENT_COLLECTION)?>">Collection Tags</a>
        </li>
        <li class="sidebar_menuitem">
            <a href="<?= site_url('entities/Menu/tags/' . CONTINENT_PRODUCTION)?>">Media Tags</a>
        </li>
        <li class="sidebar_menuitem">
            <a href="<?= site_url('entities/Menu/tags/' . CONTINENT_PROGRAM)?>">Program Tags</a>
        </li>
        <li class="sidebar_menuitem">
            <a href="<?= site_url('entities/Menu/tags/' . CONTINENT_ACADEMY)?>">Ocean Tags</a>
        </li>
        
        <li><div class="separator"></div></li>
		
		<li class="sidebar_headline">ITEMS</li>
		<li class="sidebar_menuitem">
            <a href="<?= site_url('entities/Item/items/')?>">Items</a>
        </li>
        
        <li><div class="separator"></div></li>
		
		<li class="sidebar_headline">PRESS</li>
        <li class="sidebar_menuitem">
            <a href="<?= site_url('entities/Press/pressusers')?>">Press registrations</a>
        </li>
        <li class="sidebar_menuitem">
            <a href="<?= site_url('entities/Press/presskits')?>">Presskits</a>
        </li>
        
        
        <li><div class="separator"></div></li>
		
		<li class="sidebar_headline">IMPORT</li>
		<li class="sidebar_menuitem">
            <a href="<?= site_url('entities/Import/import_item/')?>">Import artwork</a>
        </li>
        
        <li><div class="separator"></div></li>
        
        <li class="sidebar_headline">WEBSHOP</li>
        <li class="sidebar_menuitem">
            <a href="<?= site_url('entities/Shop/orders')?>">Orders</a>
        </li>
        <li class="sidebar_menuitem">
            <a href="<?= site_url('entities/Shop/items')?>">Items</a>
        </li>
        <li class="sidebar_menuitem">
            <a href="<?= site_url('entities/Shop/settings/edit/' . $this->config->item('shopSettingsId'))?>">Settings</a>
        </li>
        <li class="sidebar_menuitem">
            <a href="<?= site_url('entities/Shop/shipment/' . SHIPMENT_TYPE_STANDARD)?>">STANDARD shipment</a>
        </li>
        <li class="sidebar_menuitem">
            <a href="<?= site_url('entities/Shop/shipment/' . SHIPMENT_TYPE_EXPRESS)?>">EXPRESS shipment</a>
        </li>
        
        <li><div class="separator"></div></li>
		
		<li class="sidebar_headline">SETTINGS</li>
		<li class="sidebar_menuitem">
            <a href="<?= site_url('entities/Settings/settings/edit/' . $this->config->item('settingsId'))?>">Settings</a>
        </li>
        <li class="sidebar_menuitem">
            <a href="<?= site_url('entities/Settings/splash')?>">Splash pages</a>
        </li>
        <li class="sidebar_menuitem">
            <a href="<?= site_url('entities/Settings/newsletter')?>">Newsletter registrations</a>
        </li>      
        
          
        
	</ul>
</div>

