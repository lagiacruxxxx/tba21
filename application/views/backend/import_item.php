
    <div id="content">
        <!-- CSS -->
    	<link rel="stylesheet" type="text/css" href="<?=site_url("items/backend/css/import.css"); ?>">
    
    	<!-- JS -->
    	<script type="text/javascript" src="<?=site_url("items/backend/js/import.js"); ?>"></script>
        	
    
        <div class="content_h1">Import artwork</div>
        
        <div id="artwork_search">
            <div class="content_h2">Search artwork database</div>
            <input type="text" id="artwork_search_input" />
            <select id="artwork_search_by">
                <option value="0">by Artwork</option>
                <option value="1">by Artist</option>
                </select>
            <div id="artwork_search_button">Search</div>
        </div>
        <div id="artwork_search_results">
            <table>
                <thead>
                    <th></th>
                    <th>Artwork ID</th>
                    <th>Name</th>
                    <th>Artist</th>
                    <th>Production date</th>
                    <th>Already imported?</th>
                    <th>Links</th>
                </thead>
                <tbody>
                    
                </tbody>
            </table>
        </div>
        <div id="artwork_search_control">
            <div id="artwork_import" class="artwork_search_control_button">Import selected</div>
        </div>
    </div>    
    