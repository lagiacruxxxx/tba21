

        <div id="content_headline"><?= $this->lang->line('checkout_headline')?></div>
        
        <div id="confirm">
            <?php if(!$expired):?>
            <div id="confirm_userdata">
                <table id="table_confirm">
                    <tr>
                        <td colspan=4><div class="checkout_header"><span><?= $this->lang->line('shop_personal_data')?></span></div></td>
                    </tr>
                    <tr>
                        <td colspan=2><?= $this->lang->line('shop_firstname')?>:</td>
                        <td colspan=2><?= htmlspecialchars($order->firstname)?></td>
                    </tr>
                    <tr>
                        <td colspan=2><?= $this->lang->line('shop_lastname')?>:</td>
                        <td colspan=2><?= htmlspecialchars($order->lastname)?></td>
                    </tr>
                    <tr>
                        <td colspan=2><?= $this->lang->line('shop_email')?>:</td>
                        <td colspan=2><?= htmlspecialchars($order->email)?></td>
                    </tr>
                    <tr>
                        <td colspan=2><?= $this->lang->line('shop_phone')?>:</td>
                        <td colspan=2><?= htmlspecialchars($order->phone)?></td>
                    </tr>
                    <tr>
                        <td colspan=2><?= $this->lang->line('shop_street')?>:</td>
                        <td colspan=2><?= htmlspecialchars($order->street)?></td>
                    </tr>
                    <tr>
                        <td colspan=2><?= $this->lang->line('shop_zipcode')?>:</td>
                        <td colspan=2><?= htmlspecialchars($order->zip)?></td>
                    </tr>
                    <tr>
                        <td colspan=2><?= $this->lang->line('shop_city')?>:</td>
                        <td colspan=2><?= htmlspecialchars($order->city)?></td>
                    </tr>
                    <tr>
                        <td colspan=2><?= $this->lang->line('shop_country')?>:</td>
                        <td colspan=2><?= htmlspecialchars($country)?></td>
                    </tr>
                    
                    
                    <tr>
                        <td colspan=4><div class="checkout_header"><span><?= $this->lang->line('shop_shipment_header')?></span></div></td>
                    </tr>
                    
                    <tr>
                        <td colspan=2><?= $this->lang->line('shop_shipment_method')?>:</td>
                        <td colspan=2><?= htmlspecialchars($shipment)?></td>
                    </tr>
                    
                    <tr>
                        <td colspan=4><div class="checkout_header"><span><?= $this->lang->line('shop_payment_data')?></span></div></td>
                    </tr>
                    <?php if($order->paymentType == 'CreditCard'):?>
                    <tr>
                        <td colspan=2><?= $this->lang->line('shop_cc_owner')?>:</td>
                        <td colspan=2><?= htmlspecialchars($order->cc_cardholder)?></td>
                    </tr>
                    <tr>
                        <td colspan=2><?= $this->lang->line('shop_cc_number')?>:</td>
                        <td colspan=2><?= htmlspecialchars($order->cc_pan)?></td>
                    </tr>
                    <tr>
                        <td colspan=2><?= $this->lang->line('shop_cc_exp_date')?>:</td>
                        <td colspan=2><?= htmlspecialchars($order->cc_exp_date)?></td>
                    </tr>
                    <?php endif;?>
                    
                    <tr>
                        <td colspan=2><div class="checkout_header"><span><?= $this->lang->line('shop_summary')?></span></div></td>
                    </tr>
                    
                    <?php foreach($items->result() as $item):?>
                    <tr>
                        <td><?= htmlspecialchars($item->name)?></td>
                        <td><?= htmlspecialchars($item->amount)?></td>
                        <td class="number_align"><?= htmlspecialchars(number_format((float)$item->price, 2, '.', '')) . ' €'?></td>
                        <td class="number_align"><?= htmlspecialchars(number_format((float)$item->price * $item->amount, 2, '.', '') . ' €')?></td>
                    </tr>
                    <?php endforeach;?>
                    <tr>
                        <td class="confirm_item_total"></td>
                        <td class="confirm_item_total"></td>
                        <td class="confirm_item_total"></td>
                        <td class="confirm_item_total number_align"><?= $item_total?></td>
                    </tr>
                    <tr>
                        <td class=""></td>
                        <td class=""></td>
                        <td class=""><?= $this->lang->line('shipment_costs')?></td>
                        <td class="confirm_shipment number_align"><?= $delivery?></td>
                    </tr>
                    
                    <tr>
                        <td class=""></td>
                        <td class=""></td>
                        <td class=""><?= $this->lang->line('total')?></td>
                        <td class="confirm_total number_align"><?= $total?></td>
                    </tr>
                                     
                </table>
                
                <div id="confirm_order"><a href="<?= site_url('Shop/startPayment')?>" target="_blank"><span><?= $this->lang->line('finish_checkout')?></span></a></div>
                
                <div id="checkout_errors"></div>
            </div>
            <?php else:?>
            	<div class="shop_session_problem"><?= $this->lang->line('shop_session_problem')?></div>
            <?php endif;?>
        </div>     
