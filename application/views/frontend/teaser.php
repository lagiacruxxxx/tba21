

                
                 
            <div class="teaser rotated noselect"
                continent_id = <?= $continentId?>
            >
                <?php for($i = 0 ; $i <= $item['quadCount'] ; $i++):?>
                <div 
                    item_id=<?= $item['itemId'][$i]?> 
                    class="item item_quad hidden noselect <?php if($item['quadId'][$i] == 1):?>rotated<?php endif;?>" 
                    status="inactive" 
                    continent_id = <?= $item['continentId'][$i] ?> 
                    style="left: <?= $item['quadLeft'][$i]?>px; top: <?= $item['quadTop'][$i]?>px;"   
                    metatags="<?php foreach($item['metatags'][$i]->result() as $tag):?><?= $tag->metatag_id?>,<?php endforeach;?>" 
                    tt_header_<?= LANG_EN?>="<?= $item['name_en'][$i]?>"
                    tt_header_<?= LANG_DE?>="<?= $item['name_de'][$i]?>"
                    quad_id="<?= $item['quadId'][$i]?>"
                >
                   <div class="id_helper"><?= $item['itemId'][$i]?></div>
                   <div class="item_overlay original noselect quad_overlay" style="background-image: url(<?= site_url('items/uploads/items/' . ($item['quadId'][$i] == 1 ? $item['mirror_img'][$i] : $item['img'][$i])) ?>)"></div>
                   <div type=<?= $item['imgType'][$i]?> class="item_overlay selected selected<?= $item['imgType'][$i]?> noselect quad_overlay"></div>
                   <div type=<?= $item['imgType'][$i]?> class="item_overlay inactive noselect quad_overlay"></div>
                   <div class="item_overlay blur noselect quad_overlay" style="background-image: url(<?= site_url('items/uploads/items/' . ($item['quadId'][$i] == 1 ? $item['mirror_img_blurred'][$i] : $item['img_blurred'][$i]))?>)"></div>
                </div>    
            <?php endfor;?>

            </div>        