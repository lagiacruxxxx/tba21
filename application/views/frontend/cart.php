

        <div id="content_headline"><?= $this->lang->line('cart_headline')?></div>
        
        <div id="cart">
            <?php foreach($cartitems as $item):?>
                <div class="cart_item" rowid="<?= $item['rowid']?>">
                    <div class="cart_item_img"><img src="<?= $item['options']['image']?>" /></div>
                    <div class="cart_item_text">
                        <div class="cart_item_name"><span><?= $item['name']?></span></div>
                        <div class="cart_item_desc"><?= htmlspecialchars($item['options']['desc'])?></div>
                        <div class="cart_item_remove noselect"><?= $this->lang->line('cart_item_remove')?></div>
                    </div>
                    <div class="cart_item_qty">
                        <div class="cart_item_qty_btn less noselect"><?= $this->lang->line('cart_item_less')?></div>
                        <div class="cart_item_qty_qty"><?= $item['qty']?></div>
                        <div class="cart_item_qty_btn more noselect"><?= $this->lang->line('cart_item_more')?></div>
                    </div>
                    <div class="cart_item_price"><?= number_format((float)$item['qty'] * $item['price'], 2, '.', '') ?></div>
                </div>
            <?php endforeach;?>
            <div id="cart_empty" style="<?php if(count($cartitems) <= 0):?>display: block;<?php endif;?>"><?= $this->lang->line('cart_empty')?></div>
            <div id="cart_total" style="<?php if(count($cartitems) <= 0):?>display: none;<?php endif;?>"><?= number_format((float)$total, 2, '.', '')?></div>
            <div id="cart_checkout" style="<?php if(count($cartitems) <= 0):?>display: none;<?php endif;?>"><span><?= $this->lang->line('cart_checkout')?></span></div>
        </div>     