                
        <?php if($offset == null):?>        
            <div id="content_headline"><?= $headline?></div>
            <div id="content_columns" offset=<?php if($offset != null) echo $offset; else echo 0;?>>
        <?php endif;?>
        
        <?php if($group_by_continents):?>
            <?php $j = 0; for($i = 0 ; $i < count($listitems) ; $i++):?>
                <?php if(isset($listitems[$i]['type']) && $listitems[$i]['type'] == 'continent_headline'):?>
                    <?php $j = 0;?>
                    <div class="list_continent_separator"><?= $listitems[$i]['continentName']?></div>
                <?php else:?>
                    <div class="list_item" item_id=<?= $listitems[$i]['id']?> <?php if($j % 2 == 0):?>style="margin-right: 0px; width: 438px;"<?php endif;?>>
                        <div class="list_item_imgcontainer">
                            <img class="list_item_img" src="<?= site_url('items/uploads/detailimg/' . $listitems[$i]['detail_img'])?>" />
                        </div>
                        <div class="list_item_name"><span><?= $listitems[$i]['name']?></span></div>
                        <div class="list_item_text module_text"><?= $text[$listitems[$i]['id']]?></div>
                    </div>
                <?php endif;?>
            <?php  $j++; endfor;?>
            
        
        <?php else:?>
            <?php $i = 0; foreach($listitems as $item):?>
                <div class="list_item" item_id=<?= $item['id']?> <?php if($i % 2 == 1):?>style="margin-right: 0px; width: 438px;"<?php endif;?>>
                    <div class="list_item_imgcontainer">
                        <img class="list_item_img" src="<?= site_url('items/uploads/detailimg/' . $item['detail_img'])?>" />
                    </div>
                    <div class="list_item_name">
                        <?php if($artist[$item['id']] != ''):?><span><?= $artist[$item['id']]?></span><?php endif;?>
                        <br/>
                        <span><?= $item['name']?></span>
                    </div>
                    <div class="list_item_text module_text"><?= $text[$item['id']]?></div>
                </div>
            <?php $i++; endforeach;?>
        <?php endif;?>
        
        <?php if($offset == null):?>        
            </div>   
        <?php endif;?>
            
                
                
                
                
                
                
        