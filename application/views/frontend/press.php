

        <div id="content_headline"><?= $this->lang->line('press_header')?></div>
        
        <div id="content_columns">
            <div id="content_column_left" class="content_column">
                <div class="press_headline"><span><?= $this->lang->line('press_welcome')?></span></div>
                <div class="press_text"><?= $this->lang->line('press_infoheader')?></div>
                <br/>
                <div class="press_headline"><span><?= $this->lang->line('press_login_header')?></span></div>
                <div class="press_text"><?= $this->lang->line('press_login_info')?></div>
                <div class="press_login">
                    <table>
                        <tr>
                            <td><?= $this->lang->line('press_login_email')?></td>
                            <td><input id="press_email" type="text" name="press_email"></td>
                        </tr>
                        <tr>
                            <td></td>
                            <td><div id="press_login_button"><?= $this->lang->line('press_login_button')?></div></td>
                        </tr>
                        <tr>
                            <td colspan=2><div id="press_login_error"></div></td>
                        </tr>
                    </table>
                </div>
                <br/>
                <div class="press_headline"><span><?= $this->lang->line('press_reg_header')?></span></div> 
                <div class="press_text"><?= $this->lang->line('press_reg_info')?></div>      
                <div class="press_reg">
                    <table>
                        <tr>
                            <td><?= $this->lang->line('press_reg_firstname')?></td>
                            <td><input id="press_reg_firstname" type="text" name="press_reg_firstname"></td>
                        </tr>
                        <tr>
                            <td><?= $this->lang->line('press_reg_lastname')?></td>
                            <td><input id="press_reg_lastname" type="text" name="press_reg_lastname"></td>
                        </tr>
                        <tr>
                            <td><?= $this->lang->line('press_reg_publication')?></td>
                            <td><input id="press_reg_publication" type="text" name="press_reg_publication"></td>
                        </tr>
                        <tr>
                            <td><?= $this->lang->line('press_reg_email')?></td>
                            <td><input id="press_reg_email" type="text" name="press_reg_email"></td>
                        </tr>
                        <tr>
                            <td></td>
                            <td><div id="press_reg_button"><?= $this->lang->line('press_reg_button')?></div></td>
                        </tr>
                        <tr>
                            <td colspan=2><div id="press_reg_error"></div></td>
                        </tr>
                    </table>
                </div>
            </div>
            
            <div id="content_column_right" class="content_column">
                         
            </div>
        </div>                        
        