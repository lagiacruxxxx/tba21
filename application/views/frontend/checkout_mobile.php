

        <div id="content_headline"><?= $this->lang->line('checkout_headline')?></div>
        
        <div id="checkout">
            <div id="checkout_userdata">
                <table id="table_userdata">
                    <tr>
                        <td colspan=2 class="checkout_spacer"></td>
                    </tr>
                    <tr>
                        <td colspan=2><div class="checkout_header"><span><?= $this->lang->line('shop_personal_data')?></span></div></td>
                    </tr>
                    <tr>
                        <td><?= $this->lang->line('shop_firstname')?>:</td>
                        <td><input type="text" id="checkout_firstname" value=""></td>
                    </tr>
                    <tr>    
                        <td><?= $this->lang->line('shop_lastname')?>:</td>
                        <td><input type="text" id="checkout_lastname" value=""></td>
                    </tr>
                    <tr>
                        <td><?= $this->lang->line('shop_email')?>:</td>
                        <td><input type="text" id="checkout_email" value=""></td>
                    </tr>
                    <tr>    
                        <td><?= $this->lang->line('shop_phone')?>:</td>
                        <td><input type="text" id="checkout_phone" value=""></td>
                    </tr>
                    <tr>
                        <td><?= $this->lang->line('shop_street')?>:</td>
                        <td><input type="text" id="checkout_street" value=""></td>
                    </tr>
                    <tr>    
                        <td><?= $this->lang->line('shop_zipcode')?>:</td>
                        <td><input type="text" id="checkout_zipcode" value=""></td>
                    </tr>
                    <tr>
                        <td><?= $this->lang->line('shop_city')?>:</td>
                        <td><input type="text" id="checkout_city" value=""></td>
                    </tr>
                    <tr>    
                        <td><?= $this->lang->line('shop_country')?>:</td>
                        <td>
                            <select id="checkout_country">
                                <?php foreach($countries->result() as $c):?>
                                    <option value="<?= $c->id?>" <?php if($c->id=='AT'):?>SELECTED<?php endif;?> location="<?= $c->shipping?>"><?php if($language == LANG_DE) echo $c->name_de; else echo $c->name_en;?></option>
                                <?php endforeach;?>
                            </select>
                        </td>
                    </tr>
                    
                    
                    <tr>
                        <td colspan=4><div class="checkout_header"><span><?= $this->lang->line('shop_shipment_header')?></span></div></td>
                    </tr>
                    
                    <tr>
                        <td><?= $this->lang->line('shop_shipment_method')?>:</td>
                        <td>
                            <select class="checkout_shipment" active="1" location='austria' style="display: block;">
                                <?php foreach($shipment['austria'] as $key => $s):?>
                                    <option value="<?= $key?>" <?php if($key==SHIPMENT_TYPE_STANDARD):?>SELECTED<?php endif;?>><?= $s['name'] . ' - ' . $s['price']?></option>
                                <?php endforeach;?>
                            </select>
                            <select class="checkout_shipment" active=0 location='europe'>
                                <?php foreach($shipment['europe'] as $key => $s):?>
                                    <option value="<?= $key?>" <?php if($key==SHIPMENT_TYPE_STANDARD):?>SELECTED<?php endif;?>><?= $s['name'] . ' - ' . $s['price']?></option>
                                <?php endforeach;?>
                            </select>
                            <select class="checkout_shipment" active=0 location='worldwide'>
                                <?php foreach($shipment['worldwide'] as $key => $s):?>
                                    <option value="<?= $key?>" <?php if($key==SHIPMENT_TYPE_STANDARD):?>SELECTED<?php endif;?>><?= $s['name'] . ' - ' . $s['price']?></option>
                                <?php endforeach;?>
                            </select>
                        </td>
                        <td></td>
                        <td></td>
                    </tr>
                    
                    
                    <tr>
                        <td colspan=4><div class="checkout_header"><span><?= $this->lang->line('shop_payment_data')?></span></div></td>
                    </tr>
                
                    <tr>
                        <td><?= $this->lang->line('shop_paymentmethod')?>:</td>
                        <td>
                            <select id="payment_type">
                                <option value="CreditCard" SELECTED><?= $this->lang->line('shop_creditcard')?></option>
                            </select>
                        </td>
                    </tr>
                    
                    <tr>
                        <td colspan=4><div class="checkout_sparator"></div></td>
                    </tr>
                    
                    <tr class="pament_method CreditCard">
                        <td><?= $this->lang->line('shop_cc_owner')?>:</td>
                        <td><input type="text" id="checkout_cc_owner" value=""></td>
                    </tr>
                    
                    <tr class="pament_method CreditCard">
                        <td><?= $this->lang->line('shop_cc_number')?>:</td>
                        <td><input type="text" id="checkout_cc_number" value=""></td>
                    </tr>
                    <tr class="pament_method CreditCard">
                        <td><?= $this->lang->line('shop_cc_exp_date')?>:</td>
                        <td colspan=3>
                            <select id="checkout_cc_exp_month" class="shop_cc_exp">
                                <option value="1" SELECTED>01</option>
                                <option value="2">02</option>
                                <option value="3">03</option>
                                <option value="4">04</option>
                                <option value="5">05</option>
                                <option value="6">06</option>
                                <option value="7">07</option>
                                <option value="8">08</option>
                                <option value="9">09</option>
                                <option value="10">10</option>
                                <option value="11">11</option>
                                <option value="12">12</option>
                            </select>
                            /
                            <select id="checkout_cc_exp_year" class="shop_cc_exp">
                                <?php for($i = 0 ; $i < 20 ; $i++):?>
                                    <option value="<?= intval(date('Y')) + $i?>" ><?= intval(date('Y')) + $i?></option>
                                <?php endfor;?>
                            </select>
                        </td>
                    </tr>
                    <tr class="pament_method ccard">
                        <td><?= $this->lang->line('shop_cc_code')?>:</td>
                        <td><input type="text" id="checkout_cc_code" value="" style="width: 50px;"></td>
                    </tr> 
                    
                    <tr>    
                        <td colspan=2><div id="cc_error" class="shop_validation_error"></div></td>
                    </tr>  
                    
                    <tr>
                        <td colspan=4><div class="checkout_sparator"></div></td>
                    </tr>
                    
                    
                    
                    <tr>
                        <td colspan=4><div class="checkout_header"><span><?= $this->lang->line('shop_tos_header')?></span></div></td>
                    </tr>
                    <tr>
                        <?php if($language == LANG_EN):?>
                            <td><?= $this->lang->line('shop_terms_accept')?> <a href="<?= site_url('Shop/terms')?>" target="_blank"><?= $this->lang->line('shop_terms')?></a></td>
                        <?php else:?>
                            <td><a href="<?= site_url('Shop/terms')?>" target="_blank"><?= $this->lang->line('shop_terms')?></a> <?= $this->lang->line('shop_terms_accept')?></td>
                        <?php endif;?>
                        <td><input type="checkbox" id="checkout_terms_accept"></td>
                    </tr>
                    
                                     
                </table>
                
                <div id="finish_checkout"><span><?= $this->lang->line('checkout_continue')?></span></div>
                
                <div id="checkout_errors"></div>
            </div>
            
        </div>     
        
        <script>var cc_error_text = "<?= $this->lang->line('shop_cc_error')?>";</script>
        <script src="<?php echo $storageInfo['javascriptUrl']; ?>" type="text/javascript"></script>