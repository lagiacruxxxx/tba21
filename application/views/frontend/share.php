<html>
    
    <head>
        <meta property="og:title" content="TBA21.org"/>
    	<meta property="og:type" content="website"/>
    	<meta property="og:url" content="<?= 'http://www.tba21.org/' . ($item->prettyurl != '' ? $item->prettyurl : $item->id)?>"/>
    	<meta property="og:image" content="<?= site_url('items/uploads/detailimg/' . $item->detail_img)?>"/>
    	<meta property="og:description" content="<?= str_replace('<br>', '', $item->name)?>" />
    	
        <script>
          window.fbAsyncInit = function() {
            FB.init({
              appId      : '440229979512060',
              xfbml      : true,
              version    : 'v2.5'
            });
          };
        
          (function(d, s, id){
             var js, fjs = d.getElementsByTagName(s)[0];
             if (d.getElementById(id)) {return;}
             js = d.createElement(s); js.id = id;
             js.src = "//connect.facebook.net/en_US/sdk.js";
             fjs.parentNode.insertBefore(js, fjs);
           }(document, 'script', 'facebook-jssdk'));
        </script>
    </head>
    
    <body>
    
    </body>

    
</html>