<html>
    <head>
    	<?php if($is_mobile):?>
    		<meta name="viewport" content="width=320, minimum-scale=0.5, maximum-scale=2.0, user-scalable=no">
        <?php elseif($is_ipad):?>
        	<meta name="viewport" content="width=1280, minimum-scale=0.5, maximum-scale=2.0, user-scalable=no">
        <?php endif;?>
        
        <link rel="apple-touch-icon" sizes="57x57" href="<?= site_url('items/general/img/favicons/apple-icon-57x57.png')?>">
        <link rel="apple-touch-icon" sizes="60x60" href="<?= site_url('items/general/img/favicons/apple-icon-60x60.png')?>/apple-icon-60x60.png">
        <link rel="apple-touch-icon" sizes="72x72" href="<?= site_url('items/general/img/favicons/apple-icon-72x72.png')?>">
        <link rel="apple-touch-icon" sizes="76x76" href="<?= site_url('items/general/img/favicons/apple-icon-76x76.png')?>">
        <link rel="apple-touch-icon" sizes="114x114" href="<?= site_url('items/general/img/favicons/apple-icon-114x114.png')?>">
        <link rel="apple-touch-icon" sizes="120x120" href="<?= site_url('items/general/img/favicons/apple-icon-120x120.png')?>">
        <link rel="apple-touch-icon" sizes="144x144" href="<?= site_url('items/general/img/favicons/apple-icon-144x144.png')?>">
        <link rel="apple-touch-icon" sizes="152x152" href="<?= site_url('items/general/img/favicons/apple-icon-152x152.png')?>">
        <link rel="apple-touch-icon" sizes="180x180" href="<?= site_url('items/general/img/favicons/apple-icon-180x180.png')?>">
        <link rel="icon" type="image/png" sizes="192x192"  href="<?= site_url('items/general/img/favicons/android-icon-192x192.png')?>">
        <link rel="icon" type="image/png" sizes="32x32" href="<?= site_url('items/general/img/favicons/favicon-32x32.png')?>">
        <link rel="icon" type="image/png" sizes="96x96" href="<?= site_url('items/general/img/favicons/favicon-96x96.png')?>">
        <link rel="icon" type="image/png" sizes="16x16" href="<?= site_url('items/general/img/favicons/favicon-16x16.png')?>">
        <link rel="manifest" href="<?= site_url('items/general/img/favicons/apple-icon-57x57.png')?>/manifest.json">
        <meta name="msapplication-TileColor" content="#ffffff">
        <meta name="msapplication-TileImage" content="<?= site_url('items/general/img/favicons/ms-icon-144x144.png')?>">
        <meta name="theme-color" content="#ffffff">
        
        <meta charset="UTF-8">
        
        <meta property="og:title" content="Thyssen-Bornemisza Art Contemporary"/>
    	<meta property="og:type" content="website"/>
    	<meta property="og:url" content="http://www.tba21.org/"/>
    	<meta property="og:image" content="<?= site_url('items/frontend/img/fb_share.jpg')?>"/>
    	<meta property="og:description" content="" />
    
        <title>Thyssen-Bornemisza Art Contemporary</title>
    
        <link rel="stylesheet" type="text/css" href="<?=site_url("items/general/css/reset.css"); ?>">
        <link rel="stylesheet" type="text/css" href="<?=site_url("items/frontend/css/jquery.qtip.css"); ?>">
        <link rel="stylesheet" type="text/css" href="<?=site_url("items/general/css/fonts.css"); ?>">
        <link rel="stylesheet" type="text/css" href="<?=site_url("items/frontend/css/jquery.fancybox.css"); ?>">
        <link rel="stylesheet" type="text/css" href="<?=site_url("items/frontend/css/desktop.css"); ?>">
        
        <?php if($is_mobile):?>
    		<link rel="stylesheet" type="text/css" href="<?=site_url("items/frontend/css/mobile.css"); ?>">
    	<?php endif;?>		
        
        <script type="text/javascript" src="<?=site_url("items/general/js/jquery-1.11.2.min.js"); ?>"></script>
        <script type="text/javascript" src="<?=site_url("items/general/js/jquery-ui.min.js"); ?>"></script>
        <script type="text/javascript" src="<?=site_url("items/frontend/js/jquery.visible.min.js"); ?>"></script>
        <script type="text/javascript" src="<?=site_url("items/frontend/js/imagesloaded.pkgd.min.js"); ?>"></script>
        <script type="text/javascript" src="<?=site_url("items/frontend/js/jquery.fancybox.js"); ?>"></script>
        <script type="text/javascript" src="<?=site_url("items/frontend/js/cookie.js"); ?>"></script>
        <script type="text/javascript" src="<?=site_url("items/frontend/js/navigation.js"); ?>"></script>
        <script type="text/javascript" src="<?=site_url("items/frontend/js/responsive.js"); ?>"></script>
        <script type="text/javascript" src="<?=site_url("items/frontend/js/desktop.js"); ?>"></script>
        <script type="text/javascript" src="<?=site_url("items/frontend/js/registration.js"); ?>"></script>
        <?php if($is_mobile):?>
    		<script type="text/javascript" src="<?=site_url("items/frontend/js/jquery.mobile.custom.min.js"); ?>"></script>
    		<script type="text/javascript" src="<?=site_url("items/frontend/js/jquery.ui.touch-punch.min.js"); ?>"></script>
    		<script type="text/javascript" src="<?=site_url("items/frontend/js/mobile.js"); ?>"></script>
    	<?php elseif($is_ipad):?>
    		<script type="text/javascript" src="<?=site_url("items/frontend/js/jquery.mobile.custom.min.js"); ?>"></script>
    		<script type="text/javascript" src="<?=site_url("items/frontend/js/jquery.ui.touch-punch.min.js"); ?>"></script>
    		<script type="text/javascript" src="<?=site_url("items/frontend/js/handheld.js"); ?>"></script>
    	<?php endif;?>		
        <script type="text/javascript" src="<?=site_url("items/frontend/js/press.js"); ?>"></script>
        <script type="text/javascript" src="<?=site_url("items/frontend/js/sharing.js"); ?>"></script>
        <script type="text/javascript" src="<?=site_url("items/frontend/js/store.js"); ?>"></script>
        
        <script>
            var canvasWidth = <?= $canvasWidth?>;
            var canvasHeight = <?= $canvasHeight?>;
            var itemWidth = <?= $itemWidth?>;
            var itemHeight = <?= $itemHeight?>;
            var rootUrl = "<?= site_url()?>";
            var language = "<?= $language?>";
            var canvasCols = <?= $canvasCols?>;
            var canvasRows = <?= $canvasRows?>;
            var preselect = "<?= $preselect?>";
            var is_mobile = <?= $is_mobile ? 'true' : 'false'?>;
            var is_ipad = <?= $is_ipad ? 'true' : 'false'?>;
            var show_splash = <?= $splashpage['show'] ? 'true' : 'false'?>;
            var splash_itemId = "<?= $splashpage['itemId']?>";
            var cookieinfo = <?= $show_cookie_warning ? 'true' : 'false'?>;
        </script>
        
    	<script>
    	  window.fbAsyncInit = function() {
    	    FB.init({
    	      appId      : '440229979512060',
    	      xfbml      : true,
    	      version    : 'v2.5'
    	    });
    	  };
    	
    	  (function(d, s, id){
    	     var js, fjs = d.getElementsByTagName(s)[0];
    	     if (d.getElementById(id)) {return;}
    	     js = d.createElement(s); js.id = id;
    	     js.src = "//connect.facebook.net/en_US/sdk.js";
    	     fjs.parentNode.insertBefore(js, fjs);
    	   }(document, 'script', 'facebook-jssdk'));
    	</script>
    	
    	<script>
          (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
          (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
          m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
          })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
        
          ga('create', 'UA-5934189-70', 'auto');
          ga('send', 'pageview');
        
        </script>
    	
    </head>

    <body>
        <div id="viewport" class="noselect">
            <div id="canvas" class="noselect" style="width: <?= $canvasWidth?>px; height: <?= $canvasHeight?>px;" >
                <div id="center" class="noselect">
                    <img id="center_logo" class="noselect" src="<?= site_url('items/frontend/img/tba_logo.png')?>" />
                    <div id="center_text" class="noselect"><?= $this->lang->line('intro')?></div>
                </div> 
                
                <?= $items?>
            </div>
            <div id="direction_pointer_collection" class="direction_pointer noselect" continent_id="<?= CONTINENT_COLLECTION?>" text_<?= LANG_EN?>="<?= $continentName[LANG_EN][CONTINENT_COLLECTION]?>"  text_<?= LANG_DE?>="<?= $continentName[LANG_DE][CONTINENT_COLLECTION]?>" ><?= $continentName[$language][CONTINENT_COLLECTION]?></div>
            <div id="direction_pointer_program" class="direction_pointer noselect" continent_id="<?= CONTINENT_PROGRAM?>" text_<?= LANG_EN?>="<?= $continentName[LANG_EN][CONTINENT_PROGRAM]?>"  text_<?= LANG_DE?>="<?= $continentName[LANG_DE][CONTINENT_PROGRAM]?>" ><?= $continentName[$language][CONTINENT_PROGRAM]?></div>
            <div id="direction_pointer_production" class="direction_pointer noselect" continent_id="<?= CONTINENT_PRODUCTION?>" text_<?= LANG_EN?>="<?= $continentName[LANG_EN][CONTINENT_PRODUCTION]?>"  text_<?= LANG_DE?>="<?= $continentName[LANG_DE][CONTINENT_PRODUCTION]?>" ><?= $continentName[$language][CONTINENT_PRODUCTION]?></div>
            <div id="direction_pointer_academy" class="direction_pointer noselect" continent_id="<?= CONTINENT_ACADEMY?>" text_<?= LANG_EN?>="<?= $continentName[LANG_EN][CONTINENT_ACADEMY]?>"  text_<?= LANG_DE?>="<?= $continentName[LANG_DE][CONTINENT_ACADEMY]?>" ><?= $continentName[$language][CONTINENT_ACADEMY]?></div>
            
        </div>
        
        <?= $menus?>
        
        <?php if(!$is_mobile):?>
            <div id="bottom_menu" class="noselect">
                <span class="bottom_menuitem noselect" type="foundation" itemId="<?= $settings->foundation_item_id?>" text_<?= LANG_EN?>="<?= $staticName[LANG_EN]['foundation']?>"  text_<?= LANG_DE?>="<?= $staticName[LANG_DE]['foundation']?>"><?= $staticName[$language]['foundation']?></span>
                <span class="bottom_menuitem noselect" type="press" text_<?= LANG_EN?>="<?= $staticName[LANG_EN]['press']?>"  text_<?= LANG_DE?>="<?= $staticName[LANG_DE]['press']?>"><?= $staticName[$language]['press']?></span>
                <span class="bottom_menuitem noselect" type="contact" itemId="<?= $settings->contact_item_id?>" text_<?= LANG_EN?>="<?= $staticName[LANG_EN]['contact']?>"  text_<?= LANG_DE?>="<?= $staticName[LANG_DE]['contact']?>"><?= $staticName[$language]['contact']?></span>
                <span class="bottom_menuitem noselect share">
                    <a href="https://www.youtube.com/tbac21" target="_blank" ><img src="<?= site_url('items/frontend/img/youtube.png')?>" /></a>
                    <img id="newsletter_item" itemId="<?= $settings->newsletter_item_id?>" src="<?= site_url('items/frontend/img/newsletter_icon_black2.png')?>" />
                    <a href="https://instagram.com/tba_21" target="_blank" ><img src="<?= site_url('items/frontend/img/instagram.png')?>" /></a>
                    <a href="https://twitter.com/tba21" target="_blank" ><img src="<?= site_url('items/frontend/img/twitter.png')?>" /></a>
                    <a href="https://www.facebook.com/TBA21" target="_blank" ><img src="<?= site_url('items/frontend/img/facebook.png')?>" /></a>
                </span>
                
            </div>
        <?php endif;?>
        
        <div id="content_fade" class="noselect"></div> 
        
        <div id="content_container">
            
            <div id="content_navigation">
                <div id="content_navigation_menu">
                    <div id="menu_logo_small" class="content_navigation_menu_item"><img src="<?=site_url('items/frontend/img/tba_logo_menu.png')?>" /></div>
                    <div id="menu_button_small" class="content_navigation_menu_item"><img src="<?=site_url('items/frontend/img/menuicon.png')?>" /></div>
                </div>
                <div id="content_navigation_nav"></div>
                <div id="content_navigation_buttons">
                    <div class="content_navigation_button" type="hideSplash">
                        <img src="<?= site_url('items/frontend/img/tba_close.png')?>" />
                    </div>
                    <div class="content_navigation_button" type="search">
                        <img src="<?= site_url('items/frontend/img/search.png')?>" />
                    </div>
                    <div class="content_navigation_button" type="facebook">
                        <img src="<?= site_url('items/frontend/img/facebook.png')?>" />
                    </div>
                    <div class="content_navigation_button" type="twitter">
                        <img src="<?= site_url('items/frontend/img/twitter.png')?>" />
                    </div>
                </div>
            </div>
            
            <div id="content_search">
                <input type="text" id="content_search_input">
                <div id="content_search_button" text_<?= LANG_EN?>="<?= $staticName[LANG_EN]['search_button']?>"  text_<?= LANG_DE?>="<?= $staticName[LANG_DE]['search_button']?>"><?= $staticName[$language]['search_button']?></div>
                <div id="content_search_filters">
                    <div class="content_search_filter" class="noselect">
                        <input type="checkbox" class="noselect" id="filter_collection" continent_id="<?= CONTINENT_COLLECTION?>" CHECKED >
                        <label for="filter_collection" continent_id="<?= CONTINENT_COLLECTION?>" text_<?= LANG_EN?>="<?= $continentName[LANG_EN][CONTINENT_COLLECTION]?>"  text_<?= LANG_DE?>="<?= $continentName[LANG_DE][CONTINENT_COLLECTION]?>"><?= $continentName[$language][CONTINENT_COLLECTION]?></label>
                    </div>
                    <div class="content_search_filter" class="noselect">
                        <input type="checkbox" class="noselect" id="filter_program" continent_id="<?= CONTINENT_PROGRAM?>" CHECKED >
                        <label for="filter_program" continent_id="<?= CONTINENT_PROGRAM?>" text_<?= LANG_EN?>="<?= $continentName[LANG_EN][CONTINENT_PROGRAM]?>"  text_<?= LANG_DE?>="<?= $continentName[LANG_DE][CONTINENT_PROGRAM]?>"><?= $continentName[$language][CONTINENT_PROGRAM]?></label>
                    </div>
                    <div class="content_search_filter" class="noselect">
                        <input type="checkbox" class="noselect" id="filter_academy" continent_id="<?= CONTINENT_ACADEMY?>" CHECKED >
                        <label for="filter_academy" continent_id="<?= CONTINENT_ACADEMY?>" text_<?= LANG_EN?>="<?= $continentName[LANG_EN][CONTINENT_ACADEMY]?>"  text_<?= LANG_DE?>="<?= $continentName[LANG_DE][CONTINENT_ACADEMY]?>"><?= $continentName[$language][CONTINENT_ACADEMY]?></label>
                    </div>
                </div>
            </div> 
            
            <div id="content_loading">
                <img src="<?= site_url('items/frontend/img/TBAAnimLogo.gif')?>" />
            </div>
            
            <div id="content">

            </div>
        </div>
        
        <?php if(!$show_cookie_warning):?>
            <div id="cookie_info">
                <div id="cookie_info_container">
                    <div id="cookie_info_text"><?= $this->lang->line('cookieinfo')?></div>
                    <div id="cookie_info_btn"><?= $this->lang->line('cookieinfo_accept')?></div>        
                </div>
            </div>
        <?php endif;?>
        
        <div id="tooltip_container">
        </div>
        
        <?php if($is_mobile):?>
            <div id="orientation_overlay">
                <div text_<?= LANG_EN?>="<?= $staticName[LANG_EN]['mobile_orientationcheck']?>"  text_<?= LANG_DE?>="<?= $staticName[LANG_DE]['mobile_orientationcheck']?>"><?= $staticName[$language]['mobile_orientationcheck']?></div>
            </div>
        <?php endif;?>
    </body>
</html>
