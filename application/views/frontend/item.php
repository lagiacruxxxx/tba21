

                
                 
                    <?php if(!$item['isQuad']):?>
                    <div 
                        item_id=<?= $item['itemId']?> 
                        class="item hidden <?php if($item['row'] %2 != 0):?>rotated<?php endif;?> noselect" 
                        status="inactive" 
                        continent_id = <?= $item['continentId'] ?> 
                        style="left: <?= $item['left']?>px; top: <?= $item['top']?>px;"   
                        col=<?= $item['col']?> 
                        row=<?= $item['row']?>
                        metatags="<?php foreach($item['metatags']->result() as $tag):?><?= $tag->metatag_id?>,<?php endforeach;?>" 
                        tt_header_<?= LANG_EN?>="<?= str_replace('"', '\'', $item['name_en'])?>"
                        tt_header_<?= LANG_DE?>="<?= str_replace('"', '\'', $item['name_de'])?>"
                        
                    >
                       <div class="id_helper"><?= $item['itemId']?></div>
                       <div class="item_overlay original noselect" style="background-image: url(<?= site_url('items/uploads/items/' . ($item['row'] %2 != 0 ? $item['mirror_img'] : $item['img'])) ?>)"></div>
                       <div type=<?= $item['imgType']?> class="item_overlay selected selected<?= $item['imgType']?> noselect"></div>
                       <div type=<?= $item['imgType']?> class="item_overlay inactive noselect"></div>
                       <div class="item_overlay blur noselect" style="background-image: url(<?= site_url('items/uploads/items/' . ($item['row'] %2 != 0 ? $item['mirror_img_blurred'] : $item['img_blurred']))?>)"></div>
                    </div>    
                    <?php else:?>
                    <div 
                        item_id="null" 
                        class="item hidden <?php if($item['row'] %2 != 0):?>rotated<?php endif;?> noselect quad" 
                        status="inactive" 
                        style="left: <?= $item['left']?>px; top: <?= $item['top']?>px;"   
                        col=<?= $item['col']?> 
                        row=<?= $item['row']?>
                        continent_id = <?= $item['continentId'][0] ?> 
                    >
                        <?php for($i = 0 ; $i <= $item['quadCount'] ; $i++):?>
                            <div 
                                item_id=<?= $item['itemId'][$i]?> 
                                class="item item_quad hidden noselect <?php if($item['quadId'][$i] == 1):?>rotated<?php endif;?>" 
                                status="inactive" 
                                continent_id = <?= $item['continentId'][$i] ?> 
                                style="left: <?= $item['quadLeft'][$i]?>px; top: <?= $item['quadTop'][$i]?>px;"   
                                metatags="<?php foreach($item['metatags'][$i]->result() as $tag):?><?= $tag->metatag_id?>,<?php endforeach;?>" 
                                tt_header_<?= LANG_EN?>="<?= str_replace('"', '\'', $item['name_en'][$i])?>"
                                tt_header_<?= LANG_DE?>="<?= str_replace('"', '\'', $item['name_de'][$i])?>"
                                quad_id="<?= $item['quadId'][$i]?>"
                            >
                               <div class="id_helper"><?= $item['itemId'][$i]?></div>
                               <div class="item_overlay original noselect quad_overlay" style="background-image: url(<?= site_url('items/uploads/items/' . ($item['quadId'][$i] == 1 ? $item['mirror_img'][$i] : $item['img'][$i])) ?>)"></div>
                               <div type=<?= $item['imgType'][$i]?> class="item_overlay selected selected<?= $item['imgType'][$i]?> noselect quad_overlay"></div>
                               <div type=<?= $item['imgType'][$i]?> class="item_overlay inactive noselect quad_overlay"></div>
                               <div class="item_overlay blur noselect quad_overlay" style="background-image: url(<?= site_url('items/uploads/items/' . ($item['quadId'][$i] == 1 ? $item['mirror_img_blurred'][$i] : $item['img_blurred'][$i]))?>)"></div>
                            </div>    
                        <?php endfor;?>    
                        
                    </div>    
                    <?php endif;?>