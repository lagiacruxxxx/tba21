

		<?php if(!$expired): ?>
			<div id="content_headline"><?= $this->lang->line('shop_finished_headline')?></div>
			
			<div><?= $this->lang->line('shop_finished_text')?></div>
		<?php else:?>
        	<div class="shop_session_problem"><?= $this->lang->line('shop_session_problem')?></div>
        <?php endif;?>
