<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en" xml:lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	
</head>
<body>
	<table style="width:700px; margin: 0px auto;font-family: Helvetica;color:#000000; font-size: 14px;">
		<tr>
			<td colspan=4 style="color:#000000;font-size:16px;font-weight:bold;">
				<?= $subject;?><br/><br/>
			</td>
		</tr>
		<tr>
			<td colspan=2 style="font-size: 20px;"> 
				Olafur Eliasson—Green light<br>
				Ein künstlerischer Workshop<br>
				<br/>
				TBA21–Augarten<br/>
				12.03.—05.06.2016<br/>
				<br/>
				<br/>
				<br/>
				<br/>
			</td>
			<td colspan=2  style="text-align: right;"><img src="<?= site_url('items/frontend/img/invoiceimg.png')?>" style="width: 150px; vertical-align: top;" /></td>
		</tr>
		
		<tr>
			<td colspan=4>
				<?= $order->firstname . ' ' . $order->lastname . ' (' . $order->email . ')'  ?><br/>
				<?= $order->phone?><br/>
				<?= $order->street?><br/>
				<?= $order->zip . ' ' . $order->city . ' ' . $country?><br/>
				<br/>
				<br/>
				<br/>
			</td>
		</tr>
		
		<tr>
			<td colspan=4>
				Dear <?= $order->firstname . ' ' . $order->lastname?>,<br/>
				<br/>
				Thank you for your donation of EUR <?=$item_total?> in exchange for your Green light lamps designed by Olafur Eliasson.<br/>
				<br/>
				Your Green lights are being produced by young asylum seekers and refugees in the context of the educational and integrative art project Green light. This project was realized in collaboration with Verein Fluchtweg, Red Cross and Caritas Vienna in our exhibition space at Augarten.<br/>
				<br/>
				After covering the costs of this project, the net proceeds acquired from the lamps will be redirected in its entirety to these organizations. Through your support, TBA21, Verein Fluchtweg and Red Cross can offer future perspectives to young unaccompanied refugees and migrants in Austria.<br/>
				<br/>
				<br/>
			</td>
		</tr>
		
		<tr>
			<td colspan=4><span style="padding: 2px 6px; background-color: #000000; color: #ffffff;"><?= $this->lang->line('invoice_summary') ?></span></td>
		</tr>
			<?php foreach($items->result() as $item):?>
            <tr>
                <td><?= htmlspecialchars($item->name)?></td>
                <td><?= htmlspecialchars($item->amount)?></td>
                <td style="text-align: right;"><?= htmlspecialchars(number_format((float)$item->price, 2, '.', '')) . ' €'?></td>
                <td style="text-align: right;"><?= htmlspecialchars(number_format((float)$item->price * $item->amount, 2, '.', '') . ' €')?></td>
            </tr>
            <?php endforeach;?>
		<tr>
			<td colspan=4>
				<hr style="border:0px;height:1px;background:#000000;"/>			
			</td>
		</tr>
		<tr>
            <td ></td>
            <td ></td>
            <td ></td>
            <td style="text-align: right;"><?= $item_total . ' €'?></td>
        </tr>
        <tr>
            <td class=""></td>
            <td class=""></td>
            <td style="text-align: right;"><?= $shipment?></td>
            <td style="text-align: right;"><?= $delivery . ' €'?></td>
        </tr>
        
        <tr>
            <td class=""></td>
            <td class=""></td>
            <td style="text-align: right;"><?= $this->lang->line('total')?></td>
            <td style="text-align: right; font-weight: bold;"><?= $total . ' €'?></td>
        </tr>
		
		<tr>
			<td colspan=4>
				<hr style="border:0px;height:1px;background:#000000;margin: 15px 0px;"/>			
			</td>
		</tr>
		
		<tr>
			<td colspan=4><span style="padding: 2px 6px; background-color: #000000; color: #ffffff;"><?= $this->lang->line('invoice_payment') ?></span></td>
		</tr>
		
		<tr>
			<td colspan=4><?= $order->paymentType?></td>
		</tr>
		<tr>
			<td colspan=4><?= $order->cc_cardholder?></td>
		</tr>
		<tr>
			<td colspan=4><?= $order->cc_pan?></td>
		</tr>
		<tr>
			<td colspan=4><?= $order->cc_exp_date?></td>
		</tr>
		
		
		<tr>
			<td colspan=4>
				<br/>
				<br/>
				<br/>
				Many thanks,
				<br/>
				<br/>
				<br/>
				<br/>
				<br/>
				<br/>
			</td>
		</tr>
		
		<tr>
			<td colspan=2 style="font-size: 10px; text-align: left;">
				Thyssen-Bornemisza Art Contemporary–Augarten<br/>
				Scherzergasse 1A, 1020 Wien / tba21.org
			</td>
			<td colspan=2 style="font-size: 10px; text-align: right;">
				Die Abgabe der von Ihnen erworbene Lampe erfolgt unter Ausschluss jeglicher<br/>
				Gewährleistung und des Rechtsweges. DAC20226774/2 133446-0001
			</td>
		</tr>
		
	</table>
</body>
</html>