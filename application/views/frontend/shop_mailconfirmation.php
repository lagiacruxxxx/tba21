

	<table>
		<tr>
			<td colspan=4><span style="padding: 2px 6px; background-color: #000000; color: #ffffff;"><?= $this->lang->line('invoice_userheader') ?></span></td>
		</tr>
		<tr>
			<td colspan=4><?= $order->firstname . ' ' . $order->lastname . ' (' . $order->email . ')'  ?></td>
		</tr>
		<tr>
			<td colspan=4><?= $order->phone?></td>
		</tr>
		<tr>
			<td colspan=4><?= $order->street?></td>
		</tr>
		<tr>
			<td colspan=4><?= $order->zip . ' ' . $order->city . ' ' . $country?></td>
		</tr>
		<tr>
			<td><div style="margin-top: 20px;"></div></td>
		</tr>
		<tr>
			<td colspan=4><span style="padding: 2px 6px; background-color: #000000; color: #ffffff;"><?= $this->lang->line('invoice_summary') ?></span></td>
		</tr>
			<?php foreach($items->result() as $item):?>
            <tr>
                <td><?= htmlspecialchars($item->name)?></td>
                <td><?= htmlspecialchars($item->amount)?></td>
                <td style="text-align: right;"><?= htmlspecialchars(number_format((float)$item->price, 2, '.', '')) . ' €'?></td>
                <td style="text-align: right;"><?= htmlspecialchars(number_format((float)$item->price * $item->amount, 2, '.', '') . ' €')?></td>
            </tr>
            <?php endforeach;?>
		<tr>
			<td colspan=4>
				<hr style="border:0px;height:1px;background:#000000;"/>			
			</td>
		</tr>
		<tr>
            <td ></td>
            <td ></td>
            <td ></td>
            <td style="text-align: right;"><?= $item_total . ' €'?></td>
        </tr>
        <tr>
            <td class=""></td>
            <td class=""></td>
            <td style="text-align: right;"><?= $shipment?></td>
            <td style="text-align: right;"><?= $delivery . ' €'?></td>
        </tr>
        
        <tr>
            <td class=""></td>
            <td class=""></td>
            <td style="text-align: right;"><?= $this->lang->line('total')?></td>
            <td style="text-align: right; font-weight: bold;"><?= $total . ' €'?></td>
        </tr>

	</table>