

        <div id="content_headline"><?= $this->lang->line('presskit_header')?></div>
        
        <div class="press_text"><?= $this->lang->line('presskit_logged_in_as') . htmlspecialchars($pressuser->firstname . ' ' . $pressuser->lastname . ' (' . $pressuser->publication . ')')?></div>
        <div id="press_logout"><?= $this->lang->line('presskit_logout')?></div>
        
        <div id="content_columns">
            <div id="content_column_left" class="content_column">
                <?php $first = 0; $i = 0; foreach($presskits as $presskit):?>
                            
                    <?php if($i == $breakpoint):?>
                        
                    <?php endif;?>
                    
                    <?php
                        if($first != 0 && $first != $presskit['group'])
                            $first = 0;
                    
                        if($first == 0)
                        {
                            $first = $presskit['group'];
                            echo "<div class='presskit_year'>$first</div>";
                        }
                    ?>
                    <div class="presskit" item_id=<?= $presskit['item_id']?>>
                        <span><?php echo $language == LANG_EN ? $presskit['name'] : $presskit['name_de']; ?></span>
                    </div>
                <?php $i++; endforeach;?>
                
            </div>
            
            <div id="content_column_right" class="content_column">
            </div>
        </div>                        
        