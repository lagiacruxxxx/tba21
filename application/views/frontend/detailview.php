                
                
        <div id="content_headline"><?= $item->name?></div>
        <div id="content_header">
        <?php if($item->detail_type == DETAIL_TYPE_IMAGE):?>
            <?php if($item->detail_img != 'image_upload_placeholder.png'):?>
                <img src="<?= site_url('items/uploads/detailimg/' . $item->detail_img)?>" />
                <?php if($item->detail_img_credits != ""):?><div id="content_header_credits"><?= $item->detail_img_credits?></div><?php endif;?>
            <?php endif;?>
        <?php else:?>
            <?= $is_mobile ? $item->detail_html_mobile : $item->detail_html?>        
        <?php endif;?>        
        </div>
        <?php if($item->header != ""):?>
        <div id="content_description">
            <?= $item->header?>
        </div>        
        <?php endif;?>
        <div id="content_columns">
            <div id="content_column_left" class="content_column">
                <?php foreach($modules as $module):?>
                    <?php if($module['column_id'] == 0):?>
                        <?php 
                            switch($module['mod'])
                            {
                                case 'text':
                                    echo '<div module_id=' . $module['id'] . ' class="module module_text" style="top: ' . $module['top'] . 'px;">' . $module['content'] . '</div>';
                                    break;
                                case 'image':
                                    echo '<div module_id=' . $module['id'] . ' class="module module_image" style="top: ' . $module['top'] . 'px;"><img src="' . site_url('items/uploads/module_image/' . $module['fname']) . '" /></div>';
                                    break;
                                case 'bulletpoint':
                                    echo '<div module_id=' . $module['id'] . ' class="module module_bulletpoint" style="top: ' . $module['top'] . 'px;"><div class="enum_head"><span>' . $module['header'] . '</span></div><div class="enum_body">' . $module['content'] . '</div></div>';
                                    break;
                                case 'video':
                                    echo '<div module_id=' . $module['id'] . ' class="module module_video" style="top: ' . $module['top'] . 'px;"><iframe class="module_video_iframe" src="https://www.youtube.com/embed/' . $module['code'] . '?start=' . $module['start'] . '&amp;wmode=transparent" frameborder="0" allowfullscreen="" wmode="Opaque"></iframe></div>';
                                    break;
                                case 'html':
                                    echo '<div module_id=' . $module['id'] . ' class="module module_html" style="top: ' . $module['top'] . 'px;">' . $module['html'] . '</div>';
                                    break;
                                case 'headline':
                                    echo '<div module_id=' . $module['id'] . ' class="module module_headline" style="top: ' . $module['top'] . 'px;"><span>' . $module['headline'] . '</span></div>';
                                    break;
                                case 'download':
                                    echo '<div module_id=' . $module['id'] . ' class="module module_download" style="top: ' . $module['top'] . 'px;"><a href="'. site_url('items/uploads/module_download/' . $module['fname']) . '" target="_blank"><div class="module_download_field">' . $module['description'] . '</div></a></div>';
                                    break;
                                case '2col_image':
                                    echo '<div module_id=' . $module['id'] . ' class="module module_2col_image" style="top: ' . $module['top'] . 'px;"><img src="' . site_url('items/uploads/module_image/' . $module['fname']) . '" /></div>';
                                    break;
                                case 'store':
                                    echo '<div module_id=' . $module['id'] . ' class="module module_store" style="top: ' . $module['top'] . 'px;"><div class="module_store_text">' .$module['content'] . '</div>';
                                    echo '<div class="module_store_button store_add" magento_id="' . $module['magento_id'] . '">' . $this->lang->line('add_to_cart') . '</div>';
                                    echo '<div class="module_store_button store_tocart">' . $this->lang->line('go_to_cart') . '<span class="module_store_cart">(' . $cartcount . ')</span>' . '</div></div>';
                                    break;
                            }
                        ?>
                    <?php endif;?>
                <?php endforeach;?>
            </div>
            
            <div id="content_column_right" class="content_column">
                <?php foreach($modules as $module):?>
                    <?php if($module['column_id'] == 1):?>
                        <?php 
                            switch($module['mod'])
                            {
                                case 'text':
                                    echo '<div module_id=' . $module['id'] . ' class="module module_text" style="top: ' . $module['top'] . 'px;">' . $module['content'] . '</div>';
                                    break;
                                case 'image':
                                    echo '<div module_id=' . $module['id'] . ' class="module module_image" style="top: ' . $module['top'] . 'px;"><img src="' . site_url('items/uploads/module_image/' . $module['fname']) . '" /></div>';
                                    break;
                                case 'bulletpoint':
                                    echo '<div module_id=' . $module['id'] . ' class="module module_bulletpoint" style="top: ' . $module['top'] . 'px;"><div class="enum_head"><span>' . $module['header'] . '</span></div><div class="enum_body">' . $module['content'] . '</div></div>';
                                    break;
                                case 'video':
                                    echo '<div module_id=' . $module['id'] . ' class="module module_video" style="top: ' . $module['top'] . 'px;"><iframe class="module_video_iframe" src="https://www.youtube.com/embed/' . $module['code'] . '?start=' . $module['start'] . '&amp;wmode=transparent" frameborder="0" allowfullscreen="" wmode="Opaque"></iframe></div>';
                                    break;
                                case 'html':
                                    echo '<div module_id=' . $module['id'] . ' class="module module_html" style="top: ' . $module['top'] . 'px;">' . $module['html'] . '</div>';
                                    break;
                                case 'headline':
                                    echo '<div module_id=' . $module['id'] . ' class="module module_headline" style="top: ' . $module['top'] . 'px;"><span>' . $module['headline'] . '</span></div>';
                                    break;
                                case 'download':
                                    echo '<div module_id=' . $module['id'] . ' class="module module_download" style="top: ' . $module['top'] . 'px;"><a href="'. site_url('items/uploads/module_download/' . $module['fname']) . '" target="_blank"><div class="module_download_field">' . $module['description'] . '</div></a></div>';
                                    break;
                                case '2col_image':
                                    echo '<div module_id=' . $module['id'] . ' class="module module_2col_image" style="top: ' . $module['top'] . 'px;"><img src="' . site_url('items/uploads/module_image/' . $module['fname']) . '" /></div>';
                                    break;
                                case 'store':
                                    echo '<div module_id=' . $module['id'] . ' class="module module_store" style="top: ' . $module['top'] . 'px;"><div class="module_store_text">' .$module['content'] . '</div>';
                                    echo '<div class="module_store_button store_add" magento_id="' . $module['magento_id'] . '">' . $this->lang->line('add_to_cart') . '</div>';
                                    echo '<div class="module_store_button store_tocart">' . $this->lang->line('go_to_cart') . '<span class="module_store_cart">(' . $cartcount . ')</span>' . '</div></div>';
                                    break;                              
                            
                            }
                        ?>
                    <?php endif;?>
                <?php endforeach;?>
            </div>
        </div>   
        
        
       
        <div id="gallery">
            <?php $i = 0; foreach($galleryItems->result() as $galleryItem):?>
                <div class="gallery_item" href="<?= site_url('items/uploads/gallery/' . $galleryItem->fname)?>" data-fancybox-group="gallery_<?= $item->id?>" title="<?= nl2br($galleryItem->credits)?>">
                    <img src="<?= site_url('items/uploads/gallery/' . $galleryItem->fname)?>" title="<?= $galleryItem->credits?>"/>
                </div>
            <?php endforeach;?>
        </div>
        <?php if(count($relatedMetatags) > 0 || count($relatedItems) > 0):?>
            <div id="content_related"> 
                <div id="content_related_header">RELATED</div>
                <div id="content_related_tags">
                    <?php foreach($relatedMetatags as $relatedMetatag):?>
                        <?php if($relatedMetatag['type'] == 'metatag'):?>
                            <span metatag_id="<?= $relatedMetatag['data']->id?>"><?php echo $language == LANG_EN ? $relatedMetatag['data']->name : $relatedMetatag['data']->name_de?></span>
                        <?php else:?>
                            <a href="<?= prep_url($relatedMetatag['data']->external_url)?>" target="_blank"><span><?= $relatedMetatag['data']->external_text?></span></a>
                        <?php endif;?>
                    <?php endforeach;?>
                </div>
                <div id="content_related_items">
                    <?php foreach($relatedItems as $relatedItem):?>
                        <div class="content_related_item" item_id="<?= $relatedItem->id?>">
                            <img src="<?=site_url('items/uploads/detailimg/' . $relatedItem->detail_img)?>" />
                            <div class="content_related_item_overlay">
                                <div class="content_related_item_overlay_headline"><?= substr($relatedItem->name, 0, 55) ?> ...</div>
                            </div>
                        </div>
                    <?php endforeach;?>
                </div>
            </div>  
        <?php endif;?>
        
        
                
