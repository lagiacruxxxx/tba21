
<div id="menu_container_mobile" class="noselect">
    <div id="menu_dongle"></div>
    <div id="menu_mobile">
        <div id="menu_mobile_close"></div>
            
        <?php foreach($continents as $continentId => $continent):?>
            <div class="continent_menu" continent_id= <?= $continentId?>>
            <div class="continent_menu_header" text_<?= LANG_EN?>="<?= $continentName[LANG_EN][$continentId]?>"  text_<?= LANG_DE?>="<?= $continentName[LANG_DE][$continentId]?>"><span><?= $continentName[$language][$continentId]?></span></div>
            <?php if(isset($continentMenus[$continentId])):?>
            <ul class="continent_menu_items">
                <?php foreach($continentMenus[$continentId]['menuItems'] as $menuItem):?>
                    <li class="continent_menu_item" metatag_id="<?= $menuItem->id?>" text_<?= LANG_EN?>="<?= $menuItem->name?>" text_<?= LANG_DE?>="<?= $menuItem->name_de?>" ><span><?php echo ($language == LANG_EN ? $menuItem->name : $menuItem->name_de)?></span></li>
                <?php endforeach;?>
                <?php if($continentId == CONTINENT_COLLECTION):?>
                    <li class="continent_menu_item artist_list" text_<?= LANG_EN?>="<?=$staticName[LANG_EN]['artists']?>" text_<?= LANG_DE?>="<?=$staticName[LANG_DE]['artists']?>" ><span><?=$staticName[$language]['artists']?></span></li>
                <?php endif;?>
            </ul>
            <?php endif;?>
            </div>
        <?php endforeach;?>
        <div class="continent_menu static">
            <ul class="continent_menu_items static">
                <li class="continent_menu_item" type="foundation" itemId="<?= $settings->foundation_item_id?>" text_<?= LANG_EN?>="<?= $staticName[LANG_EN]['foundation']?>"  text_<?= LANG_DE?>="<?= $staticName[LANG_DE]['foundation']?>"><span><?= $staticName[$language]['foundation']?></span></li>
                <li class="continent_menu_item" type="press" text_<?= LANG_EN?>="<?= $staticName[LANG_EN]['press']?>"  text_<?= LANG_DE?>="<?= $staticName[LANG_DE]['press']?>"><span><?= $staticName[$language]['press']?></span></li>
                <li class="continent_menu_item" type="partners" itemId="<?= $settings->partners_item_id?>" text_<?= LANG_EN?>="<?= $staticName[LANG_EN]['partners']?>"  text_<?= LANG_DE?>="<?= $staticName[LANG_DE]['partners']?>"><span><?= $staticName[$language]['partners']?></span></li>
                <li class="continent_menu_item" type="contact" itemId="<?= $settings->contact_item_id?>" text_<?= LANG_EN?>="<?= $staticName[LANG_EN]['contact']?>"  text_<?= LANG_DE?>="<?= $staticName[LANG_DE]['contact']?>"><span><?= $staticName[$language]['contact']?></span></li>
                <li class="continent_menu_item" type="search" text_<?= LANG_EN?>="<?= $staticName[LANG_EN]['search']?>"  text_<?= LANG_DE?>="<?= $staticName[LANG_DE]['search']?>"><span><?= $staticName[$language]['search']?></span></li>
            </ul>
            
            <div id="lang_switch">
                <div class="lang_select <?php if($language == LANG_EN):?>active_select<?php endif;?>" lang="<?= LANG_EN?>">EN</div>
                <div> | </div>
                <div class="lang_select <?php if($language == LANG_DE):?>active_select<?php endif;?>" lang="<?= LANG_DE?>">DE</div>
            </div>
            
            <div id="store_cart" <?php if($cartCount > 0):?>style="display: block;"<?php endif;?>>
                <img src="<?= site_url('items/frontend/img/cart_symbol2.png')?>">
                <div id="store_cart_itemcount"><?= $cartCount;?></div>            
            </div>
            
        </div>
    </div>
</div>