

        <div id="content_headline"><?= $headline?></div>
        
            <div id="content_columns">
                <div id="content_column_left" class="content_column">
                    <?php $first = ""; $i = 0; foreach($artists->result() as $artist):?>
                        
                        <?php if($i == $break):?>
                            </div>
                            <div id="content_column_right" class="content_column">
                        <?php endif;?>
                        
                        <?php
                            if($first != '' && $first != strtoupper(substr($artist->ordering, 0, 1)))
                                $first = '';
                        
                            if($first == "")
                            {
                                $first = strtoupper(substr($artist->ordering, 0, 1));
                                echo "<div class='artist_list_header'>$first</div>";
                            }
                        ?>
                        <div class="artist_list_artist" metatag_id=<?= $artist->id?>>
                            <span><?php echo $language == LANG_EN ? $artist->name : $artist->name_de; ?></span>
                        </div>
                    <?php $i++; endforeach;?>
                </div>
            </div>                        
        </div>